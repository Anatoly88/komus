<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/holidays.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>Календарь праздников</h1>
							<div class="sideBar calendar">
								<div class="sideBar__left">
									<div class="datepicker js-datepicker"></div>
								</div>
								<div class="sideBar__right">
									<div class="holidayList">
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/1.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 января</a>
												<span class="holidaysItem__text">Новый год</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/2.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">7 января</a>
												<span class="holidaysItem__text">Рождество христово</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/3.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">23 февраля</a>
												<span class="holidaysItem__text">День защитника Отечества</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/4.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">8 марта</a>
												<span class="holidaysItem__text">Международный женский день</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/5.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 апреля</a>
												<span class="holidaysItem__text">День смеха</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/6.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">12 апреля</a>
												<span class="holidaysItem__text">День космонавтики</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/7.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 мая</a>
												<span class="holidaysItem__text">Праздник весны и труда</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/8.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">9 мая</a>
												<span class="holidaysItem__text">День Победы</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/9.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 июня</a>
												<span class="holidaysItem__text">Международный день защиты детей</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/10.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">12 июня</a>
												<span class="holidaysItem__text">День России</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/11.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 сентября</a>
												<span class="holidaysItem__text">День знаний</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/12.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">3 сентября</a>
												<span class="holidaysItem__text">День работников нефтяной и газовой</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/13.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">9 сентября</a>
												<span class="holidaysItem__text">Международный день красоты</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/14.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">13 сентября</a>
												<span class="holidaysItem__text">День программиста</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="holidaysItem">
											<a  href="#" class="holidaysItem__icon">
												<img src="images/holidaylist/15.svg" alt=""/>
											</a>
											<div class="holidaysItem__info">
												<a href="#" class="holidaysItem__title">1 октября</a>
												<span class="holidaysItem__text">Международный день музыки</span>
												<a href="#" class="holidaysItem__link">Что подарить?</a>
											</div>
										</div>
										<div class="paggination">
											<ul class="paggination__list">
												<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
												<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
												<li class="paggination__item"><span class="paggination__link current">5</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
												<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script type="text/javascript" src="js/calendar.js"></script>
		</footer><!-- #footer -->
	</body>
</html>