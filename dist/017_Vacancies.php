<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/vacancies.css" rel="stylesheet">
	<link href="css/reviews_slider.css" rel="stylesheet">
	<link href="css/photo_gallery_slider.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="vacancies js-tabsblock">
				<div class="content maxWidth">
					<h1>Вакансии</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<nav class="menuSideBar js-menuSideBar-sticky">
								<span class="menuSideBar__mobile js-menuSideBar-mobile">Вакансии</span>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">О нас</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Портфолио</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Наши партнеры</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Гос. заказчикам</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">Вакансии</a>
									</li>
								</menu>
							</nav>
						</div>
						<div class="sideBar__right">
							<p>
								Вы хотите работать в одной из самых успешных компаний российского рекламно-сувенирного бизнеса? Хотите стать частью нашей команды, получить интересную работу и возможности для развития? Тогда присоединяйтесь к нам! 
							</p>
							<p>
								Работа в компании «Комус-реклама» — это перспективная и серьезная возможность роста для динамичных, активных, коммуникабельных людей, нацеленных на высокий результат.
							</p>
							<p class="vacancies__title vacancies__title--small">
								Мы предлагаем:
							</p>
							<ul>
								<li>
									Оформление согласно ТК РФ;
								</li>
								<li>
									График работы: 5/2;
								</li>
								<li>
									Месторасположение офиса: г.Москва, ул.Маленковская, д.32, стр.3
									(в 7 минутах пешком от ст. м. «Сокольники»);
								</li>
								<li>
									Месторасположение производства: 
									между станциями метро «Волгоградский проспект» и «Текстильщики»
								</li>
								<li>
									Социальный пакет: посещение фитнес-клуба и турпоездки на льготных условиях, корпоративное кредитование сотрудников, программы для детей (подготовка к школе, организация отдыха и др.);
								</li>
								<li>
									Возможность приобретения товаров «Комуса» со скидкой;
								</li>
								<li>
									Перспективы профессионального и карьерного роста;
								</li>
								<li>
									Стабильную конкурентную заработную плату;
								</li>
								<li>
									Корпоративный спорт (футбол, волейбол, настольный теннис);
								</li>
								<li>
									Для работников производства предоставляется спецодежда и бесплатное питание.
								</li>
							</ul>
							<div class="vacancies__item">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Менеджер по продажам сувенирной и полиграфической продукции</h4>
								</header>
								<div class="vacancies__item-content">
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
							<div class="vacancies__item show">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Оператор</h4>
								</header>
								<div class="vacancies__item-content show">
								<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
							<div class="vacancies__item">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Менеджер по продажам полиграфии ЦФО</h4>
								</header>
								<div class="vacancies__item-content">
								<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
							<div class="vacancies__item">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Менеджер по продажам сувенирной и полиграфической продукции (B2G)</h4>
								</header>
								<div class="vacancies__item-content">
								<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
							<div class="vacancies__item">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Администратор</h4>
								</header>
								<div class="vacancies__item-content">
								<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
							<div class="vacancies__item">
								<header class="vacancies__item-header js-vacancies-show">
									<i class="vacancies__item-ic"></i>
									<h4 class="vacancies__title">Менеджер по продажам сувенирной продукции в регионах</h4>
								</header>
								<div class="vacancies__item-content">
								<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Образование среднее-специальное и выше;
											</li>
											<li>
												Опыт активных продаж и холодных звонков;
											</li>
											<li>
												Опыт работы с сувенирной 
												и полиграфической продукцией будет являться преимуществом!
											</li>
										</ul>
									</div>
									<div class="vacancies__item-col">
										<span class="vacancies__title vacancies__title--small">
											Пожелания к кандидатам:
										</span>
										<ul>
											<li>
												Прямые продажи (B2B) товаров и услуг Корпоративным Партнерам, продажа сувенирной и Полиграфической продукции корпоративным Партнерам;
											</li>
											<li>
												Расширение клиентской базы: звонки (в т.ч. холодные), коммерческие предложения, встречи, согласование договоров, контроль дебиторской задолженности;
											</li>
											<li>
												Работа в CRM: контакты с партнерами, задачи.
											</li>
										</ul>
									</div>
									<footer class="vacancies__item-footer">
										<div class="vacancies__item-contacts">
											<p>
												Контактная информация:
											</p>
											<p>
												<span>Телефон: (495)685-95-10, ( доб. 279) Пехота Светлана</span> 
												<span>E-mail: pesa02@komus.net</span>
											</p>
										</div>
										<a href="javascript:void(0);" data-form-class="requestCall" class="button redButton js-show-popup">Связаться с нами</a>
									</footer>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/reviews_slider.js"></script>
		<script src="js/photo_gallery_slider.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>