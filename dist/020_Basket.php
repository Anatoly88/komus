<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/order.css" rel="stylesheet">
	<link href="css/catalog_item.css" rel="stylesheet">
	<link href="css/basket.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="basket">
				<div class="content maxWidth">
					<h1>Корзина</h1>
					<form action="" class="basket__form">
						<div class="orderList__block">
							<div class="orderList__header">
								<span class="orderList__num">№</span>
								<span class="orderList__photo">Фото</span>
								<span class="orderList__name">Наименование / Артикул / Параметры <ins class="orderList__name-application">/ Нанесение</ins></span>
								<span class="orderList__price">Цена</span>
								<span class="orderList__application">Стоимость нанесения</span>
								<span class="orderList__amount">Кол-во</span>
								<span class="orderList__app">Нанесение</span>
								<span class="orderList__total">Стоимость</span>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">1.</div>
								<a href class="orderItem__img"><img src="media/1-19733618.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">2.</div>
								<a href class="orderItem__img"><img src="media/1-138308.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Часы настенные «Attendee»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">3.</div>
								<a href class="orderItem__img"><img src="media/1-436006-04.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Дорожные часы «Большое путешествие»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">4.</div>
								<a href class="orderItem__img"><img src="media/7872.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Мужские часы «Полет»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
						</div>
						<footer class="basket__footer">
							<div class="basket__footer-col">
								<a href="#" class="generate-btn">Сформировать КП</a>
								<a href="#" class="button lineButton">Продолжить покупки</a>
							</div>
							<div class="basket__footer-col">
								<div class="basket__total">
									<span>ИТОГО:</span>
									<span class="total">58 040.0<ins class="rub"></ins></span>
								</div>
								<button class="button redButton" type="submit">Оформить заказ</button>
							</div>
						</footer>
					</form>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/basket.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>