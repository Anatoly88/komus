<?php $breadcrumbs = false; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/catalog_item.css" rel="stylesheet">
		<link href="css/main_page_slider.css" rel="stylesheet">
		<link href="css/catalog_category.css" rel="stylesheet">
		<link href="css/news_item.css" rel="stylesheet">
		<link href="css/holidays.css" rel="stylesheet">
		<link href="css/about_company.css" rel="stylesheet">
		<link href="css/thanks_slider.css" rel="stylesheet">
		<link href="css/our_slider.css" rel="stylesheet">
		<link href="css/interview_block.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="js-mainPageSlider mainPageSlider">
					<div class="mainPageSlider__item">
						<a href="#" class="mainPageSlider__mobile"><img src="media/slider/640×640_1.jpg" class="" alt=""/></a>
						<a href="#" class="mainPageSlider__notebook"><img src="media/slider/1280×440_2.jpg" alt=""/></a>
						<a href="#" class="mainPageSlider__desktop" style="background-image: url(media/slider/1920×440_3.jpg);"></a>
					</div>
					<div class="mainPageSlider__item">
						<a href="#" class="mainPageSlider__mobile"><img src="media/slider/640×640_1.jpg" class="" alt=""/></a>
						<a href="#" class="mainPageSlider__notebook"><img src="media/slider/1280×440_2.jpg" alt=""/></a>
						<a href="#" class="mainPageSlider__desktop" style="background-image: url(media/slider/1920×440_3.jpg);"></a>
					</div>
					<div class="mainPageSlider__item">
						<a href="#" class="mainPageSlider__mobile"><img src="media/slider/640×640_1.jpg" class="" alt=""/></a>
						<a href="#" class="mainPageSlider__notebook"><img src="media/slider/1280×440_2.jpg" alt=""/></a>
						<a href="#" class="mainPageSlider__desktop" style="background-image: url(media/slider/1920×440_3.jpg);"></a>
					</div>
					<div class="mainPageSlider__item">
						<a href="#" class="mainPageSlider__mobile"><img src="media/slider/640×640_1.jpg" class="" alt=""/></a>
						<a href="#" class="mainPageSlider__notebook"><img src="media/slider/1280×440_2.jpg" alt=""/></a>
						<a href="#" class="mainPageSlider__desktop" style="background-image: url(media/slider/1920×440_3.jpg);"></a>
					</div>
				</div>
				<div class="tabBlock js-tabsblock">
					<div class="tabBlock__title">
						<h2>Новинки</h2>
					</div>
					<div class="tabBlock__tabs">
						<a href="javascript:void(0);" data-href="new" class="tabBlock__tab js-tab-link -active">Новинки</a>
						<a href="javascript:void(0);" data-href="specifical" class="tabBlock__tab js-tab-link">специальные предложения</a>
						<a href="javascript:void(0);" data-href="leader" class="tabBlock__tab js-tab-link">лидеры продаж</a>
						<a href="javascript:void(0);" data-href="trand" class="tabBlock__tab js-tab-link">тренды сезона</a>
					</div>
					<div class="tabBlock__list">
						<div class="js-tab__new js-tab">
							<div class="sliderItem js-sliderItem">
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__salePrice"></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__salePrice"></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__salePrice"></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__salePrice"></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__salePrice"></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
								<div class="catalogItem">
									<div class="catalogItem__block">
										<a href="#" class="catalogItem__link">
											<img src="media/7916.jpg" al="" class="catalogItem__img" />
										</a>
										<div class="catalogItem__information">
											<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
											<div class="catalogItem__price">
												<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
												<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
											</div>
											<div class="catalogItem__hiddenBlock">
												<span class="catalogItem__article">Артикул: 5654</span>
												<span class="catalogItem__availability">В наличии: 15 штук</span>
											</div>
										</div>
										<div class="catalogItem__functional">
											<div class="catalogItem__amout">
												<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
												<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
												<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
											</div>
											<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
												<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
													<g>
														<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
															c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
															c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
															C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
															 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
														<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
															c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
														<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
															c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
													</g>
												</svg>
												<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="js-tab__specifical js-tab hidden-block">

						</div>
						<div class="js-tab__leader js-tab hidden-block">

						</div>
						<div class="js-tab__trand js-tab hidden-block">

						</div>
					</div>
				</div>
				<div class="catalogCategory">
					<div class="catalogCategory__block">
						<div class="catalogCategory__title">
							<h2>Каталог</h2>
						</div>
						<div class="catalogCategory__list">
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-01.svg" class="icon1" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Ежедневники
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-02.svg" class="icon2" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Календари
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-03.svg" class="icon3" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Продуктовые подарки
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-04.svg" class="icon4" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Ручки
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-05.svg" class="icon5" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Электроника
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-06.svg" class="icon6" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Деловые аксессуары
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-07.svg" class="icon7" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Для дома
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-08.svg" class="icon8" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Готовые решения
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-09.svg" class="icon9" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Посуда и кухня
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-10.svg" class="icon10" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Подарки
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-11.svg" class="icon11" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Часы
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-12.svg" class="icon12" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Текстильная продукция
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-13.svg" class="icon13" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Зонты
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-14.svg" class="icon14" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Отдых и путешествия
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-15.svg" class="icon15" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Промо-продукция
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-16.svg" class="icon16" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Символика и награды
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-17.svg" class="icon17" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Полиграфическая продукция
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item">
								<a href="#" class="catalogCategory__item-icon">
									<img src="images/catalogcategory/Catalog-18.svg" class="icon18" alt="" />
								</a>
								<a href="#" class="catalogCategory__item-title">
									Подарочная упаковка
								</a>
								<div class="catalogCategory__itemBlock">
									<a href="#">Инструменты</a>
									<a href="#">Декор и интерьер</a>
									<a href="#">Для бани и сауны</a>
									<a href="#">Рулетки</a>
								</div>
							</div>
							<div class="catalogCategory__item pdf">
								<a href="#">
									<img src="images/catalogcategory/Catalog-19.svg" class="pdf-icon" alt="" />
									<img src="images/catalogcategory/Catalog-20.svg" class="present-icon" alt="" />
									<span>Скачать каталог в pdf</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="newsSlider">
					<div class="newsSlider__title">
						<h2>Новости</h2>
					</div>
					<div class="js-newsSlider newsSlider__slider">
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
						<div class="newsItem">
							<div class="newsItem__block">
								<a href="#" class="newsItem__img">
									<img src="media/-----16.png" alt="" />
								</a>
								<div class="newsItem__info">
									<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
									<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
									<span class="newsItem__date">18.10.2017</span>
								</div>
								<a href="#" class="newsItem__read">Читать подробнее</a>
							</div>
						</div>
					</div>
					<div class="newsSlider__all">
						<a href="#" class="button redButton newsSlider__button" data-animation="ripple">Смотреть все новости</a>
					</div>
				</div>
				<div class="holidaysScroll">
					<div class="holidaysScroll__title">
						<h2>Праздники</h2>
					</div>
					<div class="js-holidaysScroll holidaysScroll__block">
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/1sep.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">1 сентября</a>
								<span class="holidaysItem__text">День знаний</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/oil.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">3 сентября</a>
								<span class="holidaysItem__text">День работников нефтяной и газов</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/beauty.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/programmer.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">13 сентября</a>
								<span class="holidaysItem__text">День программиста</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/music.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">1 октября</a>
								<span class="holidaysItem__text">Международный день музыки</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">2 октября</a>
								<span class="holidaysItem__text">Международный день врача</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/mail.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 октября</a>
								<span class="holidaysItem__text">Всемирный день почты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
						<div class="holidaysItem">
							<a  href="#" class="holidaysItem__icon">
								<img src="images/holidays/doctor.svg" alt=""/>
							</a>
							<div class="holidaysItem__info">
								<a href="#" class="holidaysItem__title">9 сентября</a>
								<span class="holidaysItem__text">Международный день красоты</span>
								<a href="#" class="holidaysItem__link">Что подарить?</a>
							</div>
						</div>
					</div>
					<div class="holidaysScroll__all">
						<a href="#" class="button greenButton holidaysScroll__button">Календарь праздников</a>
					</div>
				</div>
				<div class="aboutCompany">
					<div class="sliderAboutCompany js-sliderAboutCompany">
						<div class="sliderAboutCompany__item" style="background-image: url(media/image_2.png);">
						</div>
						<div class="sliderAboutCompany__item" style="background-image: url(media/image_2.png);">
						</div>
						<div class="sliderAboutCompany__item" style="background-image: url(media/image_2.png);">
						</div>
					</div>
					<div class="aboutCompany__text">
						<h2 class="aboutCompany__title">О компании</h2>
						<p>Центр «Комус-реклама» уже более 20 лет успешно работает на рынке рекламно-сувенирной и полиграфической продукции и является бесспорным лидером среди российских компаний на рынке сувениров и бизнес-подарков. Центр «Комус-реклама» успел зарекомендовать себя как надежная, ориентированная на потребности Партнеров компания.</p>
						<p>Как принято в Компании «Комус», у нас нет клиентов, у нас есть Партнеры, с которыми мы ведем совместный бизнес на благо друг друга. «Комус-реклама» издает собственный каталог «Бизнес-сувениры и полиграфия» объеме, предлагая потребителям более 1200 наименований продукции, среди которой можно найти как зарекомендовавшую себя классическую продукцию, так и авангардные новинки сувенирной отрасли.</p>
						<p>Центр состоит из двух подразделений: Административно-творческого (фронт-офис) и Производственно-складского комплекса. В Административно-творческом комплексе сосредоточены лучшие управленческие и творческие кадры Компании, здесь находится административное звено, осуществляющее централизованное управление Компанией; творческая мастерская, где опытные арт-директоры работают над нестандартными заказами наших Партнеров; </p>
						<a href="#" class="aboutCompany__learnmore">узнать подробнее</a>
					</div>
				</div>
				<div class="ourBlock">
					<div class="ourBlock__block">
						<div class="ourBlock__title">
							<h2>Наши партнеры</h2>
						</div>
						<div class="ourSlider js-ourSlider greenArrow">
							<a href="#" class="ourSlider__item">
								<img src="media/-----20.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/aeroflot-russian-airlines-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-euroset.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/-------.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/gazprom-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/smi-sber-logo-vsegda-695.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/------------.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-rostelecom-v2.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/-----20.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/aeroflot-russian-airlines-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-euroset.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/-------.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/gazprom-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/smi-sber-logo-vsegda-695.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/------------.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-rostelecom-v2.png" alt="" />
							</a>
						</div>
						<div class="ourBlock__link">
							<a href="#">Все партнеры</a>
						</div>
					</div>
				</div>
				<div class="ourBlock">
					<div class="ourBlock__block">
						<div class="ourBlock__title">
							<h2>Наши бренды</h2>
						</div>
						<div class="ourSlider js-ourSlider greenArrow">
							<a href="#" class="ourSlider__item">
								<img src="media/2000px-coca-cola-logo-svg.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/1pr2me8s.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/maxresdefault.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/metro-cash-and-carry-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-svyaznoy.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/a7cd4383bb669da6bd6760850ecb4815.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-obi.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/2000px-coca-cola-logo-svg.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/1pr2me8s.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/maxresdefault.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/metro-cash-and-carry-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-svyaznoy.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/a7cd4383bb669da6bd6760850ecb4815.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-obi.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/2000px-coca-cola-logo-svg.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/1pr2me8s.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/maxresdefault.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/metro-cash-and-carry-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-svyaznoy.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/a7cd4383bb669da6bd6760850ecb4815.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-obi.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/2000px-coca-cola-logo-svg.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/1pr2me8s.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/maxresdefault.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/metro-cash-and-carry-logo.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-svyaznoy.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/a7cd4383bb669da6bd6760850ecb4815.png" alt="" />
							</a>
							<a href="#" class="ourSlider__item">
								<img src="media/logo-obi.png" alt="" />
							</a>
						</div>
						<div class="ourBlock__link">
							<a href="#">Все бренды</a>
						</div>
					</div>
				</div>
				<div class="thanksBlock">
					<div class="thanksBlock__title">
						<h2>Нас благодарят</h2>
					</div>
					<div class="thanksSlider js-thanksSlider greenArrow">
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/gos-letter-1-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Московская Городская Дума</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/gos-letter-2-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Аппарат Совета Федерации</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/gos-letter-3-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">РГГУ</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/gos-letter-4-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Центратомархив</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/gos-letter-5-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Администрация города Владимира</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/emercom-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">ФГБУ «Агентство «Эмерком» МЧС России»</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/fgup-grc-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">ФГУП «Главный радиочастотный центр»</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
						<div class="thanksSlider__item">
							<a href="#" class="thanksSlider__link js-fancybox">
								<img src="media/rostrud-b.png" alt="" />
							</a>
							<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
						</div>
					</div>
					<div class="thanksBlock__all">
						<a href="#" class="button redButton" data-animation="ripple">Смотреть все отзывы</a>
					</div>
				</div>
				<div class="interviewBlock">
					<div class="interviewBlock__block">
						<div class="questionsBlock">
							<div class="questionsBlock__form">
								<div class="questionsBlock__title">
									<h2>Если у вас возникли вопросы, мы готовы на них ответить</h2>
								</div>
								<form>
									<div class="questionsBlock__field">
										<div class="questionsBlock__field-left">
											<div class="formField js-formField">
												<input
													type="text"
													name="text-name"
													placeholder="ФИО"
													data-validation="required"
													data-error-msg="Заполните поле"
													data-validation-error-msg=" "
													/>
											</div>
											<div class="formField js-formField questionsBlock__phone">
												<input
													type="text"
													name="text-name"
													placeholder="Телефон"
													data-validation="required"
													data-error-msg="Заполните поле"
													class="js-phoneInput"
													data-validation-error-msg=" "
													/>
											</div>
											<div class="formField js-formField questionsBlock__mail">
												<input
													type="text"
													name="text-email"
													placeholder="Email"
													data-validation="email"
													data-error-msg="Заполните поле"
													data-validation-error-msg=" "
													/>
											</div>
										</div>
										<div class="questionsBlock__field-right">
											<div class="formField js-formField">
												<textarea
													type="text"
													name="text-name"
													placeholder="Вопрос"
													data-validation="required"
													data-error-msg="Заполните поле"
													data-validation-error-msg=" "
													></textarea>
											</div>
										</div>
									</div>
									<div class="formField js-formField questionsBlock__check">
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Я даю согласие на хранение и обработку своих персональных данных.<a href="#">Соглащение</a></ins>
										</label>
									</div>
									<div class="formField questionsBlock__button">
										<button class="button redButton" data-animation="ripple">Отправить</button>
									</div>
								</form>
							</div>
						</div>
						<div class="interview">
							<div class="interview__title">
								<h2>Опрос</h2>
							</div>
							<div class="interview__question">
								<span>Что должно быть в подарочной корзине?</span>
							</div>
							<div class="interview__form">
								<form>
									<div class="interview__checkbox js-interviewScroll">
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Чай</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Кофе</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Шоколад</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Мед / Варенье</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Мед / Варенье</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Мед / Варенье</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Мед / Варенье</ins>
										</label>
										<label class="checkbox">
											<input type="checkbox" name=""/>
											<span class="checkbox__button"></span>
											<ins class="checkbox__text">Мед / Варенье</ins>
										</label>
									</div>
									<div class="interview__fieldblock">
										<div class="interview__field formField">
											<input type="text" name="" placeholder="Свой вариант" />
										</div>
										<div class="interview__func">
											<div class="interview__send">
												<button class="button interview__buttonSend" data-animation="ripple">Голосовать</button>
											</div>
											<div class="interview__reset">
												<button type="reset" class="button interview__buttonReset">Сбросить</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/main_page_slider.js"></script>
			<script src="js/news_slider.js"></script>
			<script src="js/slider_item.js"></script>
			<script src="js/about_company_slider.js"></script>
			<script src="js/holidays_scroll.js"></script>
			<script src="js/our_slider.js"></script>
			<script src="js/thanks_slider.js"></script>
			<script src="js/interview_scroll.js"></script>
		</footer><!-- #footer -->
	</body>
</html>
