<?php $breadcrumbs = false; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/catalog_item.css" rel="stylesheet">
	<link href="css/kp.css" rel="stylesheet">
	<link href="css/order.css" rel="stylesheet">
	<link href="css/basket.css" rel="stylesheet">
</head>

<body class="withBackground commercial-offer-page">
	<div class="wrapper">
		<header class="commercial-offer-header">
			<a href="index.php" class="main-header__logo">
				<svg class="siteLogo" version="1.1"
					xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 179.9 72.7"
					enable-background="new 0 0 179.9 72.7" xml:space="preserve">
					<path fill="#D20A11"  d="M59.3,8.5c-8.8,0-16.5,7.3-16.5,16.9c0,1.7,0.5,3.9,1.1,5.6l-7.1,6.2c-0.3,0.3-0.3,0.3-0.6,0
						c-4.9-5.1-9.3-10.7-13.7-16.3l9.3-11.8c0,1.7,2.2,5.6,6,6.2c3.3,0,5.5-1.7,6.6-3.4c1.7-2.8,1.7-6.7-0.6-9c-2.2-2.3-4.1-2.5-6.6-2.3
						c-2.5,0.3-3.8,1.1-6,3.4c-3.1,3.9-6.2,7.9-9.3,11.8V2.3l-20.3,9v37.2L22,38.9V26.5l13.7,15.2c0.6,0.6,0.6,0.6,1.1,0l32.9-29.3
						C66.5,9.6,62.6,8.5,59.3,8.5"/>
					<path fill="#D20A11"  d="M63.8,37.7c-3.3,0-5.9-2.6-5.9-5.9c0-3.3,2.6-5.9,5.9-5.9c3.2,0,5.9,2.7,5.9,5.9
						C69.7,35.1,67,37.7,63.8,37.7 M173.6,32.2c-6.6,7.3-16.5,8.5-21.4-0.6l12.1-13c-0.8,3.4-0.9,6.4,2.7,7.9c8.8,3.4,13.1-10.7,2.7-16.3
						c-8.6-4.8-18.9,0.3-22.5,8.5l-6-8.5H122L136,31.3l-2,2c0-3.4-3.3-6.2-6.6-6.2c-1.1,0-2.2,0.6-3.3,1.1l-12.1-18L98.9,24.8l-9.3-14.7
						L75.8,26.5c0-4.5-1.1-8.5-3.9-11.8l-24.7,22c3.3,3.4,7.1,4.9,12.1,5.1c4.7,0.2,9.3-1.6,12.6-5.6l8.2-9.6l9.3,14.7l13.2-14.7
						l9.3,14.7l8.8-9c0,4.4,1.7,8.9,6.6,9c2.8,0.1,4.4-1.1,6.6-2.8l12.1-13c0,9,7.1,16.3,16.5,16.3c6.6,0,12.1-4,14.8-9.6H173.6z"/>
					<path fill="#706F6F"  d="M85.4,55.9c1-0.2,2.3-0.3,4.2-0.3c1.9,0,3.3,0.4,4.2,1.1c0.9,0.7,1.5,1.8,1.5,3.2c0,1.4-0.5,2.5-1.3,3.3
						c-1.1,1-2.6,1.5-4.5,1.5c-0.4,0-0.8,0-1.1-0.1v5h-3.1V55.9z M88.5,62.2c0.3,0.1,0.6,0.1,1,0.1c1.7,0,2.7-0.8,2.7-2.3
						c0-1.3-0.9-2-2.4-2c-0.6,0-1.1,0.1-1.3,0.1V62.2z"/>
					<polygon fill="#706F6F"  points="105.5,63.7 100.4,63.7 100.4,67 106.1,67 106.1,69.5 97.3,69.5 97.3,55.7 105.8,55.7 105.8,58.3 
						100.4,58.3 100.4,61.1 105.5,61.1 "/>
					<path fill="#706F6F"  d="M111.4,55.7v5.8h0.3l3.8-5.8h3.8l-4.8,6.1c1.8,0.5,2.6,1.7,3.2,3.3c0.5,1.5,0.9,3,1.6,4.4h-3.4
						c-0.5-0.9-0.7-2-1.1-3c-0.5-1.6-1.1-2.8-2.8-2.8h-0.6v5.8h-3.1V55.7H111.4z"/>
					<path fill="#706F6F"  d="M131.7,55.7v13.8h-3.1V58.3h-3.1v3.3c0,3.6-0.5,6.3-2.5,7.5c-0.7,0.4-1.6,0.7-2.8,0.7l-0.4-2.5
						c0.7-0.1,1.2-0.4,1.6-0.7c0.8-0.9,1-2.6,1-4.7v-6.1H131.7z"/>
					<path fill="#706F6F"  d="M137.7,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H137.7z M141.2,63.6l-0.9-2.9
						c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H141.2z"/>
					<path fill="#706F6F"  d="M159.6,64.2c-0.1-1.7-0.1-3.7-0.1-5.7h-0.1c-0.4,1.8-1,3.7-1.5,5.4l-1.7,5.4h-2.4l-1.5-5.3
						c-0.5-1.6-0.9-3.6-1.3-5.4h0c-0.1,1.9-0.1,4-0.2,5.7l-0.2,5.3h-2.9l0.9-13.8h4.2l1.4,4.6c0.4,1.6,0.9,3.3,1.2,4.9h0.1
						c0.4-1.6,0.9-3.4,1.3-5l1.5-4.6h4.1l0.8,13.8h-3.1L159.6,64.2z"/>
					<path fill="#706F6F"  d="M168.5,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H168.5z M172,63.6l-0.9-2.9
						c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H172z"/>
				</svg>
			</a>
			<div class="main-header__links">
				<div class="main-header__contacts">
					<div class="main-header__contacts-links">
						<a href="mailto:reklama@komus.net" class="main-header__email"><span>reklama@komus.net</span></a>
						<a href="tel:84952252301" class="main-header__phone">8 (495) 225-23-01</a>
					</div>
				</div>
			</div>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="basket commercial">
				<div class="commercial-offer">
					<div class="content maxWidth">
						<h1>Заголовок коммерческого предложения</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit augue lectus, vitae dictum dolor posuere molestie. Maecenas sit amet lorem feugiat, consequat est et, accumsan eros. Morbi tristique lorem vitae ipsum vulputate lacinia. Mauris aliquam suscipit mollis. Pellentesque eget nulla massa. Duis faucibus turpis nibh, quis auctor lorem vestibulum vel. Maecenas felis tortor, eleifend quis erat ultrices, elementum posuere risus.</p>
						<div class="commercial-offer__num">
							<span>№ КП:</span>
							<span>1210_11</span>
						</div>
					</div>
				</div>
				<div class="content maxWidth">
					<form action="" class="basket__form">
					<div class="orderList__block">
							<div class="orderList__header">
								<span class="orderList__num">№</span>
								<span class="orderList__photo">Фото</span>
								<span class="orderList__name">Наименование / Артикул / Параметры <ins class="orderList__name-application">/ Нанесение</ins></span>
								<span class="orderList__price">Цена</span>
								<span class="orderList__application">Стоимость нанесения</span>
								<span class="orderList__amount">Кол-во</span>
								<span class="orderList__app">Нанесение</span>
								<span class="orderList__total">Стоимость</span>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">1.</div>
								<a href class="orderItem__img"><img src="media/1-19733618.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">2.</div>
								<a href class="orderItem__img"><img src="media/1-436006-04.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Часы настенные «Attendee»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">3.</div>
								<a href class="orderItem__img"><img src="media/1-138308.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Дорожные часы «Большое путешествие»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
							<div class="orderItem">
								<div class="popUpForm__close js-basket-remove"></div>
								<div class="orderItem__number">4.</div>
								<a href class="orderItem__img"><img src="media/7872.png" alt=""/></a>
								<div class="orderItem__text">
									<a href="#" class="orderItem__name">Мужские часы «Полет»</a>
									<span class="orderItem__article">Артикул: 5645454</span>
									<div class="orderItem__option">
										<div class="orderItem__option-block">
											<span class="orderItem__option-title">Параметры</span>
											<span class="orderItem__option-item">Минимальный тираж:  3</span>
											<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
										</div>
										<div class="orderItem__option-block">
											<span class="orderItem__option-item application">
												<span class="orderItem__option-title">Нанесение:</span> Гравировка
											</span>
										</div>
									</div>
								</div>
								<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
								<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
								<div class="orderItem__amount">
									<div class="catalogItem__amout">
										<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
										<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
									</div>
								</div>
								<div class="orderItem__app">Гравировка</div>
								<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
								<div class="orderPrice">
									<div class="orderPrice__title">
										<span>Цена</span>
										<span>Стоимость нанесения</span>
										<span>Кол-во</span>
										<span>Стоимость</span>
									</div>
									<div class="orderPrice__price">
										<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
										<span class="orderPrice__price-price amount">
											<input pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" tabindex="0" type="number">
											<a href="#" class="js-catalogItem__plus catalogItem__plus" tabindex="0"></a>
											<a href="#" class="js-catalogItem__minus catalogItem__minus" tabindex="0"></a>
										</span>
										<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
									</div>
								</div>
							</div>
						</div>
						<footer class="commercial__footer">
							<div class="commercial__footer-col">
								<span class="strong">Дата составления КП: </span><span>28.12.2017</span>
							</div>

							<div class="commercial__footer-col">
								<a href="#" class="button redButton">Сформировать КП</a>
								<a href="#" class="button greenButton">Получить ссылку</a>
							</div>
							
							<div class="commercial__footer-col">
								<div class="commercial__total">
									<span>ИТОГО:</span>
									<span class="total">58 040.0<ins class="rub"></ins></span>
								</div>
							</div>
						</footer>
					</form>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/basket.js"></script>
	</footer>
</body>

</html>