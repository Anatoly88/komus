<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/printing.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="list-printing">
				<div class="content maxWidth">
					<h1>Виды нанесения</h1>
					<div class="list-printing__grid">
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_1.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ТАМПОПЕЧАТЬ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_6.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ШЕЛКОГРАФИЯ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_11.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ПЛОТТЕРНАЯ РЕЗКА</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_16.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">РИЗОГРАФ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_5.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ЛАЗЕРНАЯ ГРАВИРОВКА</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_2.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ТИСНЕНИЕ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_7.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ТЕРМОТРАНСФЕР</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_12.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">Вышивка</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_17.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ИЗГОТОВЛЕНИЕ ПЕЧАТЕЙ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_10.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">деколь</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_3.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ОФСЕТНАЯ ПЕЧАТЬ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_8.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">СУБЛИМАЦИОННАЯ ПЕЧАТЬ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_13.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ГРАВИРОВКА НА ШИЛЬДЕ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_18.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">УФ-ПЕЧАТЬ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_15.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">РЕЛЬЕФНЫЕ НАКЛЕЙКИ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_4.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ЦИФРОВАЯ ПЕЧАТЬ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_9.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">КРУГОВАЯ ШЕЛКОГРАФИЯ</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_14.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ПОЛИМЕРНая СМОЛа</span>
								</figcaption>
							</figure>
						</a>
						<a href="#" class="list-printing__item">
							<figure class="figure">
								<div class="list-printing__picture">
									<img src="media/printing_img_19.jpg" alt=""/>
								</div>
								<figcaption>
									<span class="figurecaption__text">ШЕВРОН</span>
								</figcaption>
							</figure>
						</a>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>