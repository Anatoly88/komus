<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/catalog_item.css" rel="stylesheet">
		<link href="css/element_item.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>Часы настенные «Сковорода»</h1>
							<div class="elementItem js-elementItem">
								<div class="elementItem__sideBarLeft">
									<div class="elementItem__slider elementSlider js-elementSlider">
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
										<a href="#" class="elementSlider__item js-fancybox" rel="gallery1">
											<img src="media/1-184400.png" alt="" />
										</a>
									</div>
									<div class="elementItem__sliderNav elementSliderNav js-elementSliderNav">
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
										<div class="elementSliderNav__item">
											<a href="#" class="elementSliderNav__link js-fancybox" rel="gallery1">
												<img src="media/1-184400-------4.png" alt=""/>
											</a>
										</div>
									</div>
									<div class="elementItem__options elementOptions">
										<div class="elementOptions__head">
											<span class="elementOptions__title">Параметры</span>
											<a href="#" class="elementOptions__link js-elementOptions__link active" data-hide="Скрыть" data-show="Все параметры">Скрыть</a>
										</div>
										<div class="elementOptions__table">
											<div class="elementOptions__item">
												<span class="elementOptions__name">Артикул</span>
												<span class="elementOptions__info">1-10006454</span>
											</div>
											<div class="elementOptions__item">
												<span class="elementOptions__name">Бренд</span>
												<span class="elementOptions__info">Lorem Ipsum</span>
											</div>
											<div class="elementOptions__item">
												<span class="elementOptions__name">Цвет</span>
												<span class="elementOptions__info">Silver</span>
											</div>
											<div class="elementOptions__item">
												<span class="elementOptions__name">Размер</span>
												<span class="elementOptions__info">270 × 45 × 43 мм</span>
											</div>
											<div class="elementOptions__item">
												<span class="elementOptions__name">Материал</span>
												<span class="elementOptions__info">алюминий, пластик</span>
											</div>
											<div class="elementOptions__item">
												<span class="elementOptions__name">Вес</span>
												<span class="elementOptions__info">320 гр</span>
											</div>
										</div>
									</div>
								</div>
								<div class="elementItem__sideBarRight">
									<div class="elementItem__column1">
										<div class="elementColumn1__row">
											<div class="elementAvailableColors">
												<span class="elementAvailableColors__title">Доступные цвета:</span>
												<div class="elementAvailableColors__list js-elementAvailableColors__list">
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link active">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
													<div class="elementAvailableColors__item">
														<a href="#" class="elementAvailableColors__link">
															<img src="media/1-184400-------4.png" alt="" />
														</a>
													</div>
												</div>
											</div>
											<div class="elementPrice">
												<div class="elementPrice__price">
													<span class="elementPrice__price-title">Цена:</span>
													<span class="elementPrice__price-value">3 920.0<ins class="rub"></ins></span>
												</div>
												<div class="elementPrice__purchase">
													<span class="elementPrice__price-title">Закупочная цена:</span>
													<span class="elementPrice__price-value">2 820.0<ins class="rub"></ins></span>
												</div>
											</div>
										</div>
										<div class="elementApplication">
											<div class="elementApplication__title">Добавьте нанесение:</div>
											<div class="elementApplication__select js-elementApplicationBlock">
												<select class="js-select styled-select">
													<option disabled="disabled" selected="">Без нанесения</option>
													<option>Шелк</option>
													<option>Металл</option>
													<option>Хлопок</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
												</select>
												<div class="js-elementApplicationAddBlock">

												</div>
											</div>
											<div class="js-elementApplicationHiddenBlock hidden-block">
												<div class="js-elementApplicationSelect elementApplicationSelect">
													<select class="styled-select">
														<option disabled="disabled" selected="">Без нанесения</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
													<a href="javascript:void(0);" class="js-elementApplicationSelect__delete elementApplicationSelect__delete"></a>
												</div>
											</div>
											<a href="javascript:void(0);" class="addApplication js-addApplication">Добавить нанесение</a>
											<div class="elementApplication__available">
												<span class="elementApplication__available-title">Доступные нанесения:</span>
												<span class="elementApplication__available-text">Гравировка, тампопечать, деколь, вышивка.</span>
											</div>
										</div>
									</div>
									<div class="elementItem__column2">
										<a href="javascript:void(0);" class="button greyButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Получить консультацию</a>
										<div class="elementOrdersamples desktop">
											<a href="javascript:void(0);" class="button lineButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать образцы</a>
										</div>
										<div class="circulation remains">
											<div class="remains__header">
												<span class="remains__header-title">Размер</span>
												<span class="remains__header-title">Остаток</span>
												<span class="remains__header-title">Тираж</span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>XS/42</span></span>
												<span class="remains__item-col"><span>5 465</span></span>
												<span class="remains__item-col"><input type="number" pattern="[0-9]*"></span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>S/44</span></span>
												<span class="remains__item-col"><span>545</span></span>
												<span class="remains__item-col"><input type="number" pattern="[0-9]*"></span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>M/46</span></span>
												<span class="remains__item-col"><span>11</span></span>
												<span class="remains__item-col"><input type="number" pattern="[0-9]*"></span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>L/48</span></span>
												<span class="remains__item-col"><span>701</span></span>
												<span class="remains__item-col"><input type="number" pattern="[0-9]*"></span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>XL/50</span></span>
												<span class="remains__item-col"><span>2</span></span>
												<span class="remains__item-col"><input type="number" pattern="[0-9]*"></span>
											</div>
											<div class="remains__item">
												<span class="remains__item-col"><span>XXL/52</span></span>
												<span class="remains__item-col"><span>&mdash;</span></span>
												<span class="remains__item-col"><input disabled placeholder="&mdash;" type="text" name="" class="js-only-numbers"></span>
											</div>
										</div>
									</div>
									<div class="elementItem__column3">
										<div class="elementBuyBlock circulation">
											<div class="elementBuyBlock__list">
												<div class="elementBuyBlock__item">
													<div class="elementBuyBlock__left">Тираж:</div>
													<div class="elementBuyBlock__right">1</div>
												</div>
												<div class="elementBuyBlock__item">
													<div class="elementBuyBlock__left">Стоимость тиража: </div>
													<div class="elementBuyBlock__right">2 820.0<ins class="rub"></ins></div>
												</div>
												<div class="elementBuyBlock__item">
													<div class="elementBuyBlock__left">Стоимость нанесения: </div>
													<div class="elementBuyBlock__right">828<ins class="rub"></ins></div>
												</div>
												<div class="elementBuyBlock__item">
													<div class="elementBuyBlock__left">Стоимость 1шт. с нанесением и доп. услугами:</div>
													<div class="elementBuyBlock__right">3 648.0<ins class="rub"></ins></div>
												</div>
											</div>
											<div class="elementBuyBlock__total">
												<span class="elementBuyBlock__total-title">ИТОГО:</span>
												<span class="elementBuyBlock__total-price">3 648.0<ins class="rub"></ins></span>
											</div>
											<div class="catalogItem__functional">
												<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине" tabindex="0">
													<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
														<g>
															<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																 M357,197.5H42.8l-10-35.6H367L357,197.5z"></path>
															<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"></path>
															<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"></path>
														</g>
													</svg>
													<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
												</a>
											</div>
										</div>
										<div class="elementOrdersamples mobile">
											<a href="javascript:void(0);" class="button lineButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать образцы</a>
										</div>
									</div>
									<div class="elementItemDescription desktop">
										<span class="elementItemDescription__title">Описание</span>
										<div class="elementItemDescription__text">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit augue lectus, vitae dictum dolor posuere molestie. Maecenas sit amet lorem feugiat, consequat est et, accumsan eros. Morbi tristique lorem vitae ipsum vulputate lacinia. Mauris aliquam suscipit mollis. Pellentesque eget nulla massa. Duis faucibus turpis nibh, quis auctor lorem vestibulum vel. Maecenas felis tortor, eleifend quis erat ultrices, elementum posuere risus.</p>
											<p>Donec id metus orci. Quisque eget tempus est, a eleifend neque. Maecenas ligula sapien, faucibus eu venenatis ac, ultricies a urna. Suspendisse tristique, mauris a consequat maximus, nunc augue ullamcorper urna, interdum posuere enim ligula sed mi. Maecenas maximus eleifend lacus in sagittis. Duis sit amet nulla eleifend, condimentum metus ut, molestie ex. In tincidunt ullamcorper tellus, ac dignissim odio hendrerit non. Ut dignissim quam ac ipsum sagittis, ac laoreet nunc lobortis. Aliquam odio lorem, euismod a urna vitae, pretium efficitur massa. Integer dictum dolor at enim sagittis, tempus imperdiet lorem tincidunt. Nullam vitae ipsum arcu.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="elementItemDescription mobile">
								<span class="elementItemDescription__title">Описание</span>
								<div class="elementItemDescription__text">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit augue lectus, vitae dictum dolor posuere molestie. Maecenas sit amet lorem feugiat, consequat est et, accumsan eros. Morbi tristique lorem vitae ipsum vulputate lacinia. Mauris aliquam suscipit mollis. Pellentesque eget nulla massa. Duis faucibus turpis nibh, quis auctor lorem vestibulum vel. Maecenas felis tortor, eleifend quis erat ultrices, elementum posuere risus.</p>
									<p>Donec id metus orci. Quisque eget tempus est, a eleifend neque. Maecenas ligula sapien, faucibus eu venenatis ac, ultricies a urna. Suspendisse tristique, mauris a consequat maximus, nunc augue ullamcorper urna, interdum posuere enim ligula sed mi. Maecenas maximus eleifend lacus in sagittis. Duis sit amet nulla eleifend, condimentum metus ut, molestie ex. In tincidunt ullamcorper tellus, ac dignissim odio hendrerit non. Ut dignissim quam ac ipsum sagittis, ac laoreet nunc lobortis. Aliquam odio lorem, euismod a urna vitae, pretium efficitur massa. Integer dictum dolor at enim sagittis, tempus imperdiet lorem tincidunt. Nullam vitae ipsum arcu.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="similarProducts">
					<div class="similarProducts__title">
						<h2>Похожие товары</h2>
					</div>
					<div class="sliderItem js-sliderItem">
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__salePrice"></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__salePrice"></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__salePrice"></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__salePrice"></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__salePrice"></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
						<div class="catalogItem">
							<div class="catalogItem__block">
								<a href="#" class="catalogItem__link">
									<img src="media/7916.jpg" al="" class="catalogItem__img" />
								</a>
								<div class="catalogItem__information">
									<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
									<div class="catalogItem__price">
										<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
										<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
									</div>
									<div class="catalogItem__hiddenBlock">
										<span class="catalogItem__article">Артикул: 5654</span>
										<span class="catalogItem__availability">В наличии: 15 штук</span>
									</div>
								</div>
								<div class="catalogItem__functional">
									<div class="catalogItem__amout">
										<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
										<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
										<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
									</div>
									<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
										<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
											<g>
												<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
													c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
													c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
													C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
													 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
												<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
													c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
												<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
													c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
											</g>
										</svg>
										<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/element_item.js"></script>
			<script src="js/slider_item.js"></script>
		</footer><!-- #footer -->
	</body>
</html>
