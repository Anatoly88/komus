<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/portfolio.css" rel="stylesheet">
</head>

<body class="withBackground portfolio-fancy">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="portfolio-one">
				<div class="content maxWidth">
					<h1>Календари</h1>
					<div class="portfolio-one__grid">
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_1.jpg" alt="">
						</a>
						<a href="./media/calendar_img_2.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_2.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_3.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_4.jpg" alt="">
						</a>
						<a href="./media/calendar_img_5.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_5.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_6.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_1.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_2.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_3.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_4.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_5.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_6.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_1.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_2.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_3.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_4.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_5.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_6.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_5.jpg" alt="">
						</a>
						<a href="./media/-mg-4897------.jpg" class="portfolio-one__item js-fancybox" rel="group">
							<img src="./media/calendar_img_6.jpg" alt="">
						</a>
					</div>
					<div class="paggination">
						<ul class="paggination__list">
							<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
							<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
							<li class="paggination__item"><span class="paggination__link dots">...</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
							<li class="paggination__item"><span class="paggination__link current">5</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
							<li class="paggination__item"><span class="paggination__link dots">...</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
							<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>