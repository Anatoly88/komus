"use strict";
if ($('.js-reviewsSlider')) {
	
	$('.js-reviewsSlider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: false,
		variableWidth: false,
		adaptiveHeight: true,
		centerMode: true,
		centerPadding: '0',
		autoplay: true,
		autoplaySpeed: 2000,
	});

	$('.js-reviewsSlider').addClass('visible');
}