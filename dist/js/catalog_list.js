"use strict"; 
var $filterForm = null;

$(document).on('click','.js-categoryMenu-button', function (e) {
	e.preventDefault();
	var $this = $(this),
		showText = $this.data('show'),
		hideText = $this.data('hide');
	
	$this.toggleClass('active');
	$('.js-categoryMenu-menu').toggleClass('active');
	
	if ($this.hasClass('active')) {
		$this.text(hideText);
	} else {
		$this.text(showText);
	}
});

$(document).on('click','.js-categoryMenu-touch', function (e) {
	e.preventDefault();

	$('.categoryMenu__touch').toggleClass('active');
	$('.js-categoryMenu__touch-menu').toggleClass('active');
});

$(document).on('mouseenter','.js-catalogItem__list-link', function() {
	var $this = $(this),
		dataId = $this.data('id'),
		$catalogItems = $this.closest('.catalogItem').find('.js-catalogItem__block');

	$catalogItems.each(function() {
		var dataBlockId = $(this).data('id');

		if (dataId == dataBlockId) {
			$(this).removeClass('hidden-block');
		} else {
			$(this).addClass('hidden-block');
		}
	});
});

$(document).on('click', '.js-filterPopup__button', function(e) {

	if (!$filterForm) {
		$filterForm = $('.js-catalogFilterBlock').detach();
	}

	$filterForm.appendTo(".js-catalogFilter .popUpForm__container");
});

$(document).on('click','.js-showItems', function(e) {
	e.preventDefault();
	$.get( "_002_List_items.php", function(data) {
		var $data = $(data); 
		$('.js-showItems').remove();
		$('.js-catalogList').append($data.find('.js-catalogList').html());
	});
});

$(document).on('click', '.js-categoryMenu__showMenu', function(e) {
	e.preventDefault();
	var $this = $(this);

	$this.toggleClass('active');
	$this.closest('li').toggleClass('active');
	$this.closest('li').find('menu').toggleClass('active');
});

