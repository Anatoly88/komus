"use strict";

$(document).on('click','.js-waysDelivery__radio', function () {
	if ($('.js-waysDelivery__courier').is(':checked')) {
		$('.js-waysDelivery__courierBlock').show();
	} else {
		$('.js-waysDelivery__courierBlock').hide();
	}
});