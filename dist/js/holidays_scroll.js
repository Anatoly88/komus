"use strict";

scrollHolidays();

$(window).resize(function() {
	$.each($('.js-holidaysScroll'), function(){
		var api = $(this).data('jsp');
		api.reinitialise();
	});
});

function scrollHolidays() {
	$('.js-holidaysScroll').jScrollPane({
		contentWidth: '0px',
		horizontalDragMinWidth : 20,
		horizontalDragMaxWidth: 93,
		mouseWheelSpeed: 150,
		animateScroll: true,
		animateDuration: 100,
	});
}
