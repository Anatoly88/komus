"use strict";

if ($('.js-elementSlider')) {
	$('.js-elementSlider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.js-elementSliderNav',
	});

	$('.js-elementSlider').addClass('visible');
}

if ($('js-elementSliderNav')) {
	$('.js-elementSliderNav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.js-elementSlider',
		dots: false,
		arrows: true,
		centerMode: false,
		focusOnSelect: true,
		variableWidth: true,
	});

	$('.js-elementSliderNav').addClass('visible');
}

if ($('.js-elementAvailableColors__list')) {

	$('.js-elementAvailableColors__list').slick({
		centerMode: false,
		centerPadding: '0',
		slidesToShow: 4,
		variableWidth: true,
		dots: false,
		infinite: false,
	});

	$('.js-elementAvailableColors__list').addClass('visible');
}

$(document).on('click','.js-elementOptions__link', function(e) {
	e.preventDefault();
	var $this = $(this),
		hideText = $this.data('hide'),
		showText = $this.data('show');

	$this.toggleClass('active');

	if ($this.hasClass('active')) {
		$this.text(hideText);
	} else {
		$this.text(showText);
	}

	$('.js-elementOptions__item').each(function() {
		$(this).toggleClass('hidden');
	});
});

$(document).on('click','.js-addApplication', function (e) {
	e.preventDefault();
	var block = $($(".js-elementApplicationHiddenBlock").clone().html());
	block.find('select').SumoSelect({
				placeholder: $(this).data('title'),
				csvDispCount: 20,
				floatWidth: 0,
				nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
			});
	block.show();
	$(".js-elementApplicationAddBlock").append(block);
});

$(document).on('click','.js-elementApplicationSelect__delete', function(e){
	e.preventDefault();
	$(this).closest('.js-elementApplicationSelect').remove();
});
