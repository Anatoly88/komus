$.fn.isInViewport = function () {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();

	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();

	return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.ForceNumericOnly = function () {
	return this.each(function () {
		$(this).keydown(function (e) {
			var isModifierkeyPressed = (e.metaKey || e.ctrlKey || e.shiftKey);
			var isCursorMoveOrDeleteAction = ([46,8,37,38,39,40].indexOf(e.keyCode) != -1);
			var isNumKeyPressed = (e.keyCode >= 48 && e.keyCode <= 58) || (e.keyCode >=96 && e.keyCode <= 105);
			var vKey = 86, cKey = 67,aKey = 65;
			switch(true){
			case isCursorMoveOrDeleteAction:
			case isModifierkeyPressed == false && isNumKeyPressed:
			case (e.metaKey || e.ctrlKey) && ([vKey,cKey,aKey].indexOf(e.keyCode) != -1):
			break;
			default:
				e.preventDefault();
			}
		});
	});
};

function detectIE() {
	if (navigator.userAgent.indexOf('Trident') != -1) {

		if (document.addEventListener) {
			$('html').addClass('ie-9')
		}

		if (typeof(window.msIndexedDB) != 'undefined') {
			$('html').addClass('ie-10')
		}
	}

	if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {
		$('html').addClass('ie');
	}
}

initializationElement();
validationForm();

$(document).on('validation','.js-formField input, .js-formField textarea', function(evt, valid) {
	var errorMessage = $(this).data('error-msg');
	if (valid == true) {
		$(this).attr('placeholder', '');
	} else {
		$(this).attr('placeholder', errorMessage);
	}

	if ($(this).val().length > 0) {
		$(this).addClass('active');
	} else {
		$(this).removeClass('active');
	}
});

function initializationElement() {
	detectIE();
	tmripple.init();

	if ($('.js-catalogItem__list')) {
		$('.js-catalogItem__list').slick({
		  dots: false,
		  vertical: true,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  verticalSwiping: true,
			centerMode: false,
			infinite: false,
		});
		$('.js-catalogItem__list').addClass('visible');
	}

	/*SUMMOSELECT*/
	if ($('.js-select').length > 0) {
		$('.js-select').each( function() {
			var $this = $(this),
				attr = $this.attr('multiple'),
				settings = {
					placeholder: $this.data('title'),
					csvDispCount: 20,
					floatWidth: 0,
					nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
				};

			if ($this.attr('multiple')) {
				settings.selectAll = true;
				settings.selectAlltext = $this.data('select');
			}

			$this.SumoSelect(settings);
		});

		$('.SumoSelect').each(function(){
			var $select = $(this),
				$wrapper = $select.find('.optWrapper'),
				$caption = $select.find('.CaptionCont');

			$caption.on('click', function(e) {
				if ($wrapper.find('.options li').length > 4) {
					$wrapper.jScrollPane({
						contentWidth: '0px',
						verticalDragMinHeight : 53,
						verticalDragMaxHeight: 53,
						mouseWheelSpeed: 50,
						animateScroll: true,
						animateDuration: 100
					});
				}
			});
		});
	}
	/*SUMMOSELECT*/

	/*FANCYBOX*/
	$(".js-fancybox").fancybox({
		openEffect: 'fade',
		closeEffect: 'fade',
		nextEffect: 'fade',
		prevEffect: 'fade',
		padding     : 65,
		margin      : 0,
		autoSize    : false,
		width       : '100%',
		height      : '100%',
		scrolling: false,
		changeFade: 'slow',
		helpers:  {
			title : { type : 'over' },
			overlay: {
				locked: false,
				speedIn    : 0,
					speedOut   : 0,
				css : {
					'background' : 'rgba(243, 243, 243, 0.5)'
				},
			},
		},
		beforeShow : function() {
			this.title =  (this.index + 1) +' ' + '<span>из</span>' + ' ' + this.group.length;
			$("body").css({'overflow-y':'hidden'});
		},
		afterClose: function(){
			$("body").css({'overflow-y':'visible'});
		},
		tpl: {
			error: '<p class="fancybox-error">Запрошенное содержимое не может быть загружено.<br/>Пожалуйста, повторите попытку позже.</p>',
			closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next: '<a title="Следующий" class="fancybox-nav fancybox-next arrowLink next" id="next-fancy" href="javascript:;"><span></span></a>',
			prev: '<a title="Предыдущий" class="fancybox-nav fancybox-prev arrowLink prev" id="prev-fancy" href="javascript:;"><span></span></a>'
		}
	});
	/*FANCYBOX*/

	/*TABS*/
	$(document).on('click','.js-tab-link', function(e) {
		e.preventDefault();
		var $this		= $(this),
			thisText	= $this.text(),
			classBlock	= $this.data('href'),
			$block		= $this.closest('.js-tabsblock'),
			$tabLinkBlock = $block.find('.js-mobiletabs'),
			tabBlock	= $block.find('.js-tab');

		$('.js-tab-link').each(function(){
			$(this).removeClass('-active');
		});

		tabBlock.each(function() {
			$(this).hide().removeClass('hidden-block');
		});

		$tabLinkBlock.removeClass('visible');
		$this.addClass('-active');
		$('.js-tab__' + classBlock).show().addClass('fadeIn').siblings().removeClass('fadeIn hidden-block');
	});
	/*TABS*/
	/*FORCENUMERIC*/
	if ($('.js-only-numbers').length > 0) {
		$('.js-only-numbers').ForceNumericOnly();
	}
	/*FORCENUMERIC*/
	/*PHONE INPUT*/
	var ua = navigator.userAgent.toLowerCase(),
		isAndroid = ua.indexOf("android") > -1;

	if (isAndroid) {
		$('.js-phoneInput').attr('type', 'tel');
	}

	$('.js-phoneInput').mask("+7 (999) 999-9999");
	/*PHONE INPUT*/

	/*DISABLED HOVER TOUCH*/
	if ('createTouch' in document) {
		try {
			var ignore = /:hover/;
			for (var i = 0; i < document.styleSheets.length; i++) {
				var sheet = document.styleSheets[i];
				if (!sheet.cssRules) {
					continue;
				}
				for (var j = sheet.cssRules.length - 1; j >= 0; j--) {
					var rule = sheet.cssRules[j];
					if (rule.type === CSSRule.STYLE_RULE && ignore.test(rule.selectorText)) {
						sheet.deleteRule(j);
					}
				}
			}
		}
		catch (e) {
		}
	}
	/*DISABLED HOVER TOUCH*/
};

function validationForm() {
	var myLanguage = {
		errorTitle: 'Ошибка отправки формы!',
		requiredField: 'Это обязательное поле',
		requiredFields: 'Вы задали не все обязательные поля',
		badTime: 'Вы задали некорректное время',
		badEmail: 'Вы задали некорректный e-mail',
		badTelephone: 'Вы задали некорректный номер телефона',
		badSecurityAnswer: 'Вы задали некорректный ответ на секретный вопрос',
		badDate: 'Вы задали некорректную дату',
		lengthBadStart: 'Значение должно быть в диапазоне',
		lengthBadEnd: ' символов',
		lengthTooLongStart: 'Значение длинее, чем ',
		lengthTooShortStart: 'Значение меньше, чем ',
		notConfirmed: 'Введённые значения не могут быть подтверждены',
		badDomain: 'Некорректное значение домена',
		badUrl: 'Некорретный URL',
		badCustomVal: 'Введённое значение неверно',
		andSpaces: ' и пробелы ',
		badInt: 'Значение - не число',
		badSecurityNumber: 'Введённый защитный номер - неправильный',
		badUKVatAnswer: 'Некорректный UK VAT номер',
		badStrength: 'Пароль не достаточно надёжен',
		badNumberOfSelectedOptionsStart: 'Вы должны выбрать как минимум ',
		badNumberOfSelectedOptionsEnd: ' ответов',
		badAlphaNumeric: 'Значение должно содержать только числа и буквы ',
		badAlphaNumericExtra: ' и ',
		wrongFileSize: 'Загружаемый файл слишком велик (максимальный размер %s)',
		wrongFileType: 'Принимаются файлы следующих типов %s',
		groupCheckedRangeStart: 'Выберите между ',
		groupCheckedTooFewStart: 'Выберите как минимум ',
		groupCheckedTooManyStart: 'Выберите максимум из ',
		groupCheckedEnd: ' элемент(ов)',
		badCreditCard: 'Номер кредитной карты некорректен',
		badCVV: 'CVV номер некорректно',
		wrongFileDim: 'Неверные размеры графического файла,',
		imageTooTall: 'изображение не может быть уже чем',
		imageTooWide: 'изображение не может быть шире чем',
		imageTooSmall: 'изображение слишком мало',
		min: 'минимум',
		max: 'максимум',
		imageRatioNotAccepted: 'Изображение с таким соотношением сторон не принимается',
		badBrazilTelephoneAnswer: 'Введённый номер телефона неправильный',
		badBrazilCEPAnswer: 'CEP неправильный',
		badBrazilCPFAnswer: 'CPF неправильный'
	};

	$.validate({
		language: myLanguage,
		modules: 'security',
		borderColorOnError: '#d10910',
		scrollToTopOnError: false
	});

};
/*changeQuantity*/
function changeQuantity($link, value) {
	if ($link.length <= 0){
		return;
	}

	var $input = $link.parent().find('input'),
		count = parseInt($input.val()) + value;

	count = count < 1 ? 1 : count;
	$input.val(count);

	if (isNaN($input.val())){
		$input.val(1)
	}
}

$(document).on('click','.js-catalogItem__minus', function(e) {
	e.preventDefault();
	changeQuantity($(this), -1);
});

$(window).resize(function() {
 if ($(window).width() < 1900) {
		$('.main-header__top-form form').appendTo($('.main-header__form'));
	}
});

// При скролле страницы появляется фиксированный хедер
$(window).scroll(function () {
	if ($(window).width() >= 751 && $(window).width() < 1263) {
		if ($(window).scrollTop() > 350) {
			$('.js-elementItem').addClass('fixed');
			var topScroll = $(document).scrollTop() - $('.js-elementItem').offset().top + 230;
			var documentScroll = $(document).scrollTop();
			var elementColum = $('.elementItem__column3').offset().top;

			if ( documentScroll <= elementColum - 400 ) {
					$('.elementItem__sideBarLeft').css({
						top: topScroll + 'px'
					});
			}
		} else {
			$('.js-elementSlider').slick('setPosition');
			$('.js-elementItem').removeClass('fixed');
		}
	}
});

$(window).scroll(function () {
	if ($(window).width() < 1900) {
		if ($(window).scrollTop() > 350) {
			$('.main-header__swim').addClass('fixed');
			$('.main-header__form form').appendTo($('.main-header__swim-form'));
		} else {
			$('.main-header__swim').removeClass('fixed');
			$('.main-header__swim').css({
				'transition': 'transform 0.6s'
			});
			$('.main-header__swim-form form').appendTo($('.main-header__form'));
		}
	}
	// На 1920 берем форму из main-header__top-form
	else {
		if ($(window).scrollTop() > 350) {
			$('.main-header__swim').addClass('fixed');
			$('.main-header__top-form form').appendTo($('.main-header__swim-form'));
		} else {
			$('.main-header__swim').removeClass('fixed');
			$('.main-header__swim-form form').appendTo($('.main-header__top-form'));
		}
	}
});

if ($('.menuSideBar').length > 0) {
	if ($(window).width() >= 1280) {
		(function(){
			var a = document.querySelector('.js-menuSideBar-sticky'), b = null, P = 52;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом
			window.addEventListener('scroll', Ascroll, false);
			document.body.addEventListener('scroll', Ascroll, false);
			function Ascroll() {
				if (b == null) {
					var Sa = getComputedStyle(a, ''), s = '';
					for (var i = 0; i < Sa.length; i++) {
						if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
							s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
						}
					}
					b = document.createElement('div');
					b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
					a.insertBefore(b, a.firstChild);
					var l = a.childNodes.length;
					for (var i = 1; i < l; i++) {
						b.appendChild(a.childNodes[1]);
					}
					a.style.height = b.getBoundingClientRect().height + 'px';
					a.style.padding = '0';
					a.style.border = '0';
				}
				var Ra = a.getBoundingClientRect(),
						R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.main-footer').getBoundingClientRect().top + 110);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
				if ((Ra.top - P) <= 0) {
					if ((Ra.top - P) <= R) {
						b.className = 'stop';
						b.style.top = - R + 110 +'px';
					} else {
						b.className = 'sticky';
						b.style.top = P + 110 + 'px';
					}
				} else {
					b.className = '';
					b.style.top = '';
				}
				window.addEventListener('resize', function() {
					a.children[0].style.width = getComputedStyle(a, '').width
				}, false);
			}
			})()
	}
}
// В плавающем хедере при клике на кнопку поиска показывать форму поиска

$(document).on('click', '.js-show-search-form', function(e) {
	e.preventDefault();
	$('.main-header__swim-form').toggleClass('show');
});
$(document).on('click','.js-catalogItem__plus', function(e) {
	e.preventDefault();
	changeQuantity($(this), 1);
});


$(document).on('change','.js-catalogItem__amoutInput', function(e) {
	e.preventDefault();
	changeQuantity($(this), 0);
});
/*changeQuantity*/
/*BUY BUTTON*/
$(document).on('click','.js-catalogItembuy', function(e) {
	e.preventDefault();

	var $this		= $(this),
		textButton	= $this.data('cart');

	$this.find('.js-catalogItem__buy-text').text(textButton);
	$this.removeClass('js-catalogItembuy');
});
/*BUY BUTTON*/

// Открытие меню сайдбара на мобильных

$(document).on('click','.js-menuSideBar-mobile', function(e) {
	e.preventDefault();

	var $this	= $(this);

	$(this)
		.add('.js-menuSideBar')
		.toggleClass('active');
});

$(document).on('mouseup', (e) => {
	let $target = $('.js-menuSideBar, .js-menuSideBar-mobile');

	if (!$target.is(e.target) && $target.has(e.target).length === 0) {
		$target.removeClass('active');
	}
});

// В сайдбаре на мобильных подставляем текст их ссылке в "кнопку" и добавляем активных класс ссылке

$(document).on('click','.js-menuSideBar-link', function(e) {
	e.preventDefault();

	var $this	= $(this);
	var $menuSideBarLinkText = $(this).html();
	var $menuSideBarMobile = $('.js-menuSideBar-mobile');
	var $menuSideBar = $('.js-menuSideBar');
	var $menuSideBarLink = $('.js-menuSideBar-link');

	$menuSideBarLink.removeClass('current');
	$this.addClass('current');
	$menuSideBar.removeClass('active');
	$menuSideBarMobile.html($menuSideBarLinkText).removeClass('active');
});

// Показываем вакансию при клике на заголовок вакансии

$(document).on('click','.js-vacancies-show', function(e) {
	e.preventDefault();

	var $this	= $(this);
	var $vacanciesItem = $this.parent();
	var $vacanciesContent = $this.siblings('.vacancies__item-content');

	$vacanciesItem.add($vacanciesContent).toggleClass('show');
	$vacanciesItem.siblings().removeClass('show');
	$vacanciesItem.siblings().find('.vacancies__item-content').removeClass('show');
});
