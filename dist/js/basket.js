"use strict";

$(document).on('click', '.js-basket-remove', function () {
    var $orderItemBasket = $(this).parent();

    $orderItemBasket.css({
      'transform':'translateX(-120%)',
      'transition':'transform 0.7s'
    });
    setTimeout(function () { 
      $orderItemBasket.remove();
     }, 500);
});