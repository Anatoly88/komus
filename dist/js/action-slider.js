"use strict";

if ($('.js-action-slider')) {

	$('.js-action-slider').each( function (i, item) {
		$(this).slick({
			centerMode: false,
			centerPadding: '0',
			slidesToShow: 5,
			variableWidth: true,
			dots: false,
			infinite: false,
			responsive: [
				{
					breakpoint: 1920,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 5,
						variableWidth: true,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 3,
						variableWidth: true,
					}
				},
				{
					breakpoint: 768,
					settings: {
						centerMode: true,
						centerPadding: '0',
						slidesToShow: 1,
						variableWidth: true,
					}
				}
			]
		});
		$(this).addClass('visible');
	});
}