"use strict";

if ($('.js-thanksSlider')) {

	$('.js-thanksSlider').slick({
		centerMode: false,
		centerPadding: '0',
		slidesToShow: 8,
		variableWidth: true,
		dots: false,
		infinite: false,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 6,
					variableWidth: true,
				}
			},
			{
				breakpoint: 1280,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 4,
					variableWidth: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 2,
					variableWidth: true,
				}
			}
		]
	});
	
	$('.js-thanksSlider').addClass('visible');
}

if ($('.js-thanksSlider-partners')) {

	$('.js-thanksSlider-partners').slick({
		centerMode: false,
		centerPadding: '0',
		slidesToShow: 8,
		variableWidth: true,
		dots: false,
		infinite: false,
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 6,
					variableWidth: true,
				}
			},
			{
				breakpoint: 1280,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 3,
					variableWidth: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerMode: true,
					centerPadding: '0',
					slidesToShow: 1,
					variableWidth: true,
				}
			}
		]
	});
	
	$('.js-thanksSlider-partners').addClass('visible');
}