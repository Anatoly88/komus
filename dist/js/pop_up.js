var blockUiOpen = false;

$.fn.center = function () {
	var top = ($(window).height() - this.height()) / 2;
	if (top < 0){
		top = 0; 
	}
	this.css("position","absolute");
	this.css("top", top  + "px");
	return this;
}

$(document).on('click', '.js-show-popup', function(e){
	e.preventDefault();

	var $form = $('.js-'+ $(this).data('form-class'));
	console.log($form);
	$.blockUI({ 
		message: $form,
		onOverlayClick: $.unblockUI,
		onBlock: function(){
			$('body').addClass('hidden-overflow');
			blockUiOpen = true;
			$form.addClass('visible').removeClass('hidden-block');
		},
		onUnblock: function(){
			$('body').removeClass('hidden-overflow');
			blockUiOpen = false;
		},
			css: { 
			border:0,
			centerY: false,
			position:'fixed',
			padding: 0,
			cursor: 'default',
			left: '0%',
			top: 0,
			zIndex: '2000',
			right: '0%',
			width:'100%',
			height: '100%',
			marginBottom: '0',
			marginTop: '0',
			background: 'none',
			textAlign: 'left',
		},
			overlayCSS: {
				backgroundColor: 'rgb(243, 243, 243)',
				opacity: '0.5',
				zIndex: '1050',
				'cursor': 'default'
		},
		focusInput: false
	});

	// При клике на кнопку поиска на разрешениях менее 768 вырезаем в dom блок формы поиска и вставляем в попап

	if ($(this).hasClass('js-open-search-form')) {
		$('.main-header__form form').appendTo($('.popUpForm__search-box'));
	}

	if (!$form.hasClass('not-center')) {
		$form.center();
	}
});

/*CLOSE FORM*/
$(document).on('click touchstart','.js-close-form', function(e) {
	$.unblockUI();
	$('body').removeClass('hidden-overflow');
	$('.popUpForm.visible').removeClass('visible').addClass('hidden-block');
	blockUiOpen = false;
	e.preventDefault();

	// При клике на крестик в попапе формы поиска вырезаем форму и вставляем обратно в блок формы в хедере

	if ($(this).parent().parent().hasClass('js-headerSearch')) {
		$('.popUpForm__search-box form').appendTo($('.main-header__form .content'));
	}
});

$(document).on('click touchstart', function(e) {
	var $target = $(e.target);
	if (!$target.is('input')) {
		if (blockUiOpen && ($target.closest('.popUpForm').length === 0)) {
			$.unblockUI();
			$('body').removeClass('hidden-overflow');
			if ($('.popUpForm.visible').hasClass('js-headerSearch')) {
				$('.popUpForm__search-box form').appendTo($('.main-header__form .content'));
			}
			$('.popUpForm.visible').removeClass('visible').addClass('hidden-block');
			blockUiOpen = false;
			e.preventDefault();
		}
	}
});

$(document).keyup(function(e) {
	if (e.keyCode == 27) { 
	$.unblockUI();
		$('body').removeClass('hidden-overflow');
		if ($('.popUpForm.visible').hasClass('js-headerSearch')) {
			$('.popUpForm__search-box form').appendTo($('.main-header__form .content'));
		}
		$('.popUpForm.visible').removeClass('visible').addClass('hidden-block');
		blockUiOpen = false;
	}
});
/*CLOSE FORM*/