"use strict";

if ($('.js-newsSlider')) {

	$('.js-newsSlider').slick({
		centerMode: false,
		centerPadding: '0',
		slidesToShow: 5,
		variableWidth: true,
		dots: false,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 4,
					variableWidth: true,
				}
			},
			{
				breakpoint: 1280,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 3,
					variableWidth: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 1,
					variableWidth: true,
				}
			}
		]
	});

	$('.js-newsSlider').addClass('visible');
}