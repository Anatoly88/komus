if ($('.js-sync-slider').length > 0) {
	$('.js-once-slider').each( function (i, item) {
	$(this).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.js-several-slider',
	});
	$(this).addClass('visible');
});

	$('.js-several-slider').each( function (i, item) {
		$(this).slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			asNavFor: '.js-once-slider',
			dots: false,
			arrows: false,
			variableWidth: true,
			centerMode: false,
			focusOnSelect: true,
		});
		$(this).addClass('visible');
	}); 
}
