<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/state_customers.css" rel="stylesheet">
	<link href="css/thanks_slider.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="customers">
				<div class="content maxWidth">
					<h1>Госзаказчикам</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<nav class="menuSideBar js-menuSideBar-sticky">
								<span class="menuSideBar__mobile js-menuSideBar-mobile">Гос. заказчикам</span>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">О нас</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Портфолио</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Наши партнеры</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">Гос. заказчикам</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Вакансии</a>
									</li>
								</menu>
							</nav>
						</div>
						<div class="sideBar__right">
							<div class="customers__text">
								<p>
									Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения до федерального ведомства или госкорпорации. Имея собственное производство и систему логистики, «Комус-реклама» выполняет любые заказы независимо от их сложности и места доставки готовой продукции.
								</p>
								<p>
									Если вам необходима консультация по:
								</p>
								<ul>
									<li>
										рекламно-сувенирной и полиграфической продукции;
									</li>
									<li>
										подготовке технического задания или электронного аукциона;
									</li>
									<li>
										возможным условиям сотрудничества;
									</li>
								</ul>
								<p>
									Пожалуйста, обращайтесь в Отдел контрактных продаж.
								</p>
								<div class="importantInformation">
									<span class="importantInformation__text">Получить более подробную информацию Вы можете в отделе контрактных продаж</span>
									<span class="importantInformation__contact">
										<span class="importantInformation__contact-phone">Телефон:
(495) 685-95-10,
(доб. 246, 247, 271)</span>
										<br>
										<span>E-mail: tender02@komus.net</span>
									</span>
								</div>
							</div>
							<div class="customers__img">
								<img src="./media/goszakaz_Img.jpg" alt="">
							</div>
							<div class="customers__partners">
								<h2>Наши партнеры</h2>
								<div class="customers__partners-item">
									<h6>Учреждения федерального уровня:</h6>
									<ul>
										<li>
											Аппарат Совета Федерации
										</li>
										<li>
											Аппарат Государственной Думы
										</li>
										<li>
											Управление Делами Президента
										</li>
									</ul>
								</div>
								<div class="customers__partners-item">
									<h6>Региональные и местные учреждения:</h6>
									<ul>
										<li>
											Московская Городская Дума 
										</li>
										<li>
											Волгоградская Областная Дума
										</li>
										<li>
											Прокуратура г. Москва
										</li>
										<li>
											Управление ПФР по г. Набережные Челны
										</li>
									</ul>
								</div>
								<div class="customers__partners-item">
									<h6>Министерства и ведомства:</h6>
									<ul>
										<li>
											ФСО
										</li>
										<li>
											ФСБ
										</li>
										<li>
											МЧС
										</li>
										<li>
											Министерство сельского <br> хозяйства
										</li>
										<li>
											Рособоронпоставка
										</li>
									</ul>
								</div>
								<div class="customers__partners-item">
									<h6>Предприятия:</h6>
									<ul>
										<li>
											Транснефтепродукт
										</li>
										<li>
											Газпром трансгаз Саратов
										</li>
										<li>
											Атомтехэкспорт
										</li>
										<li>
											Гормост
										</li>
									</ul>
								</div>
								<div class="customers__partners-item">
									<h6>Образовательные учреждения:</h6>
									<ul>
										<li>
											МГУ
										</li>
										<li>
											МИИТ
										</li>
										<li>
											РГГУ
										</li>
										<li>
											РУДН
										</li>
									</ul>
								</div>
							</div>
							<div class="thanksBlock">
								<div class="thanksBlock__title">
									<h2>Благодарности от партнеров</h2>
								</div>
								<div class="thanksSlider js-thanksSlider-partners greenArrow">
									<div class="thanksSlider__item">
										<a href="./media/goszakaz_Img.jpg" class="thanksSlider__link js-fancybox">
											<img src="media/gos-letter-1-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Московская Городская Дума</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/image0002.png" class="thanksSlider__link js-fancybox">
											<img src="media/gos-letter-2-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Аппарат Совета Федерации</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/about_photo_1.jpg" class="thanksSlider__link js-fancybox">
											<img src="media/gos-letter-3-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">РГГУ</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/gos-letter-4-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/gos-letter-4-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Центратомархив</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/gos-letter-5-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/gos-letter-5-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Администрация города Владимира</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/emercom-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/emercom-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">ФГБУ «Агентство «Эмерком» МЧС России»</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/fgup-grc-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/fgup-grc-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">ФГУП «Главный радиочастотный центр»</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
									<div class="thanksSlider__item">
										<a href="media/rostrud-b.png" class="thanksSlider__link js-fancybox">
											<img src="media/rostrud-b.png" alt="" />
										</a>
										<span class="thanksSlider__title">Федеральная служба по труду и занятости</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/thanks_slider.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>