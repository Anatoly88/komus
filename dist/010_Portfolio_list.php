<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/portfolio.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="portfolio-list">
				<div class="content maxWidth">
					<h1>Портфолио</h1>
					<div class="portfolio-list__grid">
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_1.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Упаковка</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_2.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Календари</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_3.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Ежедневники</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_10.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Посуда</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_12.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Новогодние подарки</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_15.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Постеры</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_16.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Символика, награды</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_17.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Готовые решения</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_11.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Креативные подарки</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_4.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Промо – продукция</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_5.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Компьютерные аксессуары</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_6.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Текстиль</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_13.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Подарки</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_14.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Офисные и деловые аксессуары</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_7.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Письменные принадлежности</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_8.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Дизайн</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
						<a href="#" class="portfolio-list__item">
							<div class="portfolio-list__pic">
								<img src="./media/portfolio_img_9.jpg" alt="">
							</div>
							<div class="portfolio-list__title">Полиграфия</div>
							<div class="portfolio-list__link">Посмотреть</div>
						</a>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>