<footer class="main-footer">
	<div class="main-footer__top">
		<div class="content maxWidth">
			<div class="main-footer__box">
				<div href="#" class="main-footer__logo">
					<a href="#">
						<svg class="siteLogo" version="1.1"
							xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 179.9 72.7"
							enable-background="new 0 0 179.9 72.7" xml:space="preserve">
							<path fill="#D20A11"  d="M59.3,8.5c-8.8,0-16.5,7.3-16.5,16.9c0,1.7,0.5,3.9,1.1,5.6l-7.1,6.2c-0.3,0.3-0.3,0.3-0.6,0
								c-4.9-5.1-9.3-10.7-13.7-16.3l9.3-11.8c0,1.7,2.2,5.6,6,6.2c3.3,0,5.5-1.7,6.6-3.4c1.7-2.8,1.7-6.7-0.6-9c-2.2-2.3-4.1-2.5-6.6-2.3
								c-2.5,0.3-3.8,1.1-6,3.4c-3.1,3.9-6.2,7.9-9.3,11.8V2.3l-20.3,9v37.2L22,38.9V26.5l13.7,15.2c0.6,0.6,0.6,0.6,1.1,0l32.9-29.3
								C66.5,9.6,62.6,8.5,59.3,8.5"/>
							<path fill="#D20A11"  d="M63.8,37.7c-3.3,0-5.9-2.6-5.9-5.9c0-3.3,2.6-5.9,5.9-5.9c3.2,0,5.9,2.7,5.9,5.9
								C69.7,35.1,67,37.7,63.8,37.7 M173.6,32.2c-6.6,7.3-16.5,8.5-21.4-0.6l12.1-13c-0.8,3.4-0.9,6.4,2.7,7.9c8.8,3.4,13.1-10.7,2.7-16.3
								c-8.6-4.8-18.9,0.3-22.5,8.5l-6-8.5H122L136,31.3l-2,2c0-3.4-3.3-6.2-6.6-6.2c-1.1,0-2.2,0.6-3.3,1.1l-12.1-18L98.9,24.8l-9.3-14.7
								L75.8,26.5c0-4.5-1.1-8.5-3.9-11.8l-24.7,22c3.3,3.4,7.1,4.9,12.1,5.1c4.7,0.2,9.3-1.6,12.6-5.6l8.2-9.6l9.3,14.7l13.2-14.7
								l9.3,14.7l8.8-9c0,4.4,1.7,8.9,6.6,9c2.8,0.1,4.4-1.1,6.6-2.8l12.1-13c0,9,7.1,16.3,16.5,16.3c6.6,0,12.1-4,14.8-9.6H173.6z"/>
							<path fill="#706F6F"  d="M85.4,55.9c1-0.2,2.3-0.3,4.2-0.3c1.9,0,3.3,0.4,4.2,1.1c0.9,0.7,1.5,1.8,1.5,3.2c0,1.4-0.5,2.5-1.3,3.3
								c-1.1,1-2.6,1.5-4.5,1.5c-0.4,0-0.8,0-1.1-0.1v5h-3.1V55.9z M88.5,62.2c0.3,0.1,0.6,0.1,1,0.1c1.7,0,2.7-0.8,2.7-2.3
								c0-1.3-0.9-2-2.4-2c-0.6,0-1.1,0.1-1.3,0.1V62.2z"/>
							<polygon fill="#706F6F"  points="105.5,63.7 100.4,63.7 100.4,67 106.1,67 106.1,69.5 97.3,69.5 97.3,55.7 105.8,55.7 105.8,58.3 
								100.4,58.3 100.4,61.1 105.5,61.1 "/>
							<path fill="#706F6F"  d="M111.4,55.7v5.8h0.3l3.8-5.8h3.8l-4.8,6.1c1.8,0.5,2.6,1.7,3.2,3.3c0.5,1.5,0.9,3,1.6,4.4h-3.4
								c-0.5-0.9-0.7-2-1.1-3c-0.5-1.6-1.1-2.8-2.8-2.8h-0.6v5.8h-3.1V55.7H111.4z"/>
							<path fill="#706F6F"  d="M131.7,55.7v13.8h-3.1V58.3h-3.1v3.3c0,3.6-0.5,6.3-2.5,7.5c-0.7,0.4-1.6,0.7-2.8,0.7l-0.4-2.5
								c0.7-0.1,1.2-0.4,1.6-0.7c0.8-0.9,1-2.6,1-4.7v-6.1H131.7z"/>
							<path fill="#706F6F"  d="M137.7,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H137.7z M141.2,63.6l-0.9-2.9
								c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H141.2z"/>
							<path fill="#706F6F"  d="M159.6,64.2c-0.1-1.7-0.1-3.7-0.1-5.7h-0.1c-0.4,1.8-1,3.7-1.5,5.4l-1.7,5.4h-2.4l-1.5-5.3
								c-0.5-1.6-0.9-3.6-1.3-5.4h0c-0.1,1.9-0.1,4-0.2,5.7l-0.2,5.3h-2.9l0.9-13.8h4.2l1.4,4.6c0.4,1.6,0.9,3.3,1.2,4.9h0.1
								c0.4-1.6,0.9-3.4,1.3-5l1.5-4.6h4.1l0.8,13.8h-3.1L159.6,64.2z"/>
							<path fill="#706F6F"  d="M168.5,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H168.5z M172,63.6l-0.9-2.9
								c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H172z"/>
						</svg>
					</a>
					<p class="copyright">
						&copy; 2009 - <?php echo date("Y"); ?> «Комус-реклама» г. Москва, ул Маленковская, д. 32, стр.3  
					</p>
				</div>
				<div class="main-footer__contacts">
					<a href="mailto:reklama@komus.net" class="main-footer__email"><span>reklama@komus.net</span></a>
					<a href="tel:84952252301" class="main-footer__phone">8 (495) 225-23-01</a>
				</div>
				<ul class="main-footer__list main-footer__list--1">
					<li>
						<a href="#">О компании</a>
					</li>
					<li>
						<a href="#">Вакансии</a>
					</li>
					<li>
						<a href="#">Госзаказчикам</a>
					</li>
					<li>
						<a href="#">Акции</a>
					</li>
					<li>
						<a href="#">Новости</a>
					</li>
					<li>
						<a href="#">Контакты</a>
					</li>
				</ul>
				<ul class="main-footer__list main-footer__list--2">
					<li>
						<a href="#">Информация для рекламных агентств</a>
					</li>
					<li>
						<a href="#">Документы</a>
					</li>
					<li>
						<a href="#">Контаргентам</a>
					</li>
					<li>
						<a href="#">Тех.требования к предоставляемым материалам</a>
					</li>
					<li>
						<a href="#">Нанесения</a>
					</li>
				</ul>
			</div>
			<div class="main-footer__social">
				<span class="strong main-footer__social-title">Мы в соц.сетях:</span>
				<ul class="main-footer__social-list">
					<li>
						<a href="#" class="icon-tw main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-yt main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-vk main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-gplus main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-fb main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-lj main-footer__social-link"></a>
					</li>
					<li>
						<a href="#" class="icon-linkedin main-footer__social-link"></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-footer__bottom">
		<div class="content maxWidth">
			<div class="main-footer__col">
				<div class="main-footer__bottom-item">
					<i class="ic ic-1"></i>
					<div class="text">
						<a href="http://www.komus.ru" target="_blank">www.komus.ru</a>
						<p>Интернет-магазин <br> «Комус»</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-5"></i>
					<div class="text">
						<a href="http://www.komus-med.ru" target="_blank">www.komus-med.ru</a>
						<p>Товары медицинского <br> назначения</p>
					</div>
				</div>
	
				<div class="main-footer__bottom-item">
					<i class="ic ic-2"></i>
					<div class="text">
						<a href="http://www.komus-upakovka.ru" target="_blank">www.komus-upakovka.ru</a>
						<p>Полимерная упаковка <br> и материалы</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-6"></i>
					<div class="text">
						<a href="http://www.komus.org" target="_blank">www.komus.org</a>
						<p>Корпоративная жизнь <br> компании</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-3"></i>
					<div class="text">
						<a href="http://www.komus-reklama.ru" target="_blank">www.komus-reklama.ru</a>
						<p>Рекламно-сувенирная <br> продукция и полиграфия</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-7"></i>
					<div class="text">
						<a href="http://www.komus-contact.ru" target="_blank">www.komus-contact.ru</a>
						<p>Аутсорсинговый <br> контакт-центр</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-4"></i>
					<div class="text">
						<a href="http://www.bumaga-komus.ru" target="_blank">www.bumaga-komus.ru</a>
						<p>Полиграфические сорта <br> бумаги и картона</p>
					</div>
				</div>

				<div class="main-footer__bottom-item">
					<i class="ic ic-8"></i>
					<div class="text">
						<a href="http://www.komus-opt.ru" target="_blank">www.komus-opt.ru</a>
						<p>Оптовые продажи</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="popUpForm hidden-block js-requestCall">
	<div class="popUpForm__block">
		<a href="#" class="popUpForm__close js-close-form"></a>
		<div class="container-fluid">
			<div class="row">
				<div class="popUpForm__container">
					<form>
						<input class="formName" type="hidden" name="">
						<span class="popUpForm__title">Заказать звонок</span>
						<div class="popUpForm__field formField js-formField">
							<input
								type="text"
								name="text"
								placeholder="Имя" 
								data-validation="required"
								data-error-msg="Заполните поле «Имя»"
								data-validation-error-msg=" "
							/>
							<ins class="popUpForm__required">*</ins>
						</div>
						<div class="popUpForm__field formField js-formField">
							<input
								type="text"
								name="text-name"
								class="js-phoneInput"
								placeholder="Номер телефона" 
								data-validation="required" 
								data-error-msg="Заполните поле «Телефон»"
								data-validation-error-msg=" "
							/>
							<ins class="popUpForm__required">*</ins>
						</div>
						<div class="popUpForm__field formField">
							<textarea placeholder="Комментарий" rows="2"></textarea>
						</div>
						<div class="popUpForm__field formField">
							<button class="button redButton" data-animation="ripple">Отправить</button>
						</div>
						<div class="popUpForm__field formField">
							<label class="checkbox">
								<input type="checkbox" name="">
								<span class="checkbox__button"></span>
								<ins class="checkbox__text">Я&nbsp;даю согласие на&nbsp;хранение и&nbsp;обработку своих персональных данных. <a href="#">Соглашение.</a></ins>
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="popUpForm popUpForm--menu hidden-block js-headerMenu">
	<div class="popUpForm__block">
		<a href="#" class="popUpForm__close js-close-form"></a>
		<div class="container-fluid">
			<div class="row">
				<div class="popUpForm__container">
					<span class="popUpForm__title main-header__menu-btn">
						меню
					</span>
					<ul class="popUpForm__menu-list">
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Каталог</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Портфолио</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Нанесение</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Акции</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Оплата и доставка</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">О компании</a>
						</li>
						<li class="popUpForm__menu-item">
							<a href="#" class="button resetButton" data-animation="ripple">Контакты</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="popUpForm popUpForm--search hidden-block js-headerSearch">
	<div class="popUpForm__block">
		<a href="#" class="popUpForm__close js-close-form"></a>
		<div class="container-fluid">
			<div class="row">
				<div class="popUpForm__container">
					<span class="popUpForm__title">
						Поиск
					</span>
					<div class="popUpForm__search-box"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="popUpForm hidden-block js-catalogFilter catalogFilter not-center">
	<div class="popUpForm__block">
		<a href="#" class="popUpForm__close js-close-form"></a>
		<div class="container-fluid">
			<div class="row">
				<div class="popUpForm__container">
					<span class="popUpForm__title">
						Фильтр
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="js/libs/modernizr.min.js"></script>
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/jquery.blockUI.js"></script>
<script src="js/libs/jquery.form-validator.min.js"></script>
<script src="js/libs/security.js"></script>
<script src="js/libs/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/libs/jquery.touchSwipe.min.js"></script>
<script src="js/libs/jquery.sumoselect.js"></script>
<script src="js/libs/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="js/libs/jquery.maskedinput.min.js"></script>
<script src="js/libs/jquery.jscrollpane.min.js"></script>
<script src="js/libs/slick.min.js"></script>
<script src="js/libs/jquery.fancybox.pack.js"></script>
<script src="js/libs/touchMyRipple.js"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script src="js/pop_up.js"></script>
<script src="js/script.js"></script>