<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/catalog_item.css" rel="stylesheet">
		<link href="css/catalog_list.css" rel="stylesheet">
		<link href="css/side_bar.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
								<div class="searcResult">
									<div class="searcResult__header">
										<span class="searcResult__title">Результаты поиска:</span>
										<span class="searcResult__result">Часы настольные</span>
									</div>
									<div class="searcResult__row">
										<div class="searcResult__search">
											<span class="searcResult__search-title">Найти другое</span>
											<div class="searcResult__field">
												<form>
													<input type="text" name="" value="Часы настольные">
													<button class="button searchButton" data-animation="ripple"><span>Искать</span></button>
												</form>
											</div>
										</div>
									</div>
									<div class="noResult">
										<span class="noResult__text">По вашему запросу ничего не найдено.<br>Скорректируйте запрос или перейдите в каталог.</span>
										<a href="#" class="button redButton" data-animation="ripple">Перейти в каталог</a>
										<img src="images/noresult/7-layers.png" class="noResult__img" alt=""/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/catalog_list.js"></script>
		</footer><!-- #footer -->
	</body>
</html>