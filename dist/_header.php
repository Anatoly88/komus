<div class="main-header__top">
	<div class="content maxWidth">
		<a href="#" class="main-header__logo">
			<svg class="siteLogo" version="1.1"
				xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 179.9 72.7"
				enable-background="new 0 0 179.9 72.7" xml:space="preserve">
				<path fill="#D20A11"  d="M59.3,8.5c-8.8,0-16.5,7.3-16.5,16.9c0,1.7,0.5,3.9,1.1,5.6l-7.1,6.2c-0.3,0.3-0.3,0.3-0.6,0
					c-4.9-5.1-9.3-10.7-13.7-16.3l9.3-11.8c0,1.7,2.2,5.6,6,6.2c3.3,0,5.5-1.7,6.6-3.4c1.7-2.8,1.7-6.7-0.6-9c-2.2-2.3-4.1-2.5-6.6-2.3
					c-2.5,0.3-3.8,1.1-6,3.4c-3.1,3.9-6.2,7.9-9.3,11.8V2.3l-20.3,9v37.2L22,38.9V26.5l13.7,15.2c0.6,0.6,0.6,0.6,1.1,0l32.9-29.3
					C66.5,9.6,62.6,8.5,59.3,8.5"/>
				<path fill="#D20A11"  d="M63.8,37.7c-3.3,0-5.9-2.6-5.9-5.9c0-3.3,2.6-5.9,5.9-5.9c3.2,0,5.9,2.7,5.9,5.9
					C69.7,35.1,67,37.7,63.8,37.7 M173.6,32.2c-6.6,7.3-16.5,8.5-21.4-0.6l12.1-13c-0.8,3.4-0.9,6.4,2.7,7.9c8.8,3.4,13.1-10.7,2.7-16.3
					c-8.6-4.8-18.9,0.3-22.5,8.5l-6-8.5H122L136,31.3l-2,2c0-3.4-3.3-6.2-6.6-6.2c-1.1,0-2.2,0.6-3.3,1.1l-12.1-18L98.9,24.8l-9.3-14.7
					L75.8,26.5c0-4.5-1.1-8.5-3.9-11.8l-24.7,22c3.3,3.4,7.1,4.9,12.1,5.1c4.7,0.2,9.3-1.6,12.6-5.6l8.2-9.6l9.3,14.7l13.2-14.7
					l9.3,14.7l8.8-9c0,4.4,1.7,8.9,6.6,9c2.8,0.1,4.4-1.1,6.6-2.8l12.1-13c0,9,7.1,16.3,16.5,16.3c6.6,0,12.1-4,14.8-9.6H173.6z"/>
				<path fill="#706F6F"  d="M85.4,55.9c1-0.2,2.3-0.3,4.2-0.3c1.9,0,3.3,0.4,4.2,1.1c0.9,0.7,1.5,1.8,1.5,3.2c0,1.4-0.5,2.5-1.3,3.3
					c-1.1,1-2.6,1.5-4.5,1.5c-0.4,0-0.8,0-1.1-0.1v5h-3.1V55.9z M88.5,62.2c0.3,0.1,0.6,0.1,1,0.1c1.7,0,2.7-0.8,2.7-2.3
					c0-1.3-0.9-2-2.4-2c-0.6,0-1.1,0.1-1.3,0.1V62.2z"/>
				<polygon fill="#706F6F"  points="105.5,63.7 100.4,63.7 100.4,67 106.1,67 106.1,69.5 97.3,69.5 97.3,55.7 105.8,55.7 105.8,58.3
					100.4,58.3 100.4,61.1 105.5,61.1 "/>
				<path fill="#706F6F"  d="M111.4,55.7v5.8h0.3l3.8-5.8h3.8l-4.8,6.1c1.8,0.5,2.6,1.7,3.2,3.3c0.5,1.5,0.9,3,1.6,4.4h-3.4
					c-0.5-0.9-0.7-2-1.1-3c-0.5-1.6-1.1-2.8-2.8-2.8h-0.6v5.8h-3.1V55.7H111.4z"/>
				<path fill="#706F6F"  d="M131.7,55.7v13.8h-3.1V58.3h-3.1v3.3c0,3.6-0.5,6.3-2.5,7.5c-0.7,0.4-1.6,0.7-2.8,0.7l-0.4-2.5
					c0.7-0.1,1.2-0.4,1.6-0.7c0.8-0.9,1-2.6,1-4.7v-6.1H131.7z"/>
				<path fill="#706F6F"  d="M137.7,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H137.7z M141.2,63.6l-0.9-2.9
					c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H141.2z"/>
				<path fill="#706F6F"  d="M159.6,64.2c-0.1-1.7-0.1-3.7-0.1-5.7h-0.1c-0.4,1.8-1,3.7-1.5,5.4l-1.7,5.4h-2.4l-1.5-5.3
					c-0.5-1.6-0.9-3.6-1.3-5.4h0c-0.1,1.9-0.1,4-0.2,5.7l-0.2,5.3h-2.9l0.9-13.8h4.2l1.4,4.6c0.4,1.6,0.9,3.3,1.2,4.9h0.1
					c0.4-1.6,0.9-3.4,1.3-5l1.5-4.6h4.1l0.8,13.8h-3.1L159.6,64.2z"/>
				<path fill="#706F6F"  d="M168.5,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H168.5z M172,63.6l-0.9-2.9
					c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H172z"/>
			</svg>
		</a>
		<div class="main-header__top-form"></div>
		<div class="main-header__links">
			<div class="main-header__contacts">
				<div class="main-header__contacts-links">
					<a href="mailto:reklama@komus.net" class="main-header__email"><span>reklama@komus.net</span></a>
					<a href="tel:84952252301" class="main-header__phone">8 (495) 225-23-01</a>
				</div>
				<a href="javascript:void(0);" class="main-header__callback-btn button redButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать звонок</a>
				<div class="main-header__buttons-mob">
					<a href="javascript:void(0);" class="main-header__callback-mob js-show-popup" data-form-class="requestCall"></a>
					<a href="javascript:void(0);" class="main-header__search-mob js-show-popup js-open-search-form" data-form-class="headerSearch"></a>
				</div>
			</div>
			<a href="#" class="main-header__basket">
				<div class="main-header__basket-ic">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
					<g>
						<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
							c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
							c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
							C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
							M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
						<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
							c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
						<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
							c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
					</g>
					</svg>
					<span class="main-header__basket-counter js-basket-counter">34</span>
				</div>
				<div class="main-header__basket-summary js-basket-summary strong">
					<span>222 290</span>
					<ins class="rub"></ins>
				</div>
			</a>
		</div>
	</div>
</div>
<div class="main-header__form js-headerForm headerForm">
	<div class="content maxWidth">
		<form>
				<div class="formField">
					<input
						type="text"
						name="text-name"
						placeholder="Ручка белая"
						data-validation-error-msg="Заполните поле"
						/>
				</div>
				<div class="formField">
					<input
						type="number"
						pattern="[0-9]*"
						name="text-name"
						placeholder="Тираж, шт"
						data-validation-error-msg="Заполните поле"
						/>
				</div>
				<div class="formField">
					<input
						type="number"
						pattern="[0-9]*"
						placeholder="Цена от, &#8381;"
						data-validation-error-msg="Заполните поле"
						/>
				</div>
				<div class="formField">
					<input
						type="number"
						pattern="[0-9]*"
						placeholder="Цена до, &#8381;"
						data-validation-error-msg="Заполните поле"
						/>
				</div>
				<a href="#" class="button searchButton" data-animation="ripple"><span>Искать</span></a>
				<button class="main-header__swim-search" type="submit"><span></span></button>
		</form>
	</div>
</div>
<nav class="main-header__menu">
	<div class="content maxWidth">
		<ul class="main-header__menu-list">
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Каталог</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Портфолио</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Нанесение</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Акции</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Оплата и доставка</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">О компании</a>
			</li>
			<li class="main-header__menu-item">
				<a href="#" class="main-header__menu-link">Контакты</a>
			</li>
		</ul>
		<a href="javascript:void(0);" class="main-header__menu-btn js-show-popup" data-form-class="headerMenu">меню</a>
	</div>
</nav>
<div class="main-header__swim">
	<div class="main-header__top">
		<a href="#" class="main-header__logo">
			<svg version="1.1" class="siteLogo"
				xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 179.9 72.7"
				enable-background="new 0 0 179.9 72.7" xml:space="preserve">
			<path fill="#D20A11"  d="M59.3,8.5c-8.8,0-16.5,7.3-16.5,16.9c0,1.7,0.5,3.9,1.1,5.6l-7.1,6.2c-0.3,0.3-0.3,0.3-0.6,0
				c-4.9-5.1-9.3-10.7-13.7-16.3l9.3-11.8c0,1.7,2.2,5.6,6,6.2c3.3,0,5.5-1.7,6.6-3.4c1.7-2.8,1.7-6.7-0.6-9c-2.2-2.3-4.1-2.5-6.6-2.3
				c-2.5,0.3-3.8,1.1-6,3.4c-3.1,3.9-6.2,7.9-9.3,11.8V2.3l-20.3,9v37.2L22,38.9V26.5l13.7,15.2c0.6,0.6,0.6,0.6,1.1,0l32.9-29.3
				C66.5,9.6,62.6,8.5,59.3,8.5"/>
			<path fill="#D20A11"  d="M63.8,37.7c-3.3,0-5.9-2.6-5.9-5.9c0-3.3,2.6-5.9,5.9-5.9c3.2,0,5.9,2.7,5.9,5.9
				C69.7,35.1,67,37.7,63.8,37.7 M173.6,32.2c-6.6,7.3-16.5,8.5-21.4-0.6l12.1-13c-0.8,3.4-0.9,6.4,2.7,7.9c8.8,3.4,13.1-10.7,2.7-16.3
				c-8.6-4.8-18.9,0.3-22.5,8.5l-6-8.5H122L136,31.3l-2,2c0-3.4-3.3-6.2-6.6-6.2c-1.1,0-2.2,0.6-3.3,1.1l-12.1-18L98.9,24.8l-9.3-14.7
				L75.8,26.5c0-4.5-1.1-8.5-3.9-11.8l-24.7,22c3.3,3.4,7.1,4.9,12.1,5.1c4.7,0.2,9.3-1.6,12.6-5.6l8.2-9.6l9.3,14.7l13.2-14.7
				l9.3,14.7l8.8-9c0,4.4,1.7,8.9,6.6,9c2.8,0.1,4.4-1.1,6.6-2.8l12.1-13c0,9,7.1,16.3,16.5,16.3c6.6,0,12.1-4,14.8-9.6H173.6z"/>
			<path fill="#706F6F"  d="M85.4,55.9c1-0.2,2.3-0.3,4.2-0.3c1.9,0,3.3,0.4,4.2,1.1c0.9,0.7,1.5,1.8,1.5,3.2c0,1.4-0.5,2.5-1.3,3.3
				c-1.1,1-2.6,1.5-4.5,1.5c-0.4,0-0.8,0-1.1-0.1v5h-3.1V55.9z M88.5,62.2c0.3,0.1,0.6,0.1,1,0.1c1.7,0,2.7-0.8,2.7-2.3
				c0-1.3-0.9-2-2.4-2c-0.6,0-1.1,0.1-1.3,0.1V62.2z"/>
			<polygon fill="#706F6F"  points="105.5,63.7 100.4,63.7 100.4,67 106.1,67 106.1,69.5 97.3,69.5 97.3,55.7 105.8,55.7 105.8,58.3
				100.4,58.3 100.4,61.1 105.5,61.1 "/>
			<path fill="#706F6F"  d="M111.4,55.7v5.8h0.3l3.8-5.8h3.8l-4.8,6.1c1.8,0.5,2.6,1.7,3.2,3.3c0.5,1.5,0.9,3,1.6,4.4h-3.4
				c-0.5-0.9-0.7-2-1.1-3c-0.5-1.6-1.1-2.8-2.8-2.8h-0.6v5.8h-3.1V55.7H111.4z"/>
			<path fill="#706F6F"  d="M131.7,55.7v13.8h-3.1V58.3h-3.1v3.3c0,3.6-0.5,6.3-2.5,7.5c-0.7,0.4-1.6,0.7-2.8,0.7l-0.4-2.5
				c0.7-0.1,1.2-0.4,1.6-0.7c0.8-0.9,1-2.6,1-4.7v-6.1H131.7z"/>
			<path fill="#706F6F"  d="M137.7,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H137.7z M141.2,63.6l-0.9-2.9
				c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H141.2z"/>
			<path fill="#706F6F"  d="M159.6,64.2c-0.1-1.7-0.1-3.7-0.1-5.7h-0.1c-0.4,1.8-1,3.7-1.5,5.4l-1.7,5.4h-2.4l-1.5-5.3
				c-0.5-1.6-0.9-3.6-1.3-5.4h0c-0.1,1.9-0.1,4-0.2,5.7l-0.2,5.3h-2.9l0.9-13.8h4.2l1.4,4.6c0.4,1.6,0.9,3.3,1.2,4.9h0.1
				c0.4-1.6,0.9-3.4,1.3-5l1.5-4.6h4.1l0.8,13.8h-3.1L159.6,64.2z"/>
			<path fill="#706F6F"  d="M168.5,66l-1,3.6h-3.2l4.2-13.8h4.1l4.3,13.8h-3.4l-1.1-3.6H168.5z M172,63.6l-0.9-2.9
				c-0.2-0.8-0.5-1.8-0.7-2.7h0c-0.2,0.8-0.4,1.9-0.6,2.7l-0.8,2.9H172z"/>
			</svg>
		</a>
		<a href="javascript:void(0);" class="main-header__menu-btn js-show-popup" data-form-class="headerMenu">меню</a>
		<nav class="main-header__swim-menu">
			<a href="#" class="main-header__swim-menu-link">Каталог</a>
			<a href="#" class="main-header__swim-menu-link">контакты</a>
		</nav>
		<div class="main-header__links">
			<div class="main-header__contacts">
				<div class="main-header__contacts-links">
					<a href="mailto:reklama@komus.net" class="main-header__email"><span>reklama@komus.net</span></a>
					<a href="tel:84952252301" class="main-header__phone">8 (495) 225-23-01</a>
				</div>
				<a href="javascript:void(0);" class="main-header__callback-btn button redButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать звонок</a>
				<a href="#" class="main-header__search-btn button searchButton js-show-search-form" data-animation="ripple"><span>Поиск</span></a>
				<div class="main-header__buttons-mob">
					<a href="javascript:void(0);" class="main-header__callback-mob js-show-popup" data-form-class="requestCall"></a>
					<a href="#" class="main-header__search-mob js-show-search-form"></a>
				</div>
			</div>
			<a href="#" class="main-header__basket">
				<div class="main-header__basket-ic">
					<svg version="1.1" id="basket" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
					<g>
						<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
							c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
							c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
							C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
							M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
						<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
							c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
						<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
							c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
					</g>
					</svg>
					<span class="main-header__basket-counter js-basket-counter">34</span>
				</div>
				<span class="main-header__basket-summary js-basket-summary strong"><span>222 290</span> <ins class="rub"></ins></span>
			</a>
		</div>
	</div>
	<div class="main-header__swim-form"></div>
</div>
<?if($breadcrumbs):?>
<div class="breadcrumb">
	<div class="content maxWidth">
		<ul class="breadcrumb__list">
			<li class="breadcrumb__item">
				<a href="#">Главная</a>
			</li>
			<li class="breadcrumb__item">
				<a href="#">О компании</a>
			</li>
			<li class="breadcrumb__item">
				<span>Наши партнеры</span>
			</li>
		</ul>
	</div>
</div>
<?endif?>
