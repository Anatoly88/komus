<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/order.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>Оформление заказа</h1>
							<div class="orderSuccessful">
								<div class="orderSuccessful__block">
									<div class="orderSuccessful__text">
										<span class="orderSuccessful__order">Ваш <span class="orderSuccessful__numberorder">заказ №139</span> успешно оформлен!</span>
										<span class="orderSuccessful__infotext">В ближайшее время наш менеджер свяжется с вами.</span>
										<a href="#" class="button redButton" data-animation="ripple">Вернуться на главную</a>
										<img class="orderSuccessful__img" src="images/order/16-layers.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
		</footer><!-- #footer -->
	</body>
</html>