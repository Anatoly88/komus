<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/order.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>Оформление заказа</h1>
							<form class="orderForm">
								<div class="stepBlock personalData">
									<span class="stepBlock__number">1</span>
									<span class="stepBlock__title">Личные данные</span>
									<div class="personalData__block">
										<div class="formField personalData__field js-formField">
											<input
												type="text"
												name="text"
												placeholder="Имя" 
												data-validation="required"
												data-error-msg="Заполните поле"
												data-validation-error-msg=" "
											/>
											<ins class="personalData__required">*</ins>
										</div>
										<div class="formField personalData__field js-formField">
											<input
												type="text"
												name="text-name"
												class="js-phoneInput"
												placeholder="Телефон" 
												data-validation="required" 
												data-error-msg="Заполните поле"
												data-validation-error-msg=" "
											/>
											<ins class="personalData__required">*</ins>
										</div>
										<div class="formField personalData__field js-formField">
											<input
												type="text"
												name="text-email"
												placeholder="Email" 
												data-validation="email" 
												data-error-msg="Заполните поле"
												data-validation-error-msg=" "
											/>
											<ins class="personalData__required">*</ins>
										</div>
									</div>
								</div>
								<div class="stepBlock waysDelivery">
									<span class="stepBlock__number">2</span>
									<span class="stepBlock__title">Способы доставки</span>
									<div class="waysDelivery__block">
										<div class="waysDelivery__radio">
											<label class="radio">
												<input type="radio" name="1" class="js-waysDelivery__radio">
												<span class="radio__button"></span>
												<ins class="radio__text">Самовывоз</ins>
											</label>
											<label class="radio">
												<input type="radio" name="1" class="js-waysDelivery__radio js-waysDelivery__courier">
												<span class="radio__button"></span>
												<ins class="radio__text">Курьерская доставка</ins>
											</label>
										</div>
										<div class="waysDelivery__courierBlock js-waysDelivery__courierBlock">
											<div class="waysDelivery__field formField js-formField">
												<input
													type="text"
													name="text"
													placeholder="Адрес" 
													data-validation="required"
													data-error-msg="Заполните поле"
													data-validation-error-msg=" "
												/>
												<ins class="personalData__required">*</ins>
											</div>
										</div>
									</div>
								</div>
								<div class="stepBlock requisites">
									<span class="stepBlock__number">3</span>
									<span class="stepBlock__title">Реквизиты</span>
									<div class="requisites__block">
										<span class="requisites__info">Прикрепите реквизиты для формирования счета на оплату:</span>
										<div class="requisites__upload uploadBlock">
											<label class="uploadFile">
												<input type="file" name="">
												<span>Прикрепить документ</span>
											</label>
											<div class="uploadFile__document">
												<span class="uploadFile__title">Добавленные документы:</span>
												<div class="uploadFile__item">
													<span class="uploadFile__item-text">Название_документа.doc</span>
													<a href="javascript:void(0);" class="uploadFile__item-close"></a>
												</div>
												<div class="uploadFile__item">
													<span class="uploadFile__item-text">Название_документа.doc</span>
													<a href="javascript:void(0);" class="uploadFile__item-close"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="stepBlock orderList">
									<span class="stepBlock__number">4</span>
									<span class="stepBlock__title">Состав заказа</span>
									<div class="orderList__block">
										<div class="orderList__header">
											<span class="orderList__num">№</span>
											<span class="orderList__photo">Фото</span>
											<span class="orderList__name">Наименование / Артикул / Параметры <ins class="orderList__name-application">/ Нанесение</ins></span>
											<span class="orderList__price">Цена</span>
											<span class="orderList__application">Стоимость нанесения</span>
											<span class="orderList__amount">Кол-во</span>
											<span class="orderList__app">Нанесение</span>
											<span class="orderList__total">Стоимость</span>
										</div>
										<div class="orderItem">
											<div class="orderItem__number">1.</div>
											<div class="orderItem__img"><img src="media/1-19733618.png" alt=""/></div>
											<div class="orderItem__text">
												<span class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</span>
												<span class="orderItem__article">Артикул: 5645454</span>
												<div class="orderItem__option">
													<div class="orderItem__option-block">
														<span class="orderItem__option-title">Параметры</span>
														<span class="orderItem__option-item">Минимальный тираж:  3</span>
														<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
													</div>
													<div class="orderItem__option-block">
														<span class="orderItem__option-item application">
															<span class="orderItem__option-title">Нанесение:</span> Гравировка
														</span>
													</div>
												</div>
											</div>
											<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
											<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
											<div class="orderItem__amount">5</div>
											<div class="orderItem__app">Гравировка</div>
											<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
											<div class="orderPrice">
												<div class="orderPrice__title">
													<span>Цена</span>
													<span>Стоимость нанесения</span>
													<span>Кол-во</span>
													<span>Стоимость</span>
												</div>
												<div class="orderPrice__price">
													<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
													<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
													<span>5</span>
													<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
												</div>
											</div>
										</div>
										<div class="orderItem">
											<div class="orderItem__number">2.</div>
											<div class="orderItem__img"><img src="media/1-19733618.png" alt=""/></div>
											<div class="orderItem__text">
												<span class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</span>
												<span class="orderItem__article">Артикул: 5645454</span>
												<div class="orderItem__option">
													<div class="orderItem__option-block">
														<span class="orderItem__option-title">Параметры</span>
														<span class="orderItem__option-item">Минимальный тираж:  3</span>
														<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
													</div>
													<div class="orderItem__option-block">
														<span class="orderItem__option-item application">
															<span class="orderItem__option-title">Нанесение:</span> Гравировка
														</span>
													</div>
												</div>
											</div>
											<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
											<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
											<div class="orderItem__amount">5</div>
											<div class="orderItem__app">Гравировка</div>
											<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
											<div class="orderPrice">
												<div class="orderPrice__title">
													<span>Цена</span>
													<span>Стоимость нанесения</span>
													<span>Кол-во</span>
													<span>Стоимость</span>
												</div>
												<div class="orderPrice__price">
													<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
													<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
													<span>5</span>
													<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
												</div>
											</div>
										</div>
										<div class="orderItem">
											<div class="orderItem__number">3.</div>
											<div class="orderItem__img"><img src="media/1-19733618.png" alt=""/></div>
											<div class="orderItem__text">
												<span class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</span>
												<span class="orderItem__article">Артикул: 5645454</span>
												<div class="orderItem__option">
													<div class="orderItem__option-block">
														<span class="orderItem__option-title">Параметры</span>
														<span class="orderItem__option-item">Минимальный тираж:  3</span>
														<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
													</div>
													<div class="orderItem__option-block">
														<span class="orderItem__option-item application">
															<span class="orderItem__option-title">Нанесение:</span> Гравировка
														</span>
													</div>
												</div>
											</div>
											<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
											<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
											<div class="orderItem__amount">5</div>
											<div class="orderItem__app">Гравировка</div>
											<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
											<div class="orderPrice">
												<div class="orderPrice__title">
													<span>Цена</span>
													<span>Стоимость нанесения</span>
													<span>Кол-во</span>
													<span>Стоимость</span>
												</div>
												<div class="orderPrice__price">
													<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
													<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
													<span>5</span>
													<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
												</div>
											</div>
										</div>
										<div class="orderItem">
											<div class="orderItem__number">4.</div>
											<div class="orderItem__img"><img src="media/1-19733618.png" alt=""/></div>
											<div class="orderItem__text">
												<span class="orderItem__name">Складные часы-будильник в бархатном чехле «Pisa» очень длинное название в 3 строки ...</span>
												<span class="orderItem__article">Артикул: 5645454</span>
												<div class="orderItem__option">
													<div class="orderItem__option-block">
														<span class="orderItem__option-title">Параметры</span>
														<span class="orderItem__option-item">Минимальный тираж:  3</span>
														<span class="orderItem__option-item">Размер:  67 x 67 x 20 мм.</span>
													</div>
													<div class="orderItem__option-block">
														<span class="orderItem__option-item application">
															<span class="orderItem__option-title">Нанесение:</span> Гравировка
														</span>
													</div>
												</div>
											</div>
											<div class="orderItem__price">3 920.0<ins class="rub"></ins></div>
											<div class="orderItem__priceApplication">1 100.0<ins class="rub"></ins></div>
											<div class="orderItem__amount">5</div>
											<div class="orderItem__app">Гравировка</div>
											<div class="orderItem__total">25 100.0<ins class="rub"></ins></div>
											<div class="orderPrice">
												<div class="orderPrice__title">
													<span>Цена</span>
													<span>Стоимость нанесения</span>
													<span>Кол-во</span>
													<span>Стоимость</span>
												</div>
												<div class="orderPrice__price">
													<span class="orderPrice__price-price">3 920.0<ins class="rub"></ins></span>
													<span class="orderPrice__price-price application">1 100.0<ins class="rub"></ins></span>
													<span>5</span>
													<span class="orderPrice__price-price">25 100.0<ins class="rub"></ins></span>
												</div>
											</div>
										</div>
									</div>
									<div class="orderLastBlock">
										<div class="orderLastBlock__textarea formField">
											<textarea placeholder="Комментарий"></textarea>
										</div>
										<div class="orderLastBlock__checkbox">
											<label class="checkbox">
												<input type="checkbox" name="">
												<span class="checkbox__button"></span>
												<ins class="checkbox__text">Согласен с политикой конфиденциальности и пользовательским<br><a href="#"> cоглашением.</a></ins>
											</label>
										</div>
										<div class="orderLastBlock__total">
											<div class="orderLastBlock__totalPrice">
												<span class="orderLastBlock__total-title">ИТОГО:</span>
												<span class="orderLastBlock__total-prices">58 040.0<ins class="rub"></ins></span>
											</div>
											<div class="orderLastBlock__button">
												<button class="button redButton" data-animation="ripple">Оформить заказ</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/order.js"></script>
		</footer><!-- #footer -->
	</body>
</html>