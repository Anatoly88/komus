<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/side_bar.css" rel="stylesheet">
	<link href="css/printing.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="one-print">
				<div class="content maxWidth">
					<h1>Тампопечать</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<nav class="menuSideBar js-menuSideBar-sticky">
								<span class="menuSideBar__mobile js-menuSideBar-mobile">Тампопечать</span>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">Тампопечать</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Тиснение</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Офсетная печать</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Цифровая печать</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Лазерная гравировка</a>
									</li>

									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Шелкография</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Термотрансфер</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Сублимационная печать</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Круговая шелкография</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Деколь</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Плоттерная резка</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Вышивка</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Гравировка на шильде</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Заливка полимерной смолой</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Рельефные наклейки</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Ризограф</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Изготовление печатей</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">УФ-печать</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Шеврон</a>
									</li>
								</menu>
							</nav>
						</div>
						<div class="sideBar__right">
							<div class="one-print__left">
								<div class="one-print__gallery js-sync-slider">
									<div class="one-print__once-slider js-once-slider">
										<div class="slide">
											<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
												<img src="./media/printing_img_1.jpg" alt="">
											</a>
										</div>
										<div class="slide">
											<a href="./images/interviewblock/1280-1919.jpg" rel="group" class="js-fancybox slide__pic">
												<img src="./media/printing_img_2.jpg" alt="">
											</a>
										</div>
										<div class="slide">
											<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
												<img src="./media/printing_img_3.jpg" alt="">
											</a>
										</div>
										<div class="slide">
											<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
												<img src="./media/-mg-4897------.jpg" alt="">
											</a>
										</div>
									</div>
									<h4>Фото</h4>
									<div class="one-print__several-slider js-several-slider">
										<div class="slide">
											<div class="slide__pic">
												<img src="./media/printing_img_1.jpg" alt="">
											</div>
										</div>
										<div class="slide">
											<div class="slide__pic">
												<img src="./media/printing_img_2.jpg" alt="">
											</div>
										</div>
										<div class="slide">
											<div class="slide__pic">
												<img src="./media/printing_img_3.jpg" alt="">
											</div>
										</div>
										<div class="slide">
											<div class="slide__pic">
												<img src="./media/-mg-4897------.jpg" alt="">
											</div>
										</div>
									</div>
								</div>
								<div class="one-print__contacts">
									<div class="importantInformation">
										<span class="importantInformation__text">Получить более подробную информацию Вы можете в оттделе контрактных продаж</span>
										<span class="importantInformation__contact">Телефон: (495) 685-95-10<br>E-mail: tender02@komus.net</span>
									</div>
								</div>
							</div>
							<div class="one-print__right">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel enim a eros consequat condimentum. Suspendisse potenti.
									Morbi ut tincidunt quam, sed consectetur leo. Etiam nulla velit, maximus quis nisl nec, pharetra dignissim neque.
									Etiam accumsan feugiat dolor id facilisis. Integer tristique tellus dolor, lacinia interdum metus faucibus interdum.
									Cras pretium, ante id vulputate tristique, tortor orci feugiat lectus, eu pharetra arcu tellus eu erat. Integer
									ut interdum lorem, in egestas lacus. Sed ullamcorper lorem ac libero facilisis, in feugiat est feugiat. Pellentesque
									egestas sem nulla, eget placerat purus pellentesque sed. Aliquam sed justo volutpat, elementum quam quis, dapibus
									ex. Sed tristique ex eget venenatis facilisis. Integer tempus porttitor feugiat. Aliquam quis ante at quam convallis
									malesuada sit amet vitae orci.
								</p>
								<p>Praesent ante tellus, gravida vel egestas sed, vestibulum ut ante. Duis dapibus mi sit amet dictum feugiat. Vivamus
									consectetur lacus dictum tellus elementum, sit amet pretium arcu fermentum. Sed accumsan ex vitae nulla tristique
									fermentum. Duis ultricies ex in convallis pretium. Sed a mollis turpis. Duis tincidunt neque vestibulum mauris mollis,
									sit amet vulputate erat pretium. Aenean gravida ipsum eget commodo imperdiet. Ut placerat efficitur dolor id accumsan.
									In blandit tortor non dictum semper.</p>
								<p>Donec vulputate sapien neque, nec viverra odio scelerisque nec. Integer vel nisl erat. Vivamus dictum ex pretium
									elit bibendum, a vulputate risus ornare. In pellentesque fermentum varius. Nulla malesuada sem ac erat rutrum, et
									ornare justo dapibus. Quisque quis neque lorem. Ut consequat eleifend rhoncus. Vestibulum vestibulum blandit enim,
									at rhoncus eros rhoncus ut. Vivamus urna purus, imperdiet cursus sapien sagittis, fringilla varius lorem.</p>
								<p>Aliquam at massa ullamcorper, ultricies quam vitae, pulvinar mi. Praesent sit amet libero id enim molestie bibendum
									eu porttitor tellus. Nam tempus lobortis scelerisque. Quisque accumsan faucibus nulla. Nullam eget ex rhoncus, facilisis
									ligula vitae, cursus ante. Nunc quis finibus augue. Nulla vel aliquet risus. Cras iaculis ultrices malesuada. Maecenas
									sodales dictum ex, vitae luctus quam aliquam at. Fusce mattis suscipit tellus, in suscipit sem malesuada vitae.
									Curabitur molestie molestie ipsum sed viverra. Vestibulum id facilisis neque.</p>
								<a href="#" class="button greenButton">Вернуться к списку нанесений</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/one_print_slider.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>