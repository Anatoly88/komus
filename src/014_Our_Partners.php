<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/partners.css" rel="stylesheet">
	<link href="css/our_slider.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="our-partners js-tabsblock">
				<div class="content maxWidth">
					<h1>Партнеры</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<nav class="menuSideBar js-menuSideBar-sticky">
								<span class="menuSideBar__mobile js-menuSideBar-mobile">Наши партнеры</span>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">О нас</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Портфолио</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">Наши партнеры</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Гос. заказчикам</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Вакансии</a>
									</li>
								</menu>
							</nav>
						</div>
						<div class="sideBar__right">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel enim a eros consequat condimentum. Suspendisse potenti. Morbi ut tincidunt quam, sed consectetur leo. Etiam nulla velit, maximus quis nisl nec, pharetra dignissim neque. Etiam accumsan feugiat dolor id facilisis. Integer tristique tellus dolor, lacinia interdum metus faucibus interdum. Cras pretium, ante id vulputate tristique, tortor orci feugiat lectus, eu pharetra arcu tellus eu erat. Integer ut interdum lorem, in egestas lacus. Sed ullamcorper lorem ac libero facilisis, in feugiat est feugiat. Pellentesque egestas sem nulla, eget placerat purus pellentesque sed. Aliquam sed justo volutpat, elementum quam quis, dapibus ex. Sed tristique ex eget venenatis facilisis. Integer tempus porttitor feugiat. Aliquam quis ante at quam convallis malesuada sit amet vitae orci.
							</p>
							<p>
								Praesent ante tellus, gravida vel egestas sed, vestibulum ut ante. Duis dapibus mi sit amet dictum feugiat. Vivamus consectetur lacus dictum tellus elementum, sit amet pretium arcu fermentum. Sed accumsan ex vitae nulla tristique fermentum. Duis ultricies ex in convallis pretium. Sed a mollis turpis. Duis tincidunt neque vestibulum mauris mollis, sit amet vulputate erat pretium. Aenean gravida ipsum eget commodo imperdiet. Ut placerat efficitur dolor id accumsan. In blandit tortor non dictum semper.
							</p>
							<p>
								Donec vulputate sapien neque, nec viverra odio scelerisque nec. Integer vel nisl erat. Vivamus dictum ex pretium elit bibendum, a vulputate risus ornare. In pellentesque fermentum varius. Nulla malesuada sem ac erat rutrum, et ornare justo dapibus. Quisque quis neque lorem. Ut consequat eleifend rhoncus. Vestibulum vestibulum blandit enim, at rhoncus eros rhoncus ut. Vivamus urna purus, imperdiet cursus sapien sagittis, fringilla varius lorem.
							</p>
							<p>
								Aliquam at massa ullamcorper, ultricies quam vitae, pulvinar mi. Praesent sit amet libero id enim molestie bibendum eu porttitor tellus. Nam tempus lobortis scelerisque. Quisque accumsan faucibus nulla. Nullam eget ex rhoncus, facilisis ligula vitae, cursus ante. Nunc quis finibus augue. Nulla vel aliquet risus. Cras iaculis ultrices malesuada. Maecenas sodales dictum ex, vitae luctus quam aliquam at. Fusce mattis suscipit tellus, in suscipit sem malesuada vitae. Curabitur molestie molestie ipsum sed viverra. Vestibulum id facilisis neque.</p>
							<div class="ourBlock">
								<div class="ourBlock__block">
									<div class="ourSlider js-ourPartnersSlider greenArrow">
										<a href="#" class="ourSlider__item">
											<img src="media/-----20.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/aeroflot-russian-airlines-logo.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/logo-euroset.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/-------.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/gazprom-logo.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/smi-sber-logo-vsegda-695.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/------------.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/logo-rostelecom-v2.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/-----20.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/aeroflot-russian-airlines-logo.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/logo-euroset.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/-------.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/gazprom-logo.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/smi-sber-logo-vsegda-695.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/------------.png" alt="" />
										</a>
										<a href="#" class="ourSlider__item">
											<img src="media/logo-rostelecom-v2.png" alt="" />
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/our_slider.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>