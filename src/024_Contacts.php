<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/template_styles.css" rel="stylesheet">
		<link href="css/contacts.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="contacts js-tabsblock">
					<div class="content maxWidth">
						<h1>Контакты</h1>
						<aside class="contacts-aside">
							<div class="sideBar__left">
								<nav class="menuSideBar">
									<span class="menuSideBar__mobile js-menuSideBar-mobile">Москва</span>
									<menu class="menuSideBar__list js-menuSideBar">
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="moscow" class="menuSideBar__link js-menuSideBar-link js-tab-link current">Москва</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="saint-petersburg" class="menuSideBar__link js-menuSideBar-link js-tab-link">Санкт-Петербург</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="volgograd" class="menuSideBar__link js-menuSideBar-link js-tab-link">Волгоград</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="ekaterinburg" class="menuSideBar__link js-menuSideBar-link js-tab-link">Екатеринбург</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="kazan" class="menuSideBar__link js-menuSideBar-link js-tab-link">Казань</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="krasnodar" class="menuSideBar__link js-menuSideBar-link js-tab-link">Краснодар</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="nizhniy-novdorod" class="menuSideBar__link js-menuSideBar-link js-tab-link">Нижний Новгород</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="perm" class="menuSideBar__link js-menuSideBar-link js-tab-link">Пермь</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="rostov-on-don" class="menuSideBar__link js-menuSideBar-link js-tab-link">Ростов-на-Дону</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="tula" class="menuSideBar__link js-menuSideBar-link js-tab-link">Тула</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="ufa" class="menuSideBar__link js-menuSideBar-link js-tab-link">Уфа</a>
										</li>
										<li class="menuSideBar__item">
											<a href="javascript:void(0);" data-href="chelyabinsk" class="menuSideBar__link js-menuSideBar-link js-tab-link">Челябинск</a>
										</li>
									</menu>
								</nav>
							</div>
						</aside>
						<section class="contacts-city js-tab__moscow js-tab">
							<span class="contacts-title">
								Москва
							</span>
							<div class="contacts-city__city">
								<div class="contacts-list">
									<article class="contacts-item">
										<span class="contacts-item__title">Центральный офис продаж</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span> г. Москва, ул Маленковская, д. 32, стр.3 </span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(495) 225-23-01</span>
										</div>
										<div class="contacts-item__email">
											<span>E-mail:</span> 
											<a href="mailto:reklama@komus.net">reklama@komus.net</a> 
										</div>
										<svg version="1.1" class="contacts-item__icon ic-co" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 35.667 43.345" style="enable-background:new 0 0 35.667 43.345;" xml:space="preserve">
											<g>
												<g>
													<g>
														<path style="fill:#D10910;" d="M25.407,4.796c-5.679-5.676-14.886-5.676-20.566,0C-0.276,9.913-0.851,19.55,3.493,25.339
															l11.633,16.797l11.633-16.797C31.099,19.55,30.524,9.913,25.407,4.796L25.407,4.796z M15.267,19.738
															c-2.653,0-4.801-2.149-4.801-4.801c0-2.649,2.148-4.801,4.801-4.801c2.652,0,4.8,2.152,4.8,4.801
															C20.067,17.589,17.919,19.738,15.267,19.738L15.267,19.738z M15.267,19.738"/>
													</g>
													<g>
														<path style="fill:#FFFFFF;" d="M34.614,33.789c0,4.863-3.938,8.801-8.797,8.801c-4.863,0-8.801-3.938-8.801-8.801
															c0-4.859,3.938-8.797,8.801-8.797C30.676,24.992,34.614,28.93,34.614,33.789L34.614,33.789z M34.614,33.789"/>
														<path style="fill:#D10910;" d="M25.817,43.089c-5.129,0-9.302-4.172-9.302-9.301c0-5.126,4.173-9.297,9.302-9.297
															c5.126,0,9.297,4.171,9.297,9.297C35.114,38.917,30.943,43.089,25.817,43.089z M25.817,25.492c-4.577,0-8.302,3.722-8.302,8.297
															c0,4.577,3.725,8.301,8.302,8.301c4.575,0,8.297-3.724,8.297-8.301C34.114,29.213,30.393,25.492,25.817,25.492z"/>
													</g>
													<path style="fill:#D10910;" d="M28.376,33.339"/>
													<g>
														<path style="fill:#3B3B3B;" d="M21.174,36.272v-5.428h1.224v4.395h1.715v-4.395h1.223v4.395h0.666v0.766l-0.108,1.831h-1.099
															v-1.565H21.174z"/>
														<path style="fill:#3B3B3B;" d="M28.866,36.371c-0.366,0-0.7-0.065-1.003-0.195c-0.303-0.131-0.562-0.318-0.778-0.562
															c-0.217-0.244-0.384-0.541-0.504-0.891c-0.119-0.35-0.179-0.746-0.179-1.19c0-0.444,0.06-0.839,0.179-1.186
															c0.12-0.347,0.287-0.638,0.504-0.874c0.216-0.236,0.476-0.416,0.778-0.541c0.302-0.125,0.637-0.187,1.003-0.187
															c0.367,0,0.701,0.062,1.003,0.187c0.303,0.125,0.562,0.307,0.778,0.545c0.217,0.238,0.384,0.532,0.504,0.878
															c0.119,0.348,0.179,0.74,0.179,1.178c0,0.444-0.06,0.841-0.179,1.19c-0.12,0.35-0.287,0.647-0.504,0.891
															c-0.216,0.245-0.476,0.432-0.778,0.562C29.567,36.306,29.233,36.371,28.866,36.371z M28.866,35.315
															c0.372,0,0.667-0.16,0.883-0.479c0.217-0.319,0.325-0.753,0.325-1.303c0-0.544-0.108-0.968-0.325-1.274
															c-0.216-0.305-0.511-0.458-0.883-0.458c-0.371,0-0.666,0.153-0.882,0.458c-0.217,0.306-0.325,0.73-0.325,1.274
															c0,0.549,0.108,0.984,0.325,1.303C28.2,35.155,28.495,35.315,28.866,35.315z"/>
													</g>
												</g>
											</g>
										</svg>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Отдел контрактных продаж</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>м. Сокольники, ул Маленковская, д. 32, стр.3 </span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(495) 225-23-01, доб.247, 271, 241</span>
										</div>
										<div class="contacts-item__email">
											<span>E-mail:</span> 
											<a href="mailto:tender02@komus.net">tender02@komus.net</a> 
										</div>
										<div class="contacts-item__inner">
											<span class="contacts-item__title">«Северо-Запад»</span>
											<div class="contacts-item__adress">
												<span>Адрес:</span>
												<span>м. Полежаевская, Куусинена ул., д. 5</span>
											</div>
											<div class="contacts-item__phone">
												<span>Телефон:</span> 
												<span>+7(495) 741-46-58</span>
											</div>
										</div>
										<div class="contacts-item__inner">
											<span class="contacts-item__title">«Туполевский»</span>
											<div class="contacts-item__adress">
												<span>Адрес:</span>
												<span>м. Бауманская, Акад. Туполева наб., д. 15, стр. 29</span>
											</div>
											<div class="contacts-item__phone">
												<span>Телефон:</span> 
												<span>+7(495) 995-00-25</span>
											</div>
										</div>
										<svg version="1.1" class="contacts-item__icon ic"
											xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30.167 43.241"
											style="enable-background:new 0 0 30.167 43.241;" xml:space="preserve">
										<g>
											<g>
												<g>
													<path style="fill:#D10910;" d="M25.491,4.916c-5.679-5.676-14.886-5.676-20.566,0c-5.117,5.117-5.692,14.754-1.348,20.543
														L15.21,42.256l11.633-16.797C31.183,19.67,30.608,10.033,25.491,4.916L25.491,4.916z M15.351,19.858
														c-2.653,0-4.801-2.149-4.801-4.801c0-2.649,2.148-4.801,4.801-4.801c2.652,0,4.8,2.152,4.8,4.801
														C20.151,17.709,18.003,19.858,15.351,19.858L15.351,19.858z M15.351,19.858"/>
												</g>
											</g>
										</g>
										</svg>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Производственно-складской комплекс</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>г. Москва, Остаповский проезд, д. 9, стр. 1</span>
										</div>
										<svg version="1.1" class="contacts-item__icon ic-psk" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 41.167 43.063" style="enable-background:new 0 0 41.167 43.063;" xml:space="preserve">
											<g>
												<g>
													<g>
														<path style="fill:#008B3B;" d="M25.199,4.505c-5.679-5.676-14.886-5.676-20.566,0C-0.484,9.623-1.059,19.26,3.285,25.048
															l11.633,16.797l11.633-16.797C30.891,19.26,30.316,9.623,25.199,4.505L25.199,4.505z M15.059,19.448
															c-2.653,0-4.801-2.149-4.801-4.801c0-2.649,2.148-4.801,4.801-4.801c2.652,0,4.8,2.152,4.8,4.801
															C19.859,17.298,17.711,19.448,15.059,19.448L15.059,19.448z M15.059,19.448"/>
													</g>
													<path style="fill:#D10910;" d="M28.168,33.048"/>
													<g>
														<path style="fill:#FFFFFF;" d="M31.466,24.58h-6.362c-4.803,0-8.732,3.929-8.732,8.732v0c0,4.803,3.929,8.732,8.732,8.732h6.362
															c4.803,0,8.732-3.929,8.732-8.732v0C40.199,28.51,36.269,24.58,31.466,24.58z"/>
														<path style="fill:#008B3B;" d="M31.466,42.545h-6.362c-5.091,0-9.232-4.142-9.232-9.232s4.142-9.232,9.232-9.232h6.362
															c5.091,0,9.232,4.142,9.232,9.232S36.557,42.545,31.466,42.545z M25.104,25.08c-4.539,0-8.232,3.693-8.232,8.232
															s3.693,8.232,8.232,8.232h6.362c4.539,0,8.232-3.693,8.232-8.232s-3.693-8.232-8.232-8.232H25.104z"/>
													</g>
													<g>
														<path style="fill:#3B3B3B;" d="M20.162,35.973v-5.32h4.762v5.32h-1.577v-4.086h-1.609v4.086H20.162z"/>
														<path style="fill:#3B3B3B;" d="M28.634,36.101c-0.372,0-0.717-0.062-1.035-0.188c-0.318-0.125-0.595-0.305-0.831-0.541
															c-0.237-0.236-0.422-0.527-0.558-0.875c-0.136-0.347-0.204-0.741-0.204-1.184s0.075-0.839,0.225-1.185
															c0.15-0.347,0.353-0.638,0.607-0.875c0.253-0.236,0.548-0.416,0.884-0.541c0.336-0.125,0.687-0.188,1.052-0.188
															c0.328,0,0.62,0.053,0.874,0.161c0.254,0.107,0.481,0.244,0.681,0.408l-0.739,1.019c-0.25-0.207-0.491-0.311-0.719-0.311
															c-0.386,0-0.69,0.136-0.911,0.408s-0.333,0.64-0.333,1.105c0,0.464,0.113,0.833,0.338,1.104c0.225,0.272,0.51,0.408,0.853,0.408
															c0.171,0,0.338-0.038,0.498-0.113c0.161-0.075,0.312-0.166,0.456-0.273l0.622,1.029c-0.264,0.23-0.551,0.392-0.858,0.489
															C29.227,36.053,28.927,36.101,28.634,36.101z"/>
														<path style="fill:#3B3B3B;" d="M31.326,35.973v-5.32h1.576v1.963h0.547l0.461-0.976c0.107-0.222,0.218-0.406,0.333-0.552
															c0.114-0.147,0.242-0.261,0.381-0.343c0.14-0.082,0.288-0.14,0.445-0.172c0.157-0.032,0.333-0.048,0.525-0.048
															c0.157,0,0.3,0.024,0.429,0.075l-0.247,1.458c-0.036-0.014-0.07-0.025-0.101-0.032c-0.032-0.007-0.066-0.01-0.102-0.01
															c-0.136,0-0.257,0.032-0.365,0.096c-0.107,0.065-0.211,0.211-0.311,0.44l-0.279,0.633l1.791,2.789h-1.726l-1.223-2.124h-0.558
															v2.124H31.326z"/>
													</g>
												</g>
											</g>
										</svg>
									</article>
								</div>
							</div>
							<div class="map map--city">
								<div id="mapMoscow" class="map__container"></div>
								<div class="map__description">
									<figure class="figure">
										<figcaption>
											<span class="figurecaption__text">«КОМУС» — москва</span>
										</figcaption>
									</figure>
								</div>
							</div>
							<div class="contacts-city__province">
								<span class="contacts-title">
									МО и ближайшие города
								</span>
								<div class="contacts-list">
									<article class="contacts-item">
										<span class="contacts-item__title">Коломна</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Окский пр-т, д. 90</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(496) 623-14-13</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Одинцово</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Одинцовский район, Малые Вяземы, Петровский пр-т., вл. 5, стр. 1</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(495) 363-46-79</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Подольск</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Комсомольская ул., д. 2</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(496) 758-31-85</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Мытищи</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Колпакова ул., д. 2</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(495) 627-72-43</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Люберцы</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Красная ул., д. 1</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(495) 565-41-83</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Балашиха</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Звездная ул., д. 7, корп. 1</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(498) 600-79-88</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Ногинск</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Климова ул., д. 50</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(499) 503-73-33</span>
										</div>
									</article>
									<article class="contacts-item">
										<span class="contacts-item__title">Зеленоград</span>
										<div class="contacts-item__adress">
											<span>Адрес:</span>
											<span>Панфилова ул., д. 28Б, корп. 1</span>
										</div>
										<div class="contacts-item__phone">
											<span>Телефон:</span> 
											<span>+7(499) 940-15-01</span>
										</div>
									</article>
								</div>
							</div>
							<div class="map map--province">
								<div id="mapMoscowProvince" class="map__container"></div>
								<div class="map__description">
									<figure class="figure">
										<figcaption>
											<span class="figurecaption__text">«КОМУС» — московская область</span>
										</figcaption>
									</figure>
								</div>
							</div>
						</section>
						<section class="contacts-city js-tab__saint-petersburg js-tab hidden-block">
							<span class="contacts-title">
								Санкт-Петербург
							</span>
							<div class="contacts-list">
								<article class="contacts-item">
									<span class="contacts-item__title">Центральный офис продаж</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span> г. Москва, ул Маленковская, д. 32, стр.3 </span>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(495) 225-23-01</span>
									</div>
									<div class="contacts-item__email">
										<span>E-mail:</span> 
										<a href="mailto:reklama@komus.net">reklama@komus.net</a> </div>
									</div>
									<svg version="1.1" class="contacts-item__icon ic-co" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 35.667 43.345" style="enable-background:new 0 0 35.667 43.345;" xml:space="preserve">
										<g>
											<g>
												<g>
													<path style="fill:#D10910;" d="M25.407,4.796c-5.679-5.676-14.886-5.676-20.566,0C-0.276,9.913-0.851,19.55,3.493,25.339
														l11.633,16.797l11.633-16.797C31.099,19.55,30.524,9.913,25.407,4.796L25.407,4.796z M15.267,19.738
														c-2.653,0-4.801-2.149-4.801-4.801c0-2.649,2.148-4.801,4.801-4.801c2.652,0,4.8,2.152,4.8,4.801
														C20.067,17.589,17.919,19.738,15.267,19.738L15.267,19.738z M15.267,19.738"/>
												</g>
												<g>
													<path style="fill:#FFFFFF;" d="M34.614,33.789c0,4.863-3.938,8.801-8.797,8.801c-4.863,0-8.801-3.938-8.801-8.801
														c0-4.859,3.938-8.797,8.801-8.797C30.676,24.992,34.614,28.93,34.614,33.789L34.614,33.789z M34.614,33.789"/>
													<path style="fill:#D10910;" d="M25.817,43.089c-5.129,0-9.302-4.172-9.302-9.301c0-5.126,4.173-9.297,9.302-9.297
														c5.126,0,9.297,4.171,9.297,9.297C35.114,38.917,30.943,43.089,25.817,43.089z M25.817,25.492c-4.577,0-8.302,3.722-8.302,8.297
														c0,4.577,3.725,8.301,8.302,8.301c4.575,0,8.297-3.724,8.297-8.301C34.114,29.213,30.393,25.492,25.817,25.492z"/>
												</g>
												<path style="fill:#D10910;" d="M28.376,33.339"/>
												<g>
													<path style="fill:#3B3B3B;" d="M21.174,36.272v-5.428h1.224v4.395h1.715v-4.395h1.223v4.395h0.666v0.766l-0.108,1.831h-1.099
														v-1.565H21.174z"/>
													<path style="fill:#3B3B3B;" d="M28.866,36.371c-0.366,0-0.7-0.065-1.003-0.195c-0.303-0.131-0.562-0.318-0.778-0.562
														c-0.217-0.244-0.384-0.541-0.504-0.891c-0.119-0.35-0.179-0.746-0.179-1.19c0-0.444,0.06-0.839,0.179-1.186
														c0.12-0.347,0.287-0.638,0.504-0.874c0.216-0.236,0.476-0.416,0.778-0.541c0.302-0.125,0.637-0.187,1.003-0.187
														c0.367,0,0.701,0.062,1.003,0.187c0.303,0.125,0.562,0.307,0.778,0.545c0.217,0.238,0.384,0.532,0.504,0.878
														c0.119,0.348,0.179,0.74,0.179,1.178c0,0.444-0.06,0.841-0.179,1.19c-0.12,0.35-0.287,0.647-0.504,0.891
														c-0.216,0.245-0.476,0.432-0.778,0.562C29.567,36.306,29.233,36.371,28.866,36.371z M28.866,35.315
														c0.372,0,0.667-0.16,0.883-0.479c0.217-0.319,0.325-0.753,0.325-1.303c0-0.544-0.108-0.968-0.325-1.274
														c-0.216-0.305-0.511-0.458-0.883-0.458c-0.371,0-0.666,0.153-0.882,0.458c-0.217,0.306-0.325,0.73-0.325,1.274
														c0,0.549,0.108,0.984,0.325,1.303C28.2,35.155,28.495,35.315,28.866,35.315z"/>
												</g>
											</g>
										</g>
									</svg>
								</article>
							</div>
							<div class="map">
								<div class="map__sontainer" id="mapSaintPetersburg"></div>
							</div>
						</section>
						<section class="contacts-region">
							<span class="contacts-title">
								Региональные представительства
							</span>
							<div class="contacts-list">
								<article class="contacts-item">
									<span class="contacts-item__title">Москва</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>ул Маленковская, д. 32, стр.3</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(495) 225-23-01</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Екатеринбург</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Щорса ул., д. 7</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(343) 378-47-02</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Уфа</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Проспект Октября, д.33, стр.1</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(347) 292-50-11</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Нижний Новгород</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Мотальный пер., д. 8 </span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(831) 467-81-20</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Челябинск</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Автодорожная ул., д. 19А</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(351) 799-20-77</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Тула</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Войкова ул., д. 2/5</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(4872) 25-00-80, 25-00-81</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Краснодар</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Северная ул., д.357</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(861) 210-07-16</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Санкт-Петербург</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Таврическая ул., д. 17</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(812) 325-26-15</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Пермь</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>3-ая Водопроводная ул., д. 1А,  стр. 1</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(812) 325-26-15</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Волгоград</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Елецкая ул., д. 21</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(8442) 59-77-02</span>
									</div>
								</article>
								<article class="contacts-item">
									<span class="contacts-item__title">Казань</span>
									<div class="contacts-item__adress">
										<span>Адрес:</span>
										<span>Лаврентьева ул., д. 3</span>
									</div>
									<div class="contacts-item__phone">
										<span>Телефон:</span> 
										<span>+7(843) 233-05-22</span>
									</div>
								</article>
							</div>
						</section>
					</div>
					<div class="content maxWidth">
						<div class="map map--region">
							<div id="mapRegion" class="map__container"></div>
							<div class="map__description">
								<figure class="figure">
									<figcaption>
										<span class="figurecaption__text">региональные представительства</span>
									</figcaption>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/main_page_slider.js"></script>
			<script src="js/news_slider.js"></script>
			<script src="js/slider_item.js"></script>
			<script src="js/contacts_map.js"></script>
		</footer><!-- #footer -->
	</body>
</html>