<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/side_bar.css" rel="stylesheet">
	<link href="css/news.css" rel="stylesheet">
	<link href="css/catalog_item.css" rel="stylesheet">	
	<link href="css/one-print-slider.css" rel="stylesheet">	
	<link href="css/action-products.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="one-new">
				<div class="content maxWidth">
					<h1>Ежедневники из кожи — традиционный элитарный стиль</h1>
						<div class="sideBar menu">
						<div class="sideBar__left">
							<div class="row">
							<nav class="menuSideBar">
								<div class="menuSideBar__mobile js-menuSideBar-mobile">
									<div class="news-list__item">
										<span class="news-list__item-date">31.12.2017</span>
										<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
									</div>
								</div>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
											</div>
										</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">
											<div class="news-list__item">
												<span class="news-list__item-date">31.12.2017</span>
												<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
											</div>
										</a>
									</li>
								</menu>
								<a href="#" class="button greenButton news-btn">Все новости</a>
							</nav>
							</div>
						</div>
							<div class="sideBar__right">
								<div class="sideBar__right-col-l">
									<div class="row">
										<div class="one-print__gallery js-sync-slider">
											<div class="one-print__once-slider js-once-slider">
												<div class="slide">
													<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
														<img src="./media/printing_img_1.jpg" alt="">
													</a>
												</div>
												<div class="slide">
													<a href="./images/interviewblock/1280-1919.jpg" rel="group" class="js-fancybox slide__pic">
														<img src="./media/printing_img_2.jpg" alt="">
													</a>
												</div>
												<div class="slide">
													<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
														<img src="./media/printing_img_3.jpg" alt="">
													</a>
												</div>
												<div class="slide">
													<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox slide__pic">
														<img src="./media/-mg-4897------.jpg" alt="">
													</a>
												</div>
											</div>
											<div class="one-new__slider">
												<h4>Фото</h4>
												<div class="one-print__several-slider js-several-slider">
													<div class="slide">
														<div class="slide__pic">
															<img src="./media/printing_img_1.jpg" alt="">
														</div>
													</div>
													<div class="slide">
														<div class="slide__pic">
															<img src="./media/printing_img_2.jpg" alt="">
														</div>
													</div>
													<div class="slide">
														<div class="slide__pic">
															<img src="./media/printing_img_3.jpg" alt="">
														</div>
													</div>
													<div class="slide">
														<div class="slide__pic">
															<img src="./media/-mg-4897------.jpg" alt="">
														</div>
													</div>
												</div>
												<div class="importantInformation">
													<span class="importantInformation__text">Получить более подробную информацию Вы можете у менеджера Центра <br>«Комус-реклама»:</span>
													<span class="importantInformation__contact">Телефон: +7 (495) 225 2301<br>E-mail: reklama@komus.net</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="sideBar__right-col-r">
									<div class="one-new__title">
										<h4>18.10.2017</h4>
									</div>
									<p>
										Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают выстроить и придерживаться грамотного бизнес-плана, а также рационально распоряжаться своим временем, которое для занятых людей действительно является деньгами.
									</p>
									<p>
										Формат ежедневника может быть А4 (210х297 мм) или более компактным, А5 (148х210 мм).
									</p>
									<p>
										Блок ежедневника разделяют на датированный, недатированный или полудатированный. Выпуск датированных ежедневников приурочен к началу будущего или недавно наступившего календарного года. На страницах такой продукции уже напечатаны даты, дни недели, часы. Это удобно, так как нет необходимости тратить свое время на то, чтобы вручную проставлять даты, часы и минуты. Недатированные ежедневники подходят людям, чья деятельность не привязана строго к началу года. Например, преподаватели, студенты и люди творческих профессий. Также, недатированные ежедневники экономичны в использовании, и могут прослужить дольше, не один год. Существуют также полудатированные ежедневники, на страницах которых написаны лишь число и месяц. Дни недели напечатаны под числом и месяцем в строчку. Вы сами выбираете нужный день недели и подчеркиваете его. Достоинство такого ежедневника в том, что у пользователя есть возможность работать с ним в любом году и с любой даты.
									</p>
									<p>
										Иногда листы ежедневника перфорируются или имеют высечку, чтобы оперативно перейти к нужной странице. Немаловажным для индивидуальности ежедневника является цвет и материал каптала, наличие ляссе (ленточки-закладки), золочение или серебрение торцов блока.
									</p>
									<p>
										Конечно, обложка ежедневника может быть различных цветов и изготовлена из таких материалов, как: кожзаменитель, ткань, дизайнерская бумага и т.п. Но ежедневник, переплет которого выполнен из натуральной кожи – несомненно, выделит вас среди конкурентов. Натуральная кожа – благородный материал ручной работы. Она требует особого умения, профессионализма и аккуратности при обработке, поэтому кожаные изделия так приятно держать в руках. Кроме того, кожа – материал, который не знает износа. При правильном уходе кожаная обложка для ежедневника прослужит годы.
									</p>
									<p>
										Изящные и неповторимые ежедневники ручной работы, каждый из которых является своего рода произведением искусства, могут стать достойным подарком для успешных деловых людей, которые ценят стиль, качество, комфорт и оригинальность.
										Персонализируют кожаные ежедневники обычно с помощью тиснения, например рисунка или логотипа. Тиснение бывает блинтовое (слепое), фольгой и конгревное.
									</p>
									<div class="content-social">
										<span class="strong content-social__title">Поделиться новостью в соц.сетях:</span>
										<ul class="content-social__list">
											<li>
												<a href="#" class="icon-tw content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-yt content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-vk content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-gplus content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-fb content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-lj content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-linkedin content-social__link"></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="action-products">
									<div class="action-products__title">
										<h2>Акционные товары</h2>
										<a href="#">Все акционные товары</a>
									</div>
									<div class="action-products__slider js-action-slider greenArrow">
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-19733618.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__list">
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="1">
													<img src="media/1-436006-04------.png" alt=""/>
												</a>
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="2">
													<img src="media/1-436006-03.png" alt=""/>
												</a>
											</div>
											<div class="catalogItem__block js-catalogItem__block" data-id="1">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-04.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
											<div class="catalogItem__block hidden-block js-catalogItem__block" data-id="2">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-042.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Часы настенные «Attendee»</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">718.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-138308.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7872.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-170356.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/one_print_slider.js"></script>
		<script src="js/action-slider.js"></script>
		<script src="js/catalog_list.js"></script>			
	</footer>
	<!-- #footer -->
</body>

</html>