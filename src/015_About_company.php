<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/about_company.css" rel="stylesheet">
	<link href="css/reviews_slider.css" rel="stylesheet">
	<link href="css/photo_gallery_slider.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="about-company js-tabsblock">
				<div class="content maxWidth">
					<h1>О компании</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<nav class="menuSideBar js-menuSideBar-sticky">
								<span class="menuSideBar__mobile js-menuSideBar-mobile">О нас</span>
								<menu class="menuSideBar__list js-menuSideBar">
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link current">О нас</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Портфолио</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Наши партнеры</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Гос. заказчикам</a>
									</li>
									<li class="menuSideBar__item">
										<a href="#" class="menuSideBar__link js-menuSideBar-link">Вакансии</a>
									</li>
								</menu>
							</nav>
						</div>
						<div class="sideBar__right">
							<div class="text-block">
								<figure class="figure left">
									<img src="media/about_photo_1.jpg" alt="" />
									<figcaption>
										<span class="figurecaption__text">коллектив «КОМУС»</span>
									</figcaption>
								</figure>
								<div class="text">
									<p>
										Центр «Комус-реклама» уже более 20 лет успешно работает на рынке рекламно-сувенирной и полиграфической продукции и является
										бесспорным лидером среди российских компаний на рынке сувениров и бизнес-подарков. Центр «Комус-реклама» успел зарекомендовать
										себя как надежная, ориентированная на потребности Партнеров компания. Как принято в Компании «Комус», у нас нет клиентов,
										у нас есть Партнеры, с которыми мы ведем совместный бизнес на благо друг друга.
									</p>
									<p>
										«Комус-реклама» издает собственный каталог «Бизнес-сувениры и полиграфия», который ежегодно увеличивается в объеме, предлагая
										потребителям более 1200 наименований продукции, среди которой можно найти как зарекомендовавшую себя классическую
										продукцию, так и авангардные новинки сувенирной отрасли.
									</p>
									<p>
										Центр состоит из двух подразделений: Административно-творческого (фронт-офис) и Производственно-складского комплекса.
									</p>
									<p>
										В Административно-творческом комплексе сосредоточены лучшие управленческие и творческие кадры Компании, здесь находится административное
										звено, осуществляющее централизованное управление Компанией; творческая мастерская, где опытные арт-директоры работают
										над нестандартными заказами наших Партнеров; дизайн-студия с уникальными специалистами по графическому дизайну; отдел
										закупок и логистики, готовый не только найти продукцию для самого взыскательного покупателя, но и доставить ее к
										месту назначения в более чем 40 регионов России; подразделения по работе с Партнерами, внимательно сопровождающие
										наших заказчиков от момента обращения до отгрузки готовой продукции.
									</p>
								</div>
							</div>
							<div class="row">
								<div class="advantages">
									<div class="advantages__title">
										<h2>Наши конкурентные преимущества</h2>
									</div>
									<div class="advantages__list">
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-01.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>Гарантированное качество услуг и высокие стандарты обслуживания</h6>
												<p>Мы возьмем на себя все хлопоты Партнера по оформлению заказа. Наши специалисты будут сопровождать Вас на каждом
													этапе работы, от начала заказа, до доставки готовой продукции в Ваш офис.</p>
											</div>
										</div>
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-02.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>Наша продукция всегда в срок</h6>
												<p>Благодаря собственному производственно-складскому комплексу мы способны обеспечить поставку продукции в кратчайшие
													сроки.</p>
											</div>
										</div>
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-03.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>У нас самое современное оборудование, позволяющее оперативно персонализировать Вашу продукцию</h6>
												<p>Наличие большого парка оборудования позволяет выполнять практически все виды персонализации: лазерную гравировку,
													тиснение, круговую и линейную шелкографию, тампопечать, деколирование, сублимацию, термоперенос, цифровую и офсетную
													печать. Персональный менеджер проведет для Вас консультацию и доставит наш каталог по первой Вашей просьбе.</p>
											</div>
										</div>
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-04.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>Связаться с нами легко</h6>
												<p>Контакт-центр, оснащенный самым современным оборудованием, позволяет не пропустить ни одного звонка от наших Партнеров.
													Круглосуточный заказ через сайт позволяет заказать продукцию, не задумываясь о времени суток, или разнице во времени.</p>
											</div>
										</div>
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-05.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>Индивидуальный подход к каждому партнеру и гибкая система ценообразования</h6>
												<p>Мы подготовим предложение, интересное именно Вам!</p>
											</div>
										</div>
										<div class="advantages__item">
											<div class="advantages__icon">
												<img src="./images/advantages/Icons_about_company-06.svg" alt="">
											</div>
											<div class="advantages__text">
												<h6>Мы всегда услышим Ваши пожелания</h6>
												<p>Наша Группа контроля качества обслуживания партнеров свяжется с Вами, чтобы узнать, остались ли Вы довольны нашим
													обслуживанием, а также выслушает все пожелания и предложения и поможет разобраться в интересующем Вас вопросе.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="text-block">
								<div class="text">
									<p>
										Предложение Центра включает в себя широкий ассортимент сувенирной продукции: письменные принадлежности, офисные и деловые
										аксессуары, часы и метеостанции, товары для отдыха и путешествий, сладкие подарки и продуктовые корзины, текстильную
										продукцию, посуду, компьютерные аксессуары, наградную атрибутику, промо-продукцию, VIP-подарки, подарочную упаковку,
										полиграфическую продукцию.
									</p>
									<p>
										Предлагаемая продукция может быть использована в промо-акциях, презентациях, на корпоративных праздниках, выставках, юбилеях,
										конференциях и других мероприятиях. Благодаря освоенным уникальным технологиям персонализации, на каждое изделие
										может быть нанесена корпоративная символика, персональная информация или поздравление.
									</p>
									<p>
										Производственно-складской комплекс занимает площадь более 1500 кв.м., располагает огромным парком оборудования и современным
										складом. Новейшие станки позволяют выполнять практически все виды персонализации. На складе размещено более 5000
										наименований популярной рекламно-сувенирной продукции, это позволяет оперативно приступить к выполнению заказа, и
										значительно сократить срок получения Партнерами готовой продукции.
									</p>
									<p>
										Философия работы Компании «Комус-реклама» базируется на принципе: «Мы продаем нечто большее, чем просто сувениры - мы продаем
										продукт, который позволяет нашим Партнерам увеличивать продажи, повышать узнаваемость бренда, помогает доносить идею
										до потребителя или просто доставляет удовольствие от приятного подарка».
									</p>
								</div>
								<figure class="figure right margin-top">
									<img src="media/about_img.jpg" alt="" />
									<figcaption>
										<span class="figurecaption__text">продукция «КОМУС»</span>
									</figcaption>
								</figure>
							</div>
							<div class="reviews">
								<div class="reviews__title">
									<h2>Отзывы</h2>
								</div>
								<div class="reviews__slider js-reviewsSlider greenArrow">
									<article class="reviews__item">
										<p>
										Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают выстроить и придерживаться грамотного бизнес-плана, а также рационально распоряжаться своим временем, которое для занятых людей действительно является деньгами.Формат ежедневника может быть А4 (210х297 мм) или более компактным, А5 (148х210 мм).
										</p>
										<p>Блок ежедневника разделяют на датированный, недатированный или полудатированный. Выпуск датированных ежедневников приурочен к началу будущего или недавно наступившего календарного года. На страницах такой продукции уже напечатаны даты, дни недели, часы. Это удобно, так как нет необходимости тратить свое время на то, чтобы вручную проставлять даты, часы и минуты. </p>
										<footer class="reviews__footer">
											<span class="reviews__date">21.11.2017</span>
											<span class="reviews__name strong">Камчатов Сергей Петрович</span>
										</footer>
									</article>
									<article class="reviews__item">
										<p>
										Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают выстроить и придерживаться грамотного бизнес-плана, а также рационально распоряжаться своим временем, которое для занятых людей действительно является деньгами.Формат ежедневника может быть А4 (210х297 мм) или более компактным, А5 ( На страницах такой продукции уже напечатаны даты, дни недели, часы. Это удобно, так как нет необходимости тратить свое время на то, чтобы вручную проставлять даты, часы и минуты. 
										</p>
										<footer class="reviews__footer">
											<span class="reviews__date">05.12.2017</span>
											<span class="reviews__name strong">Иванов Иван Петрович</span>
										</footer>
									</article>
									<article class="reviews__item">
										<p>
										 людей действительно является деньгами.Формат ежедневника может быть А4 (210х297 мм) или более компактным, А5 (148х210 мм). Блок ежедневника разделяют на датированный, недатированный или полудатированный. Выпуск датированных ежедневников приурочен к началу будущего или недавно наступившего календарного года. На страницах такой продукции уже напечатаны даты, дни недели, часы. Это удобно, так как нет необходимости тратить свое время на то, чтобы вручную проставлять даты, часы и минуты. 
										</p>
										<footer class="reviews__footer">
											<span class="reviews__date">21.01.2018</span>
											<span class="reviews__name strong">Сидоров Сергей Петрович</span>
										</footer>
									</article>
								</div>
							</div>
							<div class="photo-gallery">
								<div class="photo-gallery__title">
									<h2>Фотогалерея</h2>
								</div>
								<div class="photo-gallery__slider js-photoGallerySlider greenArrow">
									<div class="photo-gallery__item">
										<a href="./media/about_photo_1.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_1.jpg" alt="">
										</a>
										<a href="./media/goszakaz_Img.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_5.jpg" alt="">
										</a>
									</div>
									<div class="photo-gallery__item">
										<a href="./media/image0002.png" rel="group" class="js-fancybox">
											<img src="./media/photo_2.jpg" alt="">
										</a>
										<a href="./media/image0005--1-.png" rel="group" class="js-fancybox">
											<img src="./media/photo_6.jpg" alt="">
										</a>
									</div>
									<div class="photo-gallery__item">
										<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_3.jpg" alt="">
										</a>
										<a href="./media/-mg-4897------.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_7.jpg" alt="">
										</a>
									</div>
									<div class="photo-gallery__item">
										<a href="./media/7916.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_4.jpg" alt="">
										</a>
										<a href="./media/photo_8.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_8.jpg" alt="">
										</a>
									</div>
									<div class="photo-gallery__item">
										<a href="./media/photo_3.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_3.jpg" alt="">
										</a>
										<a href="./media/photo_5.jpg" rel="group" class="js-fancybox">
											<img src="./media/photo_5.jpg" alt="">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/reviews_slider.js"></script>
		<script src="js/photo_gallery_slider.js"></script>
	</footer>
	<!-- #footer -->
</body>

</html>