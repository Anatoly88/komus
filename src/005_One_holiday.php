<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/catalog_item.css" rel="stylesheet">
		<link href="css/catalog_list.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>День работников нефтяной и газовой промышленности</h1>
							<div class="sideBar holiday">
								<div class="sideBar__left">
									<nav class="categoryMenu">
										<div class="categoryMenu__touchMenu">
											<a href="javascript:void(0);" class="categoryMenu__touch"><img src="images/holidaylist/1.svg" />1 января: <span>Новый год</span> </a>
										</div>
										<a href="javascript:void(0);" class="categoryMenu__button js-categoryMenu-button" data-animation="ripple" data-show="Выбрать другой праздник" data-hide="Скрыть">Выбрать другой праздник</a>
										<menu class="categoryMenu__menu holidays js-categoryMenu-menu">
											<li class="selected active"><a href="#"><img src="images/holidaylist/1.svg" alt=""/><ins>1 января:</ins> <span>Новый год</span></a></li>
											<li><a href="#"><img src="images/holidaylist/2.svg" alt=""/><ins>7 января:</ins> <span>Рождество Христово</span></a></li>
											<li><a href="#"><img src="images/holidaylist/3.svg" alt=""/><ins>23 февраля:</ins> <span>День защитника Отечества</span></a></li>
											<li><a href="#"><img src="images/holidaylist/4.svg" alt=""/><ins>8 марта:</ins> <span>Международный женский день</span></a></li>
											<li><a href="#"><img src="images/holidaylist/5.svg" alt=""/><ins>1 апреля:</ins> <span>День смеха</span></a></li>
											<li><a href="#"><img src="images/holidaylist/6.svg" alt=""/><ins>12 апреля:</ins> <span>День космонавтики</span></a></li>
											<li><a href="#"><img src="images/holidaylist/7.svg" alt=""/><ins>1 мая:</ins> <span>Праздник весны и труда</span></a></li>
											<li><a href="#"><img src="images/holidaylist/8.svg" alt=""/><ins>9 мая:</ins> <span>День Победы</span></a></li>
											<li><a href="#"><img src="images/holidaylist/9.svg" alt=""/><ins>1 июня:</ins> <span>Международный день защиты детей</span></a></li>
											<li><a href="#"><img src="images/holidaylist/10.svg" alt=""/><ins>12 июня:</ins> <span>День России</span></a></li>
											<li><a href="#"><img src="images/holidaylist/11.svg" alt=""/><ins>1 сентября:</ins> <span>День знаний</span></a></li>
											<li><a href="#"><img src="images/holidaylist/12.svg" alt=""/><ins>3 сентября:</ins> <span>День работников нефтяной и газовой промышленн</span></a></li>
											<li><a href="#"><img src="images/holidaylist/13.svg" alt=""/><ins>9 сентября:</ins> <span>Международный день красоты</span></a></li>
											<li><a href="#"><img src="images/holidaylist/14.svg" alt=""/><ins>13 сентября:</ins> <span>День программиста</span></a></li>
											<li><a href="#"><img src="images/holidaylist/15.svg" alt=""/><ins>1 октября:</ins> <span>Международный день музыки</span></a></li>
										</menu>
									</nav>
									<a href="#" class="button greenButton holidayButton">Все праздники</a>
								</div>
								<div class="sideBar__right">
									<div class="holidayCatalog">
										<div class="holidayCatalog__item">
											<img src="images/holidays/oil.svg" alt="" />
										</div>
										<div class="holidayCatalog__info">
											<h2>З сентября: <span>День работников нефтяной и газовой промышленности</span></h2>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non enim et augue auctor eleifend vel non lorem. Vestibulum vulputate augue urna, eu faucibus felis aliquam et. Nulla eget commodo massa, at ultricies est. Nulla facilisi. Integer vehicula odio vulputate orci tempus, quis aliquam lectus vestibulum. Vivamus et gravida justo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas sed magna volutpat ex cursus imperdiet sollicitudin sit amet nunc. Aliquam sit amet interdum metus.</p>
											<p>Центр состоит из двух подразделений: Административно-творческого (фронт-офис) и Производственно-складского комплекса.В Административно-творческом комплексе сосредоточены лучшие управленческие и творческие кадры Компании, здесь находится административное звено, осуществляющее централизованное управление Компанией; творческая мастерская, где опытные арт-директоры работают над нестандартными заказами наших Партнеров; </p>
										</div>
										<span class="holidayCatalog__title">Вы можете выбрать подходящие подарки к празднику</span>
									</div>
									<div class="filterPopup">
										<a href="javascript:void(0);" class="filterPopup__button button redButton js-show-popup js-filterPopup__button" data-form-class="catalogFilter" data-animation="ripple">Фильтровать</a>
									</div>
									<div class="catalogFilter js-catalogFilterBlock">
										<form>
											<div class="catalogFilter__field">
												<div class="formField catalogFilter__item">
													<input type="number" pattern="[0-9]*" name="" placeholder="Тираж, шт">
												</div>
												<div class="formField catalogFilter__item">
													<input type="number" pattern="[0-9]*" name="" class="js-only-numbers" placeholder="Цена от, ₽">
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Материал</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Цвет</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Бренд</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Склад</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
												<div class="formField catalogFilter__item">
													<input type="number" pattern="[0-9]*" name="" class="js-only-numbers" placeholder="Цена до, ₽">
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Нанесение</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
												<div class="formField catalogFilter__item">
													<select class="js-select styled-select" multiple="multiple">
														<option disabled="disabled" selected="">Объем памяти</option>
														<option>Шелк</option>
														<option>Металл</option>
														<option>Хлопок</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
														<option>Бумага</option>
													</select>
												</div>
											</div>
											<div class="catalogFilter__buttons">
												<div class="catalogFilter__reset">
													<button class="button resetButton" data-animation="ripple">Сбросить</button>
												</div>
												<div class="catalogFilter__button">
													<button class="button filterButton redButton" data-animation="ripple">Фильтровать</button>
												</div>
											</div>
										</form>
									</div>
									<div class="catalogInformation">
										<div class="catalogInformation__found foundItem">
											<span class="foundItem__text">Найдено</span>
											<span class="foundItem__item">150 товаров</span>
										</div>
										<div class="catalogInformation__sort sortItem">
											<span class="sortItem__text">Сортировать по:</span>
											<select class="js-select styled-select">
												<option>Материал</option>
												<option>Металл</option>
												<option>Хлопок</option>
												<option>Бумага</option>
											</select>
										</div>
										<div class="catalogInformation__paggination paggination">
											<ul class="paggination__list">
												<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
												<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
												<li class="paggination__item"><span class="paggination__link current">5</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
												<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
											</ul>
										</div>
									</div>
									<div class="catalogList js-catalogList">
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-19733618.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__list">
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="1">
													<img src="media/1-436006-04------.png" alt=""/>
												</a>
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="2">
													<img src="media/1-436006-03.png" alt=""/>
												</a>
											</div>
											<div class="catalogItem__block js-catalogItem__block" data-id="1">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-04.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
											<div class="catalogItem__block hidden-block js-catalogItem__block" data-id="2">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-042.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Часы настенные «Attendee»</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">718.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-138308.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7872.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-170356.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="number" pattern="[0-9]*" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogList__nextItem"><a href="#" class="button showItems js-showItems" data-animation="ripple">Показать следующие 20 товаров</a></div>
									</div>
									<div class="catalogInformation bottom">
										<div class="catalogInformation__found foundItem">
											<span class="foundItem__text">Найдено</span>
											<span class="foundItem__item">150 товаров</span>
										</div>
										<div class="catalogInformation__sort sortItem">
											<span class="sortItem__text">Сортировать по:</span>
											<select class="js-select styled-select">
												<option>Материал</option>
												<option>Металл</option>
												<option>Хлопок</option>
												<option>Бумага</option>
											</select>
										</div>
										<div class="catalogInformation__paggination paggination">
											<ul class="paggination__list">
												<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
												<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
												<li class="paggination__item"><span class="paggination__link current">5</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
												<li class="paggination__item"><span class="paggination__link dots">...</span></li>
												<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
												<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
			<script src="js/catalog_list.js"></script>
		</footer><!-- #footer -->
	</body>
</html>