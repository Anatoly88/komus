<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/news.css" rel="stylesheet">
	<link href="css/news_item.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="news-list">
				<div class="content maxWidth">
					<h1>Новости</h1>
					<div class="sideBar menu">
						<div class="sideBar__left">
							<div class="row">
								<nav class="menuSideBar">
									<div class="menuSideBar__mobile js-menuSideBar-mobile">
										<div class="news-list__item">
											<span class="news-list__item-date">31.12.2017</span>
											<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
										</div>
									</div>
									<menu class="menuSideBar__list js-menuSideBar">
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link current">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="news-list__item">
													<span class="news-list__item-date">31.12.2017</span>
													<div class="news-list__item-title">Заголовок новости с очень длинным заголовком в несколько строк и подп...</div>
												</div>
											</a>
										</li>
									</menu>
								</nav>
							</div>
						</div>
						<div class="sideBar__right">
							<div class="row">
								<div class="news-list__grid">
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----16.png" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
												<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/logo-------3.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Акрилайт — яркий и эффективный инструмент ...</a>
												<span class="newsItem__text">Акрилайт – это тонкая световая панель из прозрачного акрила, на которой с помощью гравировки нанесен логотип, название ...</span>
												<span class="newsItem__date">01.09.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----18.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----16.png" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
												<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/logo-------3.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Акрилайт — яркий и эффективный инструмент ...</a>
												<span class="newsItem__text">Акрилайт – это тонкая световая панель из прозрачного акрила, на которой с помощью гравировки нанесен логотип, название ...</span>
												<span class="newsItem__date">01.09.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----18.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----16.png" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
												<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/logo-------3.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Акрилайт — яркий и эффективный инструмент ...</a>
												<span class="newsItem__text">Акрилайт – это тонкая световая панель из прозрачного акрила, на которой с помощью гравировки нанесен логотип, название ...</span>
												<span class="newsItem__date">01.09.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----18.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----16.png" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Никелевые наклейки — бренд, воплощенный в металле</a>
												<span class="newsItem__text">Никелевая наклейка — это металлическая пластинка толщиной от 0,04 до 0,08 мм с нанесенным слоем клея. Никелевые</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/logo-------3.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Акрилайт — яркий и эффективный инструмент ...</a>
												<span class="newsItem__text">Акрилайт – это тонкая световая панель из прозрачного акрила, на которой с помощью гравировки нанесен логотип, название ...</span>
												<span class="newsItem__date">01.09.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----18.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
									<div class="newsItem">
										<div class="newsItem__block">
											<a href="#" class="newsItem__img">
												<img src="media/-----17.jpg" alt="" />
											</a>
											<div class="newsItem__info">
												<a href="#" class="newsItem__title">Ежедневники из кожи — традиционный элитарный сти...</a>
												<span class="newsItem__text">Ежедневник играет важную роль в жизни каждого активного и успешного человека, так как каждодневные записи помогают ...</span>
												<span class="newsItem__date">18.10.2017</span>
											</div>
											<a href="#" class="newsItem__read">Читать подробнее</a>
										</div>
									</div>
								</div>
							</div>
							<div class="paggination paggination--inner-page">
								<ul class="paggination__list">
									<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
									<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
									<li class="paggination__item"><span class="paggination__link dots">...</span></li>
									<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
									<li class="paggination__item"><span class="paggination__link current">5</span></li>
									<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
									<li class="paggination__item"><span class="paggination__link dots">...</span></li>
									<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
									<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>