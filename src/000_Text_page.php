<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php require('_head.html'); ?>
		<link href="css/catalog_item.css" rel="stylesheet">
		<link href="css/template_styles.css" rel="stylesheet">
	</head>
	<body class="withBackground">
		<div class="wrapper">
			<header class="main-header">
				<?php require('_header.php'); ?>
			</header><!-- #header-->
			<main class="content-container">
				<div class="content maxWidth">
					<div class="container-fluid">
						<div class="row">
							<h1>Наши партнеры Н1 320 —1920</h1>
							<div class="sideBar menu">
								<div class="sideBar__left">
									<nav class="menuSideBar js-menuSideBar-sticky">
										<span class="menuSideBar__mobile js-menuSideBar-mobile">О нас</span>
										<menu class="menuSideBar__list js-menuSideBar">
											<li class="menuSideBar__item">
												<a href="#" class="menuSideBar__link current">О нас</a>
											</li>
											<li class="menuSideBar__item">
												<a href="#" class="menuSideBar__link">Портфолио</a>
											</li>
											<li class="menuSideBar__item">
												<a href="#" class="menuSideBar__link">Наши партнеры</a>
											</li>
											<li class="menuSideBar__item">
												<a href="#" class="menuSideBar__link">Гос. заказчикам</a>
											</li>
											<li class="menuSideBar__item">
												<a href="#" class="menuSideBar__link">Вакансии</a>
											</li>
										</menu>
									</nav>
								</div>
								<div class="sideBar__right">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel enim a eros consequat condimentum. Suspendisse potenti. Morbi ut tincidunt quam, sed consectetur leo. Etiam nulla velit, maximus quis nisl nec, pharetra dignissim neque. Etiam accumsan feugiat dolor id facilisis. <span class="strong">Integer tristique tellus dolor</span>, lacinia interdum metus faucibus interdum. Cras pretium, ante id vulputate tristique, tortor orci feugiat lectus, eu pharetra arcu tellus eu erat. Integer ut interdum lorem, in egestas lacus. Sed ullamcorper lorem ac libero facilisis, in feugiat est feugiat. Pellentesque egestas sem nulla, eget placerat purus pellentesque sed. Aliquam sed justo volutpat, elementum quam quis, dapibus ex. Sed tristique ex eget venenatis facilisis. Integer tempus porttitor feugiat. Aliquam quis ante at quam convallis malesuada sit amet vitae orci.</p>
									<p>Praesent ante tellus, gravida vel egestas sed, vestibulum ut ante. <a href="#">Duis dapibus mi</a> sit amet dictum feugiat. Vivamus consectetur lacus dictum tellus elementum, sit amet pretium arcu fermentum. Sed accumsan ex vitae nulla tristique fermentum. Duis ultricies ex in convallis pretium. Sed a mollis turpis. Duis tincidunt neque vestibulum mauris mollis, sit amet vulputate erat pretium. Aenean gravida ipsum eget commodo imperdiet. Ut placerat efficitur dolor id accumsan. <a href="#">In blandit tortor non</a> dictum semper.</p>
									<h2>О компании Н2 320 —1920</h2>
									<figure class="figure left">
										<img src="media/image0005--1-.jpg" alt=""/>
										<figcaption>
											<span class="figurecaption__text">коллектив «КОМУС»</span>
										</figcaption>
									</figure>
									<p>Центр «Комус-реклама» уже более 20 лет успешно работает на рынке рекламно-сувенирной и полиграфической продукции и является бесспорным лидером среди российских компаний на рынке сувениров и бизнес-подарков. Центр «Комус-реклама» успел зарекомендовать себя как надежная, ориентированная на потребности Партнеров компания. Как принято в Компании «Комус», у нас нет клиентов, у нас есть Партнеры, с которыми мы ведем совместный бизнес на благо друг друга.</p>
									<p>«Комус-реклама» издает собственный каталог «Бизнес-сувениры и полиграфия», который ежегодно увеличивается в объеме, предлагая потребителям более 1200 наименований продукции, среди которой можно найти как зарекомендовавшую себя классическую продукцию, так и авангардные новинки сувенирной отрасли.</p>
									<p>В Административно-творческом комплексе сосредоточены лучшие управленческие и творческие кадры Компании, здесь находится административное звено, осуществляющее централизованное управление Компанией; творческая мастерская.
									опытные арт-директоры работают над нестандартными заказами наших Партнеров; дизайн-студия с уникальными специалистами по графическому дизайну; отдел закупок и логистики, готовый не только найти продукцию для самого взыскательного покупателя, но и доставить ее к месту назначения в более чем 40 регионов России; подразделения по работе с Партнерами, внимательно сопровождающие наших заказчиков от момента обращения до отгрузки готовой продукции.</p>
									<h3>Госзаказчикам Н3 320 —1920</h3>
									<div style="display: flex;">
										<div style="width: 50%;">
											<p>Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения до федерального ведомства или госкорпорации. Имея собственное производство и систему логистики, «Комус-реклама» выполняет любые заказы независимо от их сложности и места доставки готовой продукции.</p>
											<p>Если вам необходима консультация по:</p>
											<ul>
												<li>рекламно-сувенирной и полиграфической продукции;
													<ul>
														<li>1 подпункт 1го пункта</li>
														<li>
															2 подпункт 1го пункта
															<ul>
																<li>
																	1 подпункт 2 подпукта 1 пункта
																</li>
																<li>
																	2 подпункт 2 подпукта 1 пункта
																</li>
															</ul>
														</li>
													</ul>
												</li>
												<li>подготовке технического задания или электронного аукциона;</li>
												<li>возможным условиям сотрудничества;</li>
											</ul>
											<p>Пожалуйста, обращайтесь в Отдел контрактных продаж.</p>
										</div>
										<div style="width: 50%;">
											<p>Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения до федерального ведомства или госкорпорации. Имея собственное производство и систему логистики, «Комус-реклама» выполняет любые заказы независимо от их сложности и места доставки готовой продукции.</p>
											<p>Если вам необходима консультация по:</p>
											<ol>
												<li>рекламно-сувенирной и полиграфической продукции;
													<ol>
														<li>1 подпункт 1го пункта</li>
														<li>
															2 подпункт 1го пункта
															<ol>
																<li>
																	1 подпункт 2 подпукта 1 пункта
																</li>
																<li>
																	2 подпункт 2 подпукта 1 пункта
																</li>
															</ol>
														</li>
													</ol>
												</li>
												<li>подготовке технического задания или электронного аукциона;</li>
												<li>возможным условиям сотрудничества;</li>
											</ol>
											<p>Пожалуйста, обращайтесь в Отдел контрактных продаж.</p>
										</div>
									</div>
									<h4>Важно прочитать Н4 320 —1920</h4>
									<p>Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения до федерального ведомства или госкорпорации. Имея собственное производство и систему логистики, «Комус-реклама» выполняет любые заказы независимо от их сложности и места доставки готовой продукции.</p>
									<div class="importantInformation">
										<span class="importantInformation__text">Получить более подробную информацию<br>Вы можете в оттделе контрактных продаж</span>
										<span class="importantInformation__contact">Телефон: (495) 685-95-10, (доб. 246, 247, 271)<br>E-mail: tender02@komus.net</span>
									</div>
									<p>Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения.</p>
									<h5>Блок с табличкой Н5 320 —1920</h5>
									<p>Центр «Комус-реклама» работает в рамках федеральных законов 44-ФЗ, 223-ФЗ и имеет многолетний опыт сотрудничества с государственными организациями различного уровня от небольшого учреждения до федерального ведомства или госкорпорации. Имея собственное производство и систему логистики, «Комус-реклама» выполняет любые заказы независимо от их сложности и места доставки готовой продукции.</p>
									<div class="responsiveTable">
										<table>
											<thead>
												<tr>
													<th width="49%%">Наименование / Артикул / Параметры / Нанесение</th>
													<th width="16%">Цена</th>
													<th width="22%">Стоимость<br>нанесения</th>
													<th width="13%">Кол-во</th>
												</tr>
												<tr class="spaceThead">
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Складные часы-будильник в бархатном чехле «Pisa»</td>
													<td><span class="strong price">3 920.0<ins class="rub"></ins></span></td>
													<td><span class="strong price deposition">1 120.0<ins class="rub"></ins></span></td>
													<td>5</td>
												</tr>
												<tr>
													<td>Часы настенные «Attendee»</td>
													<td><span class="strong price">490.0<ins class="rub"></ins></span></td>
													<td><span class="strong price deposition">-</span></td>
													<td>8</td>
												</tr>
												<tr>
													<td>Дорожные часы «Большое путешествие»</td>
													<td><span class="strong price">3 920.0<ins class="rub"></ins></span></td>
													<td><span class="strong price deposition">1 120.0<ins class="rub"></ins></span></td>
													<td>5</td>
												</tr>
												<tr>
													<td>Мужские часы «Полет»</td>
													<td><span class="strong price">490.0<ins class="rub"></ins></span></td>
													<td><span class="strong price deposition">500.0<ins class="rub"></ins></span></td>
													<td>6</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="4">Описание к таблице. Какая красивая таблица. Слов нет.</td>
												</tr>
											</tfoot>
										</table>
									</div>
									<div style="display: flex; flex-wrap: wrap;">
										<div style="width: 33%;">
											<h6>Учреждения федерального уровня Н6 320—1920</h6>
											<p>Аппарат Совета Федерации Аппарат Государственной Думы Управление Делами Президента</p>
										</div>
										<div style="width: 33%;">
											<h6>Региональные и местные учреждения Н6 320—1920</h6>
											<p>Московская Городская Дума Волгоградская Областная Дума Прокуратура г. Москва Управление ПФР по г. Набережные Челны</p>
										</div>
										<div style="width: 33%;">
											<h6>Министерства и ведомства Н6 320—1920</h6>
											<p>ФСО ФСБ МЧС Министерство сельского хозяйства Рособоронпоставка</p>
										</div>
									</div>
									<h2>UI – элементы</h2>
									<div style="display: flex; flex-wrap: wrap;">
										<div style="width: 33.33%">
											<div style="width: 192px; margin-bottom: 20px;">
												<a href="javascript:void(0);" class="button redButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать звонок</a>
											</div>
											<div style="width: 192px; margin-bottom: 20px;">
												<button class="button redButton js-show-popup" data-form-class="requestCall" data-animation="ripple">Заказать звонок</button>
											</div>
											<div style=" width: 149px; margin-bottom: 20px;">
												<a href="#" class="button searchButton" data-animation="ripple"><span>Искать</span></a>
											</div>
											<div style=" width: 149px; margin-bottom: 20px;">
												<button class="button searchButton" data-animation="ripple"><span>Искать</span></button>
											</div>
											<div style=" width: 274px; margin-bottom: 20px;">
												<a href="#" class="button greenButton">Календарь праздников</a>
											</div>
											<div style=" width: 274px; margin-bottom: 20px;">
												<button class="button greenButton">Календарь праздников</button>
											</div>
											<div style=" width: 239px; margin-bottom: 20px;">
												<a href="#" class="button lineButton">Заказать образцы</a>
											</div>
											<div style=" width: 239px; margin-bottom: 20px;">
												<button class="button lineButton">Заказать образцы</button>
											</div>
											<div style=" width: 116px; margin-bottom: 20px;">
												<a href="#" class="button resetButton" data-animation="ripple">Сбросить</a>
											</div>
											<div style=" width: 116px; margin-bottom: 20px;">
												<button class="button resetButton" data-animation="ripple">Сбросить</button>
											</div>
											<div style=" width: 296px; margin-bottom: 20px;">
												<a href="#" class="button showItems" data-animation="ripple">Показать следующие 20 товаров</a>
											</div>
											<div style=" width: 296px; margin-bottom: 20px;">
												<button class="button showItems" data-animation="ripple">Показать следующие 20 товаров</button>
											</div>
										</div>
										<div style="width: 33.33%">
											<div class="uploadBlock">
												<label class="uploadFile">
													<input type="file" name="">
													<span>Прикрепить документ</span>
												</label>
												<div class="uploadFile__document">
													<span class="uploadFile__title">Добавленные документы:</span>
													<div class="uploadFile__item">
														<span class="uploadFile__item-text">Название_документа.doc</span>
														<a href="javascript:void(0);" class="uploadFile__item-close"></a>
													</div>
													<div class="uploadFile__item">
														<span class="uploadFile__item-text">Название_документа.doc</span>
														<a href="javascript:void(0);" class="uploadFile__item-close"></a>
													</div>
												</div>
											</div>
											<div style="margin-bottom: 30px; width: 153px;">
												<select class="js-select styled-select">
													<option disabled="disabled" selected>Материал</option>
													<option>Шелк</option>
													<option>Металл</option>
													<option>Хлопок</option>
													<option>Бумага</option>
												</select>
											</div>
											<div style="padding-bottom: 30px; width: 153px;">
												<select class="js-select styled-select">
													<option disabled="disabled" selected>Материал</option>
													<option>Шелк</option>
													<option>Металл</option>
													<option>Хлопок</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
												</select>
											</div>
											<div style="margin-bottom: 30px; width: 153px;">
												<select class="js-select styled-select" multiple="multiple">
													<option disabled="disabled" selected>Материал</option>
													<option>Шелк</option>
													<option>Металл</option>
													<option>Хлопок</option>
													<option>Бумага</option>
												</select>
											</div>
											<div style="padding-bottom: 30px; width: 153px;">
												<select class="js-select styled-select" multiple="multiple">
													<option disabled="disabled" selected>Материал</option>
													<option>Шелк</option>
													<option>Металл</option>
													<option>Хлопок</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
													<option>Бумага</option>
												</select>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="checkbox">
													<input type="checkbox" name=""/>
													<span class="checkbox__button"></span>
													<ins class="checkbox__text">Я даю согласие на хранение и обработку своих персональных данных. </ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="checkbox">
													<input type="checkbox" disabled name=""/>
													<span class="checkbox__button"></span>
													<ins class="checkbox__text">Я даю согласие на хранение и обработку своих персональных данных. </ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="radio">
													<input type="radio" name="1"/>
													<span class="radio__button"></span>
													<ins class="radio__text">Курьерская доставка</ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="radio">
													<input type="radio" name="1"/>
													<span class="radio__button"></span>
													<ins class="radio__text">Курьерская доставка</ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="radio bold">
													<input type="radio" name="1"/>
													<span class="radio__button"></span>
													<ins class="radio__text">Курьерская доставка</ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="radio bold">
													<input type="radio" name="1"/>
													<span class="radio__button"></span>
													<ins class="radio__text">Курьерская доставка</ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<label class="radio">
													<input type="radio" disabled name=""/>
													<span class="radio__button"></span>
													<ins class="radio__text">Курьерская доставка</ins>
												</label>
											</div>
											<div style="margin-bottom: 30px;">
												<a href="javascript:void(0);" class="js-show-popup" data-form-class="headerMenu">Меню</a>
											</div>
											<div style="margin-bottom: 30px;">
												<a href="javascript:void(0);" class="addApplication">Добавить нанесение</a>
											</div>
										</div>
										<div style="width: 33.33%">
											<div style="margin-bottom: 30px">
												<form>
													<div class="formField js-formField">
														<input
															type="text"
															name="text-name"
															placeholder="Цена от, &#8381;" 
															data-validation="required"
															data-error-msg="Заполните поле цена"
															data-validation-error-msg=" "
															/>
													</div>
													<div class="formField js-formField">
														<input
															type="text"
															name="text-email"
															placeholder="Email" 
															data-validation="email" 
															data-error-msg="Не верно введен email"
															data-validation-error-msg=" "
															/>
													</div>
												</form>
											</div>
											<div style="margin-bottom: 30px">
												<a href="#" class="redLink">Все акционные товары</a>
											</div>
											<div style="margin-bottom: 30px;">
												<div class="paggination" style="width: 395px;">
													<ul class="paggination__list">
														<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
														<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
														<li class="paggination__item"><span class="paggination__link dots">...</span></li>
														<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
														<li class="paggination__item"><span class="paggination__link current">5</span></li>
														<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
														<li class="paggination__item"><span class="paggination__link dots">...</span></li>
														<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
														<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
													</ul>
												</div>
											</div>
											<div style="margin-bottom: 30px; display: flex;">
												<a href="javascript:void(0);" class="arrowLink prev"></a>
												<a href="javascript:void(0);" class="arrowLink next"></a>
												<a href="javascript:void(0);" class="arrowLink prev disabled"></a>
												<a href="javascript:void(0);" class="arrowLink next disabled"></a>
											</div>
										</div>
										<div style="margin-bottom: 30px;width: 100%; display: flex; flex-wrap: wrap;">
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__salePrice"></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__salePrice"></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__salePrice"></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__salePrice"></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__salePrice"></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
											<div class="catalogItem">
												<div class="catalogItem__block">
													<a href="#" class="catalogItem__link">
														<img src="media/7916.jpg" al="" class="catalogItem__img" />
													</a>
													<div class="catalogItem__information">
														<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
														<div class="catalogItem__price">
															<span class="catalogItem__currentPrice">3920.0<ins class="rub"></ins></span>
															<span class="catalogItem__currentPrice sale">4000.0<ins class="rub"></ins></span>
														</div>
														<div class="catalogItem__hiddenBlock">
															<span class="catalogItem__article">Артикул: 5654</span>
															<span class="catalogItem__availability">В наличии: 15 штук</span>
														</div>
													</div>
													<div class="catalogItem__functional">
														<div class="catalogItem__amout">
															<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
															<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
															<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
														</div>
														<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
															<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																	 viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
																<g>
																	<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																		c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																		c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																		C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																		 M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																	<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																		c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																	<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																		c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
																</g>
															</svg>
															<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div>
											<a href="media/-mg-4897------.jpg" class="js-fancybox" rel="gallery1">
												<img src="media/image0005--1-.jpg" alt="" />
											</a>
											<a href="media/image0005--1-.jpg" class="js-fancybox" rel="gallery1">
												<img src="media/image0005--1-.jpg" alt="" />
											</a>
											<a href="media/-mg-4897------.jpg" class="js-fancybox" rel="gallery1">
												<img src="media/image0005--1-.jpg" alt="" />
											</a>
											<a href="media/image0005--1-.jpg" class="js-fancybox" rel="gallery1">
												<img src="media/image0005--1-.jpg" alt="" />
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main><!-- #content-->
		</div>
		<footer class="footer">
			<?php require('_footer.php'); ?>
		</footer><!-- #footer -->
	</body>
</html>