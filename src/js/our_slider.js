"use strict";

if ($('.js-ourSlider')) {

	$('.js-ourSlider').each( function (i, item) {
		$(this).slick({
			centerMode: false,
			centerPadding: '0',
			slidesToShow: 8,
			variableWidth: true,
			dots: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 2000,
			responsive: [
				{
					breakpoint: 1920,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 6,
						variableWidth: true,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 4,
						variableWidth: true,
					}
				},
				{
					breakpoint: 768,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 2,
						variableWidth: true,
					}
				}
			]
		});
		$(this).addClass('visible');
	});
}

if ($('.js-ourPartnersSlider')) {

	$('.js-ourPartnersSlider').each( function (i, item) {
		$(this).slick({
			centerMode: false,
			centerPadding: '0',
			slidesToShow: 6,
			variableWidth: true,
			dots: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 2000,
			responsive: [
				{
					breakpoint: 1920,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 6,
						variableWidth: true,
					}
				},
				{
					breakpoint: 1280,
					settings: {
						centerMode: false,
						centerPadding: '0',
						slidesToShow: 3,
						variableWidth: true,
					}
				},
				{
					breakpoint: 768,
					settings: {
						centerMode: true,
						centerPadding: '0',
						slidesToShow: 1,
						variableWidth: true,
					}
				}
			]
		});
		$(this).addClass('visible');
	});
}