"use strict";

interviewScroll();

function interviewScroll() {
	
	if ($('.js-interviewScroll')) {
		$('.js-interviewScroll').jScrollPane({
			contentWidth: '0px',
			verticalDragMinHeight : 10,
			verticalDragMaxHeight: 53,
			mouseWheelSpeed: 50,
			animateScroll: true,
			animateDuration: 100
		});
	}
}