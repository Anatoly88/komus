if ($('.contacts').length > 0) {
var moscowMap;
var moskowProvinceMap;
var regionMap;

  $(window).on('resize', function() {
    setTimeout(function(){
      moscowMap.container.fitToViewport();
    }, 250);
    setTimeout(function(){
      moskowProvinceMap.container.fitToViewport();
    }, 250);
    setTimeout(function(){
      regionMap.container.fitToViewport();
    }, 250);
  });

  function init() {
  
    // Карта Москвы
  
    moscowMap = new ymaps.Map("mapMoscow", {
      center: [55.786384, 37.674499],
      zoom: 11,
    },{
      searchControlProvider: 'yandex#search'
    });
    moscowMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 70 }}}));
    

    if ($(window).width() >= 1024) {
      moscowMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 50 }}}));
    }
  
    // Карта Мос.обл.
  
    moskowProvinceMap = new ymaps.Map("mapMoscowProvince", {
      center: [56.849526, 35.935396],
      zoom: 13,
    },{
      searchControlProvider: 'yandex#search'
    });
    moskowProvinceMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 70 }}}));

    if ($(window).width() >= 1024) {
      moskowProvinceMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 50 }}}));
    }
  
    // Карта регионов
  
      regionMap = new ymaps.Map("mapRegion", {
        center: [56.849526, 35.935396],
        zoom: 5,
      },{
        searchControlProvider: 'yandex#search'
      });
      regionMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 70 }}}));
      

    if ($(window).width() >= 1024) {
      regionMap.controls.add(new ymaps.control.ZoomControl({options: { position: { left: 10, bottom: 50 }}}));
    }

// 		// Создание макета балуна
    // MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
    // 	'<div id="balloonPopover" class="popover top">' +
    // 			'<a class="close" href="#">&times;</a>' +
    // 			'<div class="arrow"></div>' +
    // 			'<div class="popover-inner">' +
    // 			'$[[options.contentLayout observeSize minWidth=320 maxWidth=320 maxHeight=165]]' +
    // 			'</div>' +
    // 			'</div>', {
    // 			/**
    // 			 * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
    // 			 * @function
    // 			 * @name build
    // 			 */
    // 			build: function () {
    // 					this.constructor.superclass.build.call(this);

    // 					this._$element = $('.popover', this.getParentElement());

    // 					this.applyElementOffset();

    // 					this._$element.find('.close')
    // 							.on('click', $.proxy(this.onCloseClick, this));
    // 			},

    // 			/**
    // 			 * Удаляет содержимое макета из DOM.
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
    // 			 * @function
    // 			 * @name clear
    // 			 */
    // 			clear: function () {
    // 					this._$element.find('.close')
    // 							.off('click');

    // 					this.constructor.superclass.clear.call(this);
    // 			},

    // 			/**
    // 			 * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
    // 			 * @function
    // 			 * @name onSublayoutSizeChange
    // 			 */
    // 			onSublayoutSizeChange: function () {
    // 					MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

    // 					if(!this._isElement(this._$element)) {
    // 							return;
    // 					}

    // 					this.applyElementOffset();

    // 					this.events.fire('shapechange');
    // 			},

    // 			/**
    // 			 * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
    // 			 * @function
    // 			 * @name applyElementOffset
    // 			 */
    // 			applyElementOffset: function () {
    // 					this._$element.css({
    // 							left: -(this._$element[0].offsetWidth / 2),
    // 							top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight + 20)
    // 					});
    // 			},

    // 			/**
    // 			 * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
    // 			 * @function
    // 			 * @name onCloseClick
    // 			 */
    // 			onCloseClick: function (e) {
    // 					e.preventDefault();

    // 					this.events.fire('userclose');
    // 			},

    // 			/**
    // 			 * Используется для автопозиционирования (balloonAutoPan).
    // 			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
    // 			 * @function
    // 			 * @name getClientBounds
    // 			 * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
    // 			 */
    // 			getShape: function () {
    // 					if(!this._isElement(this._$element)) {
    // 							return MyBalloonLayout.superclass.getShape.call(this);
    // 					}

    // 					var position = this._$element.position();

    // 					return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
    // 							[position.left, position.top], [
    // 									position.left + this._$element[0].offsetWidth,
    // 									position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
    // 							]
    // 					]));
    // 			},

    // 			/**
    // 			 * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
    // 			 * @function
    // 			 * @private
    // 			 * @name _isElement
    // 			 * @param {jQuery} [element] Элемент.
    // 			 * @returns {Boolean} Флаг наличия.
    // 			 */
    // 			_isElement: function (element) {
    // 					return element && element[0] && element.find('.arrow')[0];
    // 			}
    // 	}),

// Создание вложенного макета содержимого балуна.
  // MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
  // 		'<h5 class="popover-title">$[properties.balloonHeader]</h5>' +
  // 		'<div class="row">' +
  // 			'<div class="cell">' +
  // 				'<span>$[properties.balloonCell1Title]</span>' +
  // 				'<span class="strong">$[properties.balloonCell1Val]</span>' +
  // 			'</div>' +
  // 			'<div class="cell">' +
  // 				'<span>$[properties.balloonCell2Title]</span>' +
  // 				'<span class="strong">$[properties.balloonCell2Val]</span>' +
  // 			'</div>' +
  // 			'<div class="cell">' +
  // 				'<span>$[properties.balloonCell3Title]</span>' +
  // 				'<span class="strong">$[properties.balloonCell3Val]</span>' +
  // 			'</div>' +
  // 		'</div>' +
  // 		'<div class="popover-footer">По этому адресу ещё: <a href="#">$[properties.balloonFooter]</a></div>'
  // ),

    objectManagerMoscowMap = new ymaps.ObjectManager({
      // Чтобы метки начали кластеризоваться, выставляем опцию.
      clusterize: false,
      // ObjectManager принимает те же опции, что и кластеризатор.
      gridSize: 32,
      clusterDisableClickZoom: false,
    });

    objectManagerMoscowProvinceMap = new ymaps.ObjectManager({
      // Чтобы метки начали кластеризоваться, выставляем опцию.
      clusterize: false,
      // ObjectManager принимает те же опции, что и кластеризатор.
      gridSize: 32,
      clusterDisableClickZoom: false,
    });

    objectManagerRegionMap = new ymaps.ObjectManager({
      // Чтобы метки начали кластеризоваться, выставляем опцию.
      clusterize: false,
      // ObjectManager принимает те же опции, что и кластеризатор.
      gridSize: 32,
      clusterDisableClickZoom: false,
    });

    objectManagerMoscowMap.objects.options.set({
      balloonPanelMaxMapArea: 0,
      iconLayout: 'default#image',
     });

    objectManagerMoscowProvinceMap.objects.options.set({
      balloonPanelMaxMapArea: 0,
      iconLayout: 'default#image',
     });

    objectManagerRegionMap.objects.options.set({
      balloonPanelMaxMapArea: 0,
      iconLayout: 'default#image',
     });

    moscowMap.geoObjects.add(objectManagerMoscowMap);
    moskowProvinceMap.geoObjects.add(objectManagerMoscowProvinceMap);
    regionMap.geoObjects.add(objectManagerRegionMap);
    
    $.ajax({
      url: "./media/geo_moscow.json"
    }).done(function(data) {
      objectManagerMoscowMap.add(data);
    });
    
    $.ajax({
      url: "./media/geo_moscow_province.json"
    }).done(function(data) {
      objectManagerMoscowProvinceMap.add(data);
    });
    
    $.ajax({
      url: "./media/geo_region.json"
    }).done(function(data) {
      objectManagerRegionMap.add(data);
    });
    
    // objectManager.objects.events.add('mouseenter', function (e) {
    // 	var objectId = e.get('objectId');
    // 	objectManager.objects.balloon.open(objectId);
    // });

    moscowMap.controls.remove('geolocationControl');
    moscowMap.controls.remove('searchControl');
    moscowMap.controls.remove('trafficControl');
    moscowMap.controls.remove('typeSelector');
    moscowMap.controls.remove('fullscreenControl');
    moscowMap.controls.remove('rulerControl');
    moscowMap.controls.remove('zoomControl');
    moscowMap.behaviors.disable('scrollZoom');
    moscowMap.behaviors.disable('drag');

    moskowProvinceMap.controls.remove('geolocationControl');
    moskowProvinceMap.controls.remove('searchControl');
    moskowProvinceMap.controls.remove('trafficControl');
    moskowProvinceMap.controls.remove('typeSelector');
    moskowProvinceMap.controls.remove('fullscreenControl');
    moskowProvinceMap.controls.remove('rulerControl');
    moskowProvinceMap.controls.remove('zoomControl');
    moskowProvinceMap.behaviors.disable('scrollZoom');
    moskowProvinceMap.behaviors.disable('drag');

    regionMap.controls.remove('geolocationControl');
    regionMap.controls.remove('searchControl');
    regionMap.controls.remove('trafficControl');
    regionMap.controls.remove('typeSelector');
    regionMap.controls.remove('fullscreenControl');
    regionMap.controls.remove('rulerControl');
    regionMap.controls.remove('zoomControl');
    regionMap.behaviors.disable('scrollZoom');
    regionMap.behaviors.disable('drag');
  }
  ymaps.ready(init);
}
