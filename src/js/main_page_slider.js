"use strict";
if ($('.js-mainPageSlider')) {
	
	$('.js-mainPageSlider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: false,
		variableWidth: false
	});

	$('.js-mainPageSlider').addClass('visible');
}