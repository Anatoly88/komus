"use strict";

$('.js-datepicker').datepicker({
	dateFormat : "yy-mm-dd",
	minDate: new Date($('#hiddendelivdate').val()),
	monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
	firstDay: 1,
	beforeShowDay: function(date) {
		var today = new Date(), maxDate;
		today.setHours(0,0,0,0);
		maxDate = new Date().setDate(today.getDate() + 4);
		if (date <= maxDate && date >= today ) {
			return [true, 'holiday'];
		}
		return [true, ''];
	}
});