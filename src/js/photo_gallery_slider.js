"use strict";

if ($('.js-photoGallerySlider')) {

	$('.js-photoGallerySlider').slick({
		slidesToShow: 4,
		variableWidth: true,
		dots: false,
		infinite: true,
		centerMode: false,
		centerPadding: '0',
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 4,
					variableWidth: true,
				}
			},
			{
				breakpoint: 1280,
				settings: {
					centerMode: false,
					centerPadding: '0',
					slidesToShow: 3,
					variableWidth: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerMode: true,
					centerPadding: '35px',
					slidesToShow: 1,
					variableWidth: true,
				}
			}
		]
	});

	$('.js-photoGallerySlider').addClass('visible');
}