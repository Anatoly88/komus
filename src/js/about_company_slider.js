"use strict";
if ($('.js-sliderAboutCompany')) {
	
	$('.js-sliderAboutCompany').slick({
		dots: true,
		infinite: true,
		speed: 300,
		arrows: false,
		slidesToShow: 1,
		centerMode: false,
		variableWidth: false,
		autoplay: true,
		autoplaySpeed: 5000,
	});

	$('.js-sliderAboutCompany').addClass('visible');
}