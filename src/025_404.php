<?php $breadcrumbs = false; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/404.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="four-h-four">
				<div class="content maxWidth">
					<div class="four-h-four__text">
						<h1>Страница не найдена</h1>
						<span>Возможные причины ошибки:</span>
						<ul>
							<li>ошибка при наборе адреса страницы (URL);</li>
							<li>переход по неработающей или  неправильной  ссылке;</li>
							<li>отсутствие запрашиваемой страницы на сайте.</li>
						</ul>
						<a href="#" class="button redButton">Перейти в каталог</a>
					</div>
					<div class="four-h-four__numbers">
						<span class="four-h-four__number four-h-four__number--1">4</span>
						<span class="four-h-four__number four-h-four__number--2">0</span>
						<span class="four-h-four__number four-h-four__number--3">4</span>
						<span class="four-h-four__word">Ошибка</span>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>