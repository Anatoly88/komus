<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/side_bar.css" rel="stylesheet">
	<link href="css/catalog_item.css" rel="stylesheet">	
	<link href="css/action-products.css" rel="stylesheet">	
	<link href="css/sales.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="one-sale">
				<div class="content maxWidth">
					<h1>Скидка на все товары для спорта, отдыха и туризма только в Январе</h1>
					<div class="row">
						<div class="sideBar menu">
							<div class="sideBar__left">
								<nav class="menuSideBar">
									<div class="menuSideBar__mobile js-menuSideBar-mobile">
										<div class="one-sale__item">
											<div class="one-sale__item-title">Новогодняя акция от фирмы «Комус». Скидка 10% на все товары.</div>
											<div class="one-sale__item-period">
												Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
											</div>
										</div>
									</div>
									<menu class="menuSideBar__list js-menuSideBar">
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link current">
												<div class="one-sale__item">
													<div class="one-sale__item-title">Новогодняя акция от фирмы «Комус». Скидка 10% на все товары.</div>
													<div class="one-sale__item-period">
														Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
													</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="one-sale__item">
													<div class="one-sale__item-title">Ежедневники из кожи — традиционный элитарный стил...</div>
													<div class="one-sale__item-period">
														Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
													</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="one-sale__item">
													<div class="one-sale__item-title">Скидка на все товары для спорта, отдыха и туризм...</div>
													<div class="one-sale__item-period">
														Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
													</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="one-sale__item">
													<div class="one-sale__item-title">При покупке офисных принадлежностей более 500 ...</div>
													<div class="one-sale__item-period">
														Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
													</div>
												</div>
											</a>
										</li>
										<li class="menuSideBar__item">
											<a href="#" class="menuSideBar__link js-menuSideBar-link">
												<div class="one-sale__item">
													<div class="one-sale__item-title">Новогодняя акция от фирмы «Комус». Скидка 10% на все ...</div>
													<div class="one-sale__item-period">
														Срок действия акции до <span class="one-sale__item-date">31.12.2017</span>
													</div>
												</div>
											</a>
										</li>
									</menu>
									<a href="#" class="button redButton action-btn">Все акции</a>
								</nav>
							</div>
							<div class="sideBar__right">
								<div class="sideBar__right-col-l">
									<figure class="figure">
										<div class="figure__img">
											<img src="media/sales_one.png" alt="">
										</div>
										<figcaption>
											<span class="figurecaption__precent">-15<span class="precent">%</span></span>
											<span class="figurecaption__text"><span>на товары для спорта</span></span>
										</figcaption>
									</figure>
									<div class="importantInformation">
										<span class="importantInformation__text">Получить более подробную информацию Вы можете у менеджера Центра <br>«Комус-реклама»:</span>
										<span class="importantInformation__contact">Телефон: +7 (495) 225 2301<br>E-mail: reklama@komus.net</span>
									</div>
								</div>
								<div class="sideBar__right-col-r">
									<div class="one-sale__title">
										<h4><span>Сроки действия акции:</span> с 18.10.2017 по 30.01.2018</h4>
									</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eros magna, scelerisque ac rhoncus eu, vulputate vitae elit. Mauris volutpat mattis velit, a convallis leo rhoncus quis. Fusce id eleifend ante. Cras interdum non justo id vestibulum. Donec eu enim tempor lectus ultricies dictum nec a sem. Morbi venenatis metus non bibendum pharetra. Duis id lectus turpis. Fusce eget ornare mi.</p>
									<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris efficitur consectetur nulla. Mauris vitae enim hendrerit, convallis velit in, fringilla lectus. Vivamus tincidunt est sit amet mauris luctus varius. Duis ante libero, laoreet et placerat quis, bibendum sollicitudin sem. Quisque eleifend vitae nisi sed egestas. Etiam orci lectus, varius sed viverra vel, finibus sit amet orci.</p>
									<div class="content-social">
										<span class="strong content-social__title">Поделиться акцией в соц.сетях:</span>
										<ul class="content-social__list">
											<li>
												<a href="#" class="icon-tw content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-yt content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-vk content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-gplus content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-fb content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-lj content-social__link"></a>
											</li>
											<li>
												<a href="#" class="icon-linkedin content-social__link"></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="action-products">
									<div class="action-products__title">
										<h2>Акционные товары</h2>
										<a href="#">Все акционные товары</a>
									</div>
									<div class="action-products__slider js-action-slider greenArrow">
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7916.jpg" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-19733618.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__list">
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="1">
													<img src="media/1-436006-04------.png" alt=""/>
												</a>
												<a href="#" class="catalogItem__list-link js-catalogItem__list-link" data-id="2">
													<img src="media/1-436006-03.png" alt=""/>
												</a>
											</div>
											<div class="catalogItem__block js-catalogItem__block" data-id="1">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-04.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
											<div class="catalogItem__block hidden-block js-catalogItem__block" data-id="2">
												<a href="#" class="catalogItem__link">
													<img src="media/1-436006-042.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Часы настенные «Attendee»</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">718.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-138308.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/7872.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
										<div class="catalogItem">
											<div class="catalogItem__block">
												<a href="#" class="catalogItem__link">
													<img src="media/1-170356.png" al="" class="catalogItem__img" />
												</a>
												<div class="catalogItem__information">
													<span class="catalogItem__title"><a href="#" class="catalogItem__title-link">Подставка для ручек &laquo;Office Express&raquo;</a></span>
													<div class="catalogItem__price">
														<span class="catalogItem__currentPrice">3920.0<ins class="rub">a</ins></span>
														<span class="catalogItem__currentPrice sale">4000.0<ins class="rub">a</ins></span>
													</div>
													<div class="catalogItem__hiddenBlock">
														<span class="catalogItem__article">Артикул: 5654</span>
														<span class="catalogItem__availability">В наличии: 15 штук</span>
													</div>
												</div>
												<div class="catalogItem__functional">
													<div class="catalogItem__amout">
														<input type="text" name="" value="1" class="catalogItem__amout-input js-catalogItem__amoutInput" />
														<a href="#" class="js-catalogItem__plus catalogItem__plus"></a>
														<a href="#" class="js-catalogItem__minus catalogItem__minus"></a>
													</div>
													<a href="#cart" class="catalogItem__buy js-catalogItembuy" data-cart="В корзине">
														<svg class="catalogItem__buy-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																viewBox="0 0 400 359.3" enable-background="new 0 0 400 359.3" xml:space="preserve">
															<g>
																<path fill="#D10910" d="M393.9,138.5c-2.2-3-5.8-4.9-9.4-4.9H14.6c-3.6,0-7.2,1.9-9.4,4.9c-1.8,2.6-2.6,6-2.1,9.2l0.1,0.8l-0.1,0.8
																	c-0.3,1.7-0.2,3.5,0.3,5.5l54.1,172.7c-5.9,1.7-10.2,7.2-10.2,13.7c0,7.9,6.4,14.2,14.2,14.2h276c7.9,0,14.2-6.4,14.2-14.2
																	c0-6.9-4.9-12.6-11.3-13.9l4.7-15.2l9-28.6l0.8-3l0.4-1.4l1.1-4.1l38.1-121.3l0.3-0.5c0.6-1.1,0.9-2.2,0.9-3.4l0-0.4l0.1-0.4
																	C396.6,144.9,395.9,141.2,393.9,138.5z M68.7,289.9h262.5l-11.4,36.2H80.1L68.7,289.9z M336.4,261.5H62l-10-35.6h295L336.4,261.5z
																	M357,197.5H42.8l-10-35.6H367L357,197.5z"/>
																<path fill="#008B3B" d="M178.8,27.7l-69.5,69.5c-5.5,5.5-14.5,5.5-20.1,0l0,0c-5.5-5.5-5.5-14.5,0-20.1l69.5-69.5
																	c5.5-5.5,14.5-5.5,20.1,0l0,0C184.3,13.1,184.3,22.1,178.8,27.7z"/>
																<path fill="#008B3B" d="M217.8,27.7l69.5,69.5c5.5,5.5,14.5,5.5,20.1,0l0,0c5.5-5.5,5.5-14.5,0-20.1L237.9,7.6
																	c-5.5-5.5-14.5-5.5-20.1,0l0,0C212.3,13.1,212.3,22.1,217.8,27.7z"/>
															</g>
														</svg>
														<span class="catalogItem__buy-text js-catalogItem__buy-text">Купить</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
		<script src="js/action-slider.js"></script>
		<script src="js/catalog_list.js"></script>			
	</footer>
	<!-- #footer -->
</body>

</html>