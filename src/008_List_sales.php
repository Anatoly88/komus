<?php $breadcrumbs = true; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require('_head.html'); ?>
	<link href="css/template_styles.css" rel="stylesheet">
	<link href="css/sales.css" rel="stylesheet">
</head>

<body class="withBackground">
	<div class="wrapper">
		<header class="main-header">
			<?php require('_header.php'); ?>
		</header>
		<!-- #header-->
		<main class="content-container">
			<div class="sales-list">
				<div class="content maxWidth">
					<h1>Акции</h1>
					<div class="sales-list__grid">
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">10<span class="precent">%</span></span>
									<span class="figurecaption__text">Новогодние скидки</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Новогодняя акция от фирмы «Комус». Скидка 10% на все товары!
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_2.jpg" alt="">
								<figcaption>
									<span class="figurecaption__twofer">2<span class="price">по цене</span>1</span>
									<span class="figurecaption__text">Европейское качество</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Ежедневники из кожи — традиционный элитарный стиль из Европы.
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_3.jpg" alt="">
								<figcaption>
									<span class="figurecaption__gift"><span class="plus">+</span>подарок</span>
									<span class="figurecaption__text">настольное украшение</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									При покупке офисных принадлежностей более 500 единиц настольное украш...
								</h6>
								<div class="period">Срок действия акции до <span class="date">28.02.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">-15<span class="precent">%</span></span>
									<span class="figurecaption__text">на товары для спорта</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Скидка на все товары для спорта, отдыха и туризма только в Январе.
								</h6>
								<div class="period">Срок действия акции до <span class="date">30.01.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">10<span class="precent">%</span></span>
									<span class="figurecaption__text">Новогодние скидки</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Новогодняя акция от фирмы «Комус». Скидка 10% на все товары!
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_2.jpg" alt="">
								<figcaption>
									<span class="figurecaption__twofer">2<span class="price">по цене</span>1</span>
									<span class="figurecaption__text">Европейское качество</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Ежедневники из кожи — традиционный элитарный стиль из Европы.
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_3.jpg" alt="">
								<figcaption>
									<span class="figurecaption__gift"><span class="plus">+</span>подарок</span>
									<span class="figurecaption__text">настольное украшение</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									При покупке офисных принадлежностей более 500 единиц настольное украш...
								</h6>
								<div class="period">Срок действия акции до <span class="date">28.02.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">-15<span class="precent">%</span></span>
									<span class="figurecaption__text">на товары для спорта</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Скидка на все товары для спорта, отдыха и туризма только в Январе.
								</h6>
								<div class="period">Срок действия акции до <span class="date">30.01.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">10<span class="precent">%</span></span>
									<span class="figurecaption__text">Новогодние скидки</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Новогодняя акция от фирмы «Комус». Скидка 10% на все товары!
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_2.jpg" alt="">
								<figcaption>
									<span class="figurecaption__twofer">2<span class="price">по цене</span>1</span>
									<span class="figurecaption__text">Европейское качество</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Ежедневники из кожи — традиционный элитарный стиль из Европы.
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_3.jpg" alt="">
								<figcaption>
									<span class="figurecaption__gift"><span class="plus">+</span>подарок</span>
									<span class="figurecaption__text">настольное украшение</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									При покупке офисных принадлежностей более 500 единиц настольное украш...
								</h6>
								<div class="period">Срок действия акции до <span class="date">28.02.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">-15<span class="precent">%</span></span>
									<span class="figurecaption__text">на товары для спорта</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Скидка на все товары для спорта, отдыха и туризма только в Январе.
								</h6>
								<div class="period">Срок действия акции до <span class="date">30.01.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">10<span class="precent">%</span></span>
									<span class="figurecaption__text">Новогодние скидки</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Новогодняя акция от фирмы «Комус». Скидка 10% на все товары!
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_2.jpg" alt="">
								<figcaption>
									<span class="figurecaption__twofer">2<span class="price">по цене</span>1</span>
									<span class="figurecaption__text">Европейское качество</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Ежедневники из кожи — традиционный элитарный стиль из Европы.
								</h6>
								<div class="period">Срок действия акции до <span class="date">31.12.2017</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_3.jpg" alt="">
								<figcaption>
									<span class="figurecaption__gift"><span class="plus">+</span>подарок</span>
									<span class="figurecaption__text">настольное украшение</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									При покупке офисных принадлежностей более 500 единиц настольное украш...
								</h6>
								<div class="period">Срок действия акции до <span class="date">28.02.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
						<div class="sales-list__item">
							<figure class="figure">
								<img src="media/sales_img_1.jpg" alt="">
								<figcaption>
									<span class="figurecaption__precent">-15<span class="precent">%</span></span>
									<span class="figurecaption__text">на товары для спорта</span>
								</figcaption>
							</figure>
							<footer class="sales-list__item-footer">
								<h6>
									Скидка на все товары для спорта, отдыха и туризма только в Январе.
								</h6>
								<div class="period">Срок действия акции до <span class="date">30.01.2018</span></div>
								<a href="#" class="read-more">Подробнее</a>
							</footer>
						</div>
					</div>
					<div class="paggination paggination--inner-page">
						<ul class="paggination__list">
							<li class="paggination__item"><a href="#" class="paggination__link arrow prev"></a></li>
							<li class="paggination__item"><a href="#" class="paggination__link">1</a></li>
							<li class="paggination__item"><span class="paggination__link dots">...</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">4</a></li>
							<li class="paggination__item"><span class="paggination__link current">5</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">6</a></li>
							<li class="paggination__item"><span class="paggination__link dots">...</span></li>
							<li class="paggination__item"><a href="#" class="paggination__link">307</a></li>
							<li class="paggination__item"><a href="#" class="paggination__link arrow next"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</main>
		<!-- #content-->
	</div>
	<footer class="footer">
		<?php require('_footer.php'); ?>
	</footer>
	<!-- #footer -->
</body>

</html>